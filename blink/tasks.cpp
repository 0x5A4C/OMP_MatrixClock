#include "tasks.hpp"

#include "espressif/esp_common.h"
#include "esp/uart.h"
#include "FreeRTOS.h"
#include "task.h"
#include "esp8266.h"

#include "IRremote.h"

const int gpioDataIn = 12;
const int gpioMark = 2;
const int freq = 200;

//------------------------------------------------------------------------------

class IrPilot: public IRrecv
{
public:
  IrPilot(void);

  enum class ButtonCode
  {
    Unknown,
    OnOff,
    Source,
    NoSound,
    Record,
    ChPlus,
    TimeShift,
    VolMin,
    FullScr,
    VolPlus,
    But0,
    ChMinus,
    Recall,
    But1,
    But2,
    But3,
    But4,
    But5,
    But6,
    But7,
    But8,
    But9,
    RepeatedLastButton,
    None
  };

  ButtonCode getButton(void);

private:
  decode_results results;

  virtual uint8_t readData(void);
  virtual void irDataMark(bool val);
};

IrPilot::IrPilot(void): IRrecv()
{
    gpio_enable(gpioDataIn, GPIO_INPUT);
    resume();
}

IrPilot::ButtonCode IrPilot::getButton(void)
{
  ButtonCode buttonCode = ButtonCode::None;

  if(decode(&results))
    {
      resume(); // Receive the next value

    switch(results.value)
    {
      case 0xffb24d:
      buttonCode = ButtonCode::OnOff;
      break;
      case 0xff2ad5:
      buttonCode = ButtonCode::Source;
      break;
      case 0xff6897:
      buttonCode = ButtonCode::NoSound;
      break;
      case 0xff32cd:
      buttonCode = ButtonCode::Record;
      break;
      case 0xffa05f:
      buttonCode = ButtonCode::ChPlus;
      break;
      case 0xff30cf:
      buttonCode = ButtonCode::TimeShift;
      break;
      case 0xff50af:
      buttonCode = ButtonCode::VolMin;
      break;
      case 0xff02fd:
      buttonCode = ButtonCode::FullScr;
      break;
      case 0xff7887:
      buttonCode = ButtonCode::VolPlus;
      break;
      case 0xff48b7:
      buttonCode = ButtonCode::But0;
      break;
      case 0xff40bf:
      buttonCode = ButtonCode::ChMinus;
      break;
      case 0xff38c7:
      buttonCode = ButtonCode::Recall;
      break;
      case 0xff906f:
      buttonCode = ButtonCode::But1;
      break;
      case 0xffb847:
      buttonCode = ButtonCode::But2;
      break;
      case 0xfff807:
      buttonCode = ButtonCode::But3;
      break;
      case 0xffb04f:
      buttonCode = ButtonCode::But4;
      break;
      case 0xff9867:
      buttonCode = ButtonCode::But5;
      break;
      case 0xffd827:
      buttonCode = ButtonCode::But6;
      break;
      case 0xff8877:
      buttonCode = ButtonCode::But7;
      break;
      case 0xffa857:
      buttonCode = ButtonCode::But8;
      break;
      case 0xffe817:
      buttonCode = ButtonCode::But9;
      break;
      case 0xffffffff:
      buttonCode = ButtonCode::RepeatedLastButton;
      break;
      default:
      buttonCode = ButtonCode::Unknown;
    }
  }
  return buttonCode;
}

// get state of IR receiver
uint8_t IrPilot::readData(void)
{
  return gpio_read(gpioDataIn);
}

// mark state of IR receiver - log, blink led or whatever
void IrPilot::irDataMark(bool val)
{
  gpio_write(gpioMark, val);
}

IrPilot irPilot;
decode_results results;

extern "C" void decode(void)
{
  switch(irPilot.getButton())
  {
    case IrPilot::ButtonCode::Unknown:
    printf("ir button: Unknown\n");
    break;
    case IrPilot::ButtonCode::OnOff:
    printf("ir button: OnOff\n");
    break;
    case IrPilot::ButtonCode::Source:
    printf("ir button: Source\n");
    break;
    case IrPilot::ButtonCode::NoSound:
    printf("ir button: NoSound\n");
    break;
    case IrPilot::ButtonCode::Record:
    printf("ir button: Record\n");
    break;
    case IrPilot::ButtonCode::ChPlus:
    printf("ir button: ChPlus\n");
    break;
    case IrPilot::ButtonCode::TimeShift:
    printf("ir button: TimeShift\n");
    break;
    case IrPilot::ButtonCode::VolMin:
    printf("ir button: VolMin\n");
    break;
    case IrPilot::ButtonCode::FullScr:
    printf("ir button: FullScr\n");
    break;
    case IrPilot::ButtonCode::VolPlus:
    printf("ir button: VolPlus\n");
    break;
    case IrPilot::ButtonCode::But0:
    printf("ir button: But0\n");
    break;
    case IrPilot::ButtonCode::ChMinus:
    printf("ir button: ChMinus\n");
    break;
    case IrPilot::ButtonCode::Recall:
    printf("ir button: Recall\n");
    break;
    case IrPilot::ButtonCode::But1:
    printf("ir button: But1\n");
    break;
    case IrPilot::ButtonCode::But2:
    printf("ir button: But2\n");
    break;
    case IrPilot::ButtonCode::But3:
    printf("ir button: But3\n");
    break;
    case IrPilot::ButtonCode::But4:
    printf("ir button: But4\n");
    break;
    case IrPilot::ButtonCode::But5:
    printf("ir button: But5\n");
    break;
    case IrPilot::ButtonCode::But6:
    printf("ir button: But6\n");
    break;
    case IrPilot::ButtonCode::But7:
    printf("ir button: But7\n");
    break;
    case IrPilot::ButtonCode::But8:
    printf("ir button: But8\n");
    break;
    case IrPilot::ButtonCode::But9:
    printf("ir button: But9\n");
    break;
    case IrPilot::ButtonCode::RepeatedLastButton:
    printf("ir button: RepeatedLastButton\n");
    break;
    case IrPilot::ButtonCode::None:
    // printf("ir button: None\n");
    break;
    default:
    // printf("default\n");
    break;

  }
}

extern "C" void process(void *arg)
{
  // timer_set_frequency(FRC2, freq);
  irPilot.process();
}

extern "C" void initTasks(void)
{
    // /* configure GPIOs */
    // gpio_enable(gpioMark, GPIO_OUTPUT);
    // gpio_write(gpioMark, 1);
    // gpio_enable(gpioDataIn, GPIO_INPUT);
    //
    // /* stop both timers and mask their interrupts as a precaution */
    // timer_set_interrupts(FRC2, false);
    // timer_set_run(FRC2, false);
    //
    // /* set up ISRs */
    // _xt_isr_attach(INUM_TIMER_FRC2, process, NULL);
    //
    // /* configure timer frequencies */
    // timer_set_frequency(FRC2, freq);
    //
    // /* unmask interrupts and start timers */
    // timer_set_interrupts(FRC2, true);
    // timer_set_run(FRC2, true);
}
