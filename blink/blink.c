/* The classic "blink" example
 *
 * This sample code is in the public domain.
 */
#include <stdlib.h>
#include "espressif/esp_common.h"
#include "esp/uart.h"
#include "FreeRTOS.h"
#include "task.h"
#include "esp8266.h"

#include "tasks.hpp"

//------------------------------------------------------------------------------
const int gpioIn = 12;
const int gpio = 2;
const int freq_frc2 = 20000;

 void frc2InterruptHandler(void *arg)
 {
     /* FRC2 needs the match register updated on each timer interrupt */
    //  timer_set_frequency(FRC2, freq_frc2);
    //  gpio_toggle(gpio);
    // gpio_write(gpio, gpio_read(gpioIn));

     process(arg);
 }

void frc2Init()
{
  /* configure GPIOs */
  gpio_enable(gpioIn, GPIO_INPUT);

  gpio_enable(gpio, GPIO_OUTPUT);
  gpio_write(gpio, 1);

  /* stop both timers and mask their interrupts as a precaution */
  timer_set_interrupts(FRC1, false);
  timer_set_run(FRC1, false);

  /* set up ISRs */
  _xt_isr_attach(INUM_TIMER_FRC1, frc2InterruptHandler, NULL);

  /* configure timer frequencies */
  timer_set_frequency(FRC1, freq_frc2);

  /* unmask interrupts and start timers */
  timer_set_interrupts(FRC1, true);
  timer_set_run(FRC1, true);
}

//------------------------------------------------------------------------------

/* This task uses the high level GPIO API (esp_gpio.h) to blink an LED.
 *
 */
void blinkenTask(void *pvParameters)
{
    while(1)
    {
        vTaskDelay(100 / portTICK_PERIOD_MS);
        // printf("heart bit--------------\n\r");
        decode();
    }
}

/* This task demonstrates an alternative way to use raw register
   operations to blink an LED.

   The step that sets the iomux register can't be automatically
   updated from the 'gpio' constant variable, so you need to change
   the line that sets IOMUX_GPIO2 if you change 'gpio'.

   There is no significant performance benefit to this way over the
   blinkenTask version, so it's probably better to use the blinkenTask
   version.

   NOTE: This task isn't enabled by default, see the commented out line in user_init.
*/
void blinkenRegisterTask(void *pvParameters)
{
    GPIO.ENABLE_OUT_SET = BIT(gpio);
    IOMUX_GPIO2 = IOMUX_GPIO2_FUNC_GPIO | IOMUX_PIN_OUTPUT_ENABLE; /* change this line if you change 'gpio' */
    while(1) {
        GPIO.OUT_SET = BIT(gpio);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
        GPIO.OUT_CLEAR = BIT(gpio);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}

void user_init(void)
{
    uart_set_baud(0, 115200);

    xTaskCreate(blinkenTask, "blinkenTask", 256, NULL, 2, NULL);
    //xTaskCreate(blinkenRegisterTask, "blinkenRegisterTask", 256, NULL, 2, NULL);

    frc2Init();
    initTasks();
}
