#ifndef TASKS_HPP
#define TASKS_HPP

#ifdef __cplusplus
extern "C" {
#endif

void decode(void);
void process(void *arg);
void initTasks(void);

#ifdef __cplusplus
}
#endif


#endif
