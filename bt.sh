#!/bin/sh

astyle -s3 --style=allman -n -r src/system/framework/Stm/'*.cpp'
astyle -s3 --style=allman -n -r src/system/framework/Stm/'*.hpp'
astyle -s3 --style=allman -n -r src/system/framework/Stm/'*.h'
astyle -s3 --style=allman -n -r src/system/framework/Stm/'*.c'

astyle -s3 --style=allman -n -r src/system/framework/IO/'*.cpp'
astyle -s3 --style=allman -n -r src/system/framework/IO/'*.hpp'
astyle -s3 --style=allman -n -r src/system/framework/IO/'*.h'
astyle -s3 --style=allman -n -r src/system/framework/IO/'*.c'

astyle -s3 --style=allman -n -r src/system/framework/sGfx/'*.cpp'
astyle -s3 --style=allman -n -r src/system/framework/sGfx/'*.hpp'
astyle -s3 --style=allman -n -r src/system/framework/sGfx/'*.h'
astyle -s3 --style=allman -n -r src/system/framework/sGfx/'*.c'

astyle -s3 --style=allman -n -r src/bsp/'*.cpp'
astyle -s3 --style=allman -n -r src/bsp/'*.hpp'
astyle -s3 --style=allman -n -r src/bsp/'*.h'
astyle -s3 --style=allman -n -r src/bsp/'*.c'

astyle -s3 --style=allman -n -r src/app/'*.cpp'
astyle -s3 --style=allman -n -r src/app/'*.hpp'
astyle -s3 --style=allman -n -r src/app/'*.h'
astyle -s3 --style=allman -n -r src/app/'*.c'
