#include "espressif/esp_common.h"
#include "esp/uart.h"

#include <unistd.h>
#include <string.h>

#include "FreeRTOS.h"
#include "task.h"

#include "lwip/err.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"
#include "lwip/netdb.h"
#include "lwip/dns.h"

#include "ssid_config.h"

// #define WEB_SERVER "api.thingspeak.com"
// #define WEB_PORT "80"
// #define WEB_PATH "/"

#define WEB_SERVER "www.howsmyssl.com"
#define WEB_PORT "443"

#define vTaskDelayMs(ms)	vTaskDelay((ms)/portTICK_PERIOD_MS)
#define UNUSED_ARG(x)	(void)x

void tsk(void *pvParameters)
{
	UNUSED_ARG(pvParameters);
#if 1
  int successes = 0, failures = 0;
  printf("HTTP get task starting...\r\n");

  while(1)
  {
    const struct addrinfo hints =
    {
        .ai_family = AF_UNSPEC,
        .ai_socktype = SOCK_STREAM,
    };
    struct addrinfo *res;

    printf("Running DNS lookup for %s...\r\n", WEB_SERVER);
    int err = getaddrinfo(WEB_SERVER, "80", &hints, &res);

    if (err != 0 || res == NULL)
    {
        printf("DNS lookup failed err=%d res=%p\r\n", err, res);
        if(res)
            freeaddrinfo(res);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
        failures++;
        continue;
    }

    struct sockaddr *sa = res->ai_addr;
    if (sa->sa_family == AF_INET) {
        printf("DNS lookup succeeded. IP=%s\r\n", inet_ntoa(((struct sockaddr_in *)sa)->sin_addr));
    }

    int s = socket(res->ai_family, res->ai_socktype, 0);
    if(s < 0)
    {
        printf("... Failed to allocate socket.\r\n");
        freeaddrinfo(res);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
        failures++;
        continue;
    }

    printf("... allocated socket\r\n");

    if(connect(s, res->ai_addr, res->ai_addrlen) != 0)
    {
        close(s);
        freeaddrinfo(res);
        printf("... socket connect failed.\r\n");
        vTaskDelay(4000 / portTICK_PERIOD_MS);
        failures++;
        continue;
    }

    printf("... connected\r\n");
    freeaddrinfo(res);

    // String url = "/update?key=";
    // url += writeAPIKey;
    // url += "&field1=";
    // url += String(humidity);//
    // url += "&field2=";
    // url += String(humiditySetPoint );//
    // url += "&field3=";
    // url += String(humidifier);
    // url += "&field4=";
    // url += String(autoMode); //
    // url += "&field5=";
    // url += String(t);
    // url += "\r\n";
    //
    // // Send request to the server
    // client.print(String("GET ") + url + " HTTP/1.1\r\n" +
    //              "Host: " + host + "\r\n" +
    //              "Connection: close\r\n\r\n");


    // const char *req =
    //     "GET "WEB_PATH" HTTP/1.1\r\n"
    //     "Host: "WEB_SERVER"\r\n"
    //     "User-Agent: esp-open-rtos/0.1 esp8266\r\n"
    //     "Connection: close\r\n"
    //     "\r\n";

    // const char *req =
    //     "GET /update?key=A4RLMXL6LYHWZAX4&field1=123 HTTP/1.1\r\n"
    //     "Host: "WEB_SERVER"\r\n"
    //     "User-Agent: esp-open-rtos/0.1 esp8266\r\n"
    //     "Connection: close\r\n"
    //     "\r\n";

    const char *req =
        "\r\n";


    if (write(s, req, strlen(req)) < 0)
    {
        printf("... socket send failed\r\n");
        close(s);
        vTaskDelay(4000 / portTICK_PERIOD_MS);
        failures++;
        continue;
    }
    printf("... socket send success\r\n");

    // static char recv_buf[128];
    // int r;
    // do {
    //     bzero(recv_buf, 128);
    //     r = read(s, recv_buf, 127);
    //     if(r > 0) {
    //         printf("%s", recv_buf);
    //     }
    // } while(r > 0);
    //
    // printf("... done reading from socket. Last read return=%d errno=%d\r\n", r, errno);
    // if(r != 0)
    //     failures++;
    // else
    //     successes++;
    // close(s);
    // printf("successes = %d failures = %d\r\n", successes, failures);
    // for(int countdown = 10; countdown >= 0; countdown--) {
    //     printf("%d... ", countdown);
    //     vTaskDelay(1000 / portTICK_PERIOD_MS);
    // }
    // printf("\r\nStarting again!\r\n");

    vTaskDelayMs(5000);
  }
#endif
#if 0
  while(1)
  {
    /*
     * Wait until we can resolve the DNS for the server, as an indication
     * our network is probably working...
     */
    const struct addrinfo hints = {
        .ai_family = AF_INET,
        .ai_socktype = SOCK_STREAM,
    };
    struct addrinfo *res = NULL;
    int dns_err = 0;
    do {
        if (res)
            freeaddrinfo(res);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
        dns_err = getaddrinfo(WEB_SERVER, WEB_PORT, &hints, &res);
    } while(dns_err != 0 || res == NULL);

    int fd = socket(res->ai_family, res->ai_socktype, 0);
    if (fd < 0) {
        freeaddrinfo(res);
        printf("socket failed\n");
        continue;
    }

    printf("Initializing BearSSL... ");
    vTaskDelay(1000 / portTICK_PERIOD_MS);
  }
#endif
}


void httpGet_init(void)
{
    printf("ntpGet_init\n");

    struct sdk_station_config config =
    {
        .ssid = "335_01",
        .password = "121119710987",
    };

    printf("wifi init\n");
    /* required to call wifi_set_opmode before station_set_config */
    sdk_wifi_set_opmode(STATION_MODE);
    sdk_wifi_station_set_config(&config);

    printf("task create\n");
    xTaskCreate(tsk, "tsk", 1024, NULL, 1, NULL);
}
