/******************************************************************
 A library for controling a set of 8x8 LEDs with a MAX7219 or
 MAX7221 displays.
 This is a plugin for Adafruit's core graphics library, providing
 basic graphics primitives (points, lines, circles, etc.).
 You need to download and install Adafruit_GFX to use this library.

 Adafruit invests time and resources providing this open
 source code, please support Adafruit and open-source hardware
 by purchasing products from Adafruit!

 Written by Mark Ruys.
 BSD license, check license.txt for more information.
 All text above must be included in any redistribution.
 ******************************************************************/

#include "Max72xxPanel.h"

#include <esp/spi.h>
#include <esp/gpio.h>

#define SPI_BUS 1

#define LOW 0
#define HIGH 1

// The opcodes for the MAX7221 and MAX7219
#define OP_NOOP         0
#define OP_DIGIT0       1
#define OP_DIGIT1       2
#define OP_DIGIT2       3
#define OP_DIGIT3       4
#define OP_DIGIT4       5
#define OP_DIGIT5       6
#define OP_DIGIT6       7
#define OP_DIGIT7       8
#define OP_DECODEMODE   9
#define OP_INTENSITY   10
#define OP_SCANLIMIT   11
#define OP_SHUTDOWN    12
#define OP_DISPLAYTEST 15

static const spi_settings_t bus_settings =
{
    mode         : SPI_MODE0,
    freq_divider : SPI_FREQ_DIV_10M,
    msb          : true,
    endianness   : SPI_BIG_ENDIAN,
    minimal_pins : true
};

Max72xxPanel::Max72xxPanel(uint8_t csPin, uint8_t hDisplays, uint8_t vDisplays)
{

  Max72xxPanel::spiCS = csPin;

  uint8_t displays = hDisplays * vDisplays;
  Max72xxPanel::hDisplays = hDisplays;
	Max72xxPanel::bitmapSize = displays << 3;

  Max72xxPanel::bitmap = (uint8_t*)malloc(bitmapSize);
  Max72xxPanel::matrixRotation = (uint8_t*)malloc(displays);
  Max72xxPanel::matrixPosition = (uint8_t*)malloc(displays);

  for ( uint8_t display = 0; display < displays; display++ )
  {
  	matrixPosition[display] = display;
  	matrixRotation[display] = 0;
  }

  // SPI.begin();
//SPI.setBitOrder(MSBFIRST);
//SPI.setDataMode(SPI_MODE0);
  // pinMode(SPI_CS, OUTPUT);
  gpio_enable(spiCS, GPIO_OUTPUT);
  gpio_write(spiCS, true);

  // Clear the screen
  fillScreen(0);

  // Make sure we are not in test mode
  spiTransfer(OP_DISPLAYTEST, 0);

  // We need the multiplexer to scan all segments
  spiTransfer(OP_SCANLIMIT, 7);

  // We don't want the multiplexer to decode segments for us
  spiTransfer(OP_DECODEMODE, 0);

  // Enable display
  shutdown(false);

  // Set the brightness to a medium value
  setIntensity(7);
}

void Max72xxPanel::setPosition(uint8_t display, uint8_t x, uint8_t y) {
	matrixPosition[x + hDisplays * y] = display;
}

void Max72xxPanel::setRotation(uint8_t display, uint8_t rotation) {
	matrixRotation[display] = rotation;
}

void Max72xxPanel::setRotation(uint8_t rotation) {
	// Adafruit_GFX::setRotation(rotation);
  this->rotation = rotation;
}

void Max72xxPanel::shutdown(bool b) {
  spiTransfer(OP_SHUTDOWN, b ? 0 : 1);
}

void Max72xxPanel::setIntensity(uint8_t intensity) {
  spiTransfer(OP_INTENSITY, intensity);
}

void Max72xxPanel::fillScreen(uint16_t color) {
  memset(bitmap, color ? 0xff : 0, bitmapSize);
}

void Max72xxPanel::drawPixel(int16_t xx, int16_t yy, uint16_t color) {
	// Operating in uint8_ts is faster and takes less code to run. We don't
	// need values above 200, so switch from 16 bit ints to 8 bit unsigned
	// ints (uint8_ts).
	int8_t x = xx;
	uint8_t y = yy;
	uint8_t tmp;

	if ( rotation ) {
		// Implement Adafruit's rotation.
		if ( rotation >= 2 ) {										// rotation == 2 || rotation == 3
			x = _width - 1 - x;
		}

		if ( rotation == 1 || rotation == 2 ) {		// rotation == 1 || rotation == 2
			y = _height - 1 - y;
		}

		if ( rotation & 1 ) {     								// rotation == 1 || rotation == 3
			tmp = x; x = y; y = tmp;
		}
	}

	if ( x < 0 || x >= WIDTH || y < 0 || y >= HEIGHT ) {
		// Ignore pixels outside the canvas.
		return;
	}

	// Translate the x, y coordinate according to the layout of the
	// displays. They can be ordered and rotated (0, 90, 180, 270).

	uint8_t display = matrixPosition[(x >> 3) + hDisplays * (y >> 3)];
	x &= 0b111;
	y &= 0b111;

	uint8_t r = matrixRotation[display];
	if ( r >= 2 ) {										   // 180 or 270 degrees
		x = 7 - x;
	}
	if ( r == 1 || r == 2 ) {				     // 90 or 180 degrees
		y = 7 - y;
	}
	if ( r & 1 ) {     								   // 90 or 270 degrees
		tmp = x; x = y; y = tmp;
	}

	uint8_t d = display / hDisplays;
	x += (display - d * hDisplays) << 3; // x += (display % hDisplays) * 8
	y += d << 3;												 // y += (display / hDisplays) * 8

	// Update the color bit in our bitmap buffer.

	uint8_t *ptr = bitmap + x + WIDTH * (y >> 3);
	uint8_t val = 1 << (y & 0b111);

	if ( color ) {
		*ptr |= val;
	}
	else {
		*ptr &= ~val;
	}
}

void Max72xxPanel::write() {
	// Send the bitmap buffer to the displays.

	for ( uint8_t row = OP_DIGIT7; row >= OP_DIGIT0; row-- ) {
		spiTransfer(row);
	}
}

void Max72xxPanel::spiTransfer(uint8_t opcode, uint8_t data) {
	// If opcode > OP_DIGIT7, send the opcode and data to all displays.
	// If opcode <= OP_DIGIT7, display the column with data in our buffer for all displays.
	// We do not support (nor need) to use the OP_NOOP opcode.

  spi_settings_t old_settings;
  spi_get_settings(SPI_BUS, &old_settings);
  spi_set_settings(SPI_BUS, &bus_settings);

	// Enable the line
	// digitalWrite(SPI_CS, LOW);
  gpio_write(spiCS, LOW);

	// Now shift out the data, two uint8_ts per display. The first uint8_t is the opcode,
	// the second uint8_t the data.
	uint8_t end = opcode - OP_DIGIT0;
	uint8_t start = bitmapSize + end;
  // uint16_t buf = (opcode << 8) | (opcode <= OP_DIGIT7 ? bitmap[start] : data);
  uint8_t dt = (opcode <= OP_DIGIT7 ? bitmap[start] : data);
	do {
		start -= 8;
//FIXME
		// SPI.transfer(opcode);
		// SPI.transfer(opcode <= OP_DIGIT7 ? bitmap[start] : data);
    // spi_transfer(SPI_BUS, &opcode, NULL, sizeof(uint8_t), SPI_8BIT);
    // spi_transfer(SPI_BUS, &dt,     NULL, sizeof(uint8_t), SPI_8BIT);
    spi_transfer_8(SPI_BUS, (opcode));
    spi_transfer_8(SPI_BUS, (opcode <= OP_DIGIT7 ? bitmap[start] : data));

    // spi_transfer(SPI_BUS, &buf, NULL, sizeof(uint16_t), SPI_16BIT);
	}
	while ( start > end );

	// Latch the data onto the display(s)
	// digitalWrite(SPI_CS, HIGH);
  gpio_write(spiCS, HIGH);
  spi_set_settings(SPI_BUS, &old_settings);
}
