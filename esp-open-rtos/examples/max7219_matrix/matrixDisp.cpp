// #include "Max72xxPanel.h"

#include <esp/spi.h>
#include <esp/gpio.h>

#define CS_PIN 16

#define SPI_BUS 1

#define LOW 0
#define HIGH 1

// The opcodes for the MAX7221 and MAX7219
#define OP_NOOP         0
#define OP_DIGIT0       1
#define OP_DIGIT1       2
#define OP_DIGIT2       3
#define OP_DIGIT3       4
#define OP_DIGIT4       5
#define OP_DIGIT5       6
#define OP_DIGIT6       7
#define OP_DIGIT7       8
#define OP_DECODEMODE   9
#define OP_INTENSITY   10
#define OP_SCANLIMIT   11
#define OP_SHUTDOWN    12
#define OP_DISPLAYTEST 15

static const spi_settings_t bus_settings =
{
    mode         : SPI_MODE0,
    freq_divider : SPI_FREQ_DIV_10M,
    msb          : true,
    endianness   : SPI_BIG_ENDIAN,
    minimal_pins : true
};

// Max72xxPanel ledMatrixDisp(CS_PIN, 4, 1);

extern "C" void ledDisp_Init(void)
{
  // ledMatrixDisp.setRotation(0, 1);        // 1
  // ledMatrixDisp.setRotation(1, 1);        // 2
  // ledMatrixDisp.setRotation(2, 1);        // 3
  // ledMatrixDisp.setRotation(3, 1);        // 4
  // ledMatrixDisp.fillScreen(0);
  // ledMatrixDisp.drawPixel(3, 3, 1);
  // ledMatrixDisp.write();
  // ledMatrixDisp.setIntensity(4); // Set brightness between 0 and 15

//-init-------------------------------------------------------------------------
gpio_enable(CS_PIN, GPIO_OUTPUT);
gpio_write(CS_PIN, HIGH);

spi_settings_t old_settings;
spi_get_settings(SPI_BUS, &old_settings);
spi_set_settings(SPI_BUS, &bus_settings);

gpio_write(CS_PIN, LOW);

// Clear the screen
// fillScreen(0);

// Make sure we are not in test mode
// spiTransfer(OP_DISPLAYTEST, 0);
spi_transfer_8(SPI_BUS, OP_DISPLAYTEST);
spi_transfer_8(SPI_BUS, 0);

// We need the multiplexer to scan all segments
// spiTransfer(OP_SCANLIMIT, 7);
spi_transfer_8(SPI_BUS, OP_SCANLIMIT);
spi_transfer_8(SPI_BUS, 7);

// We don't want the multiplexer to decode segments for us
// spiTransfer(OP_DECODEMODE, 0);
spi_transfer_8(SPI_BUS, OP_DECODEMODE);
spi_transfer_8(SPI_BUS, 0);

// Enable display
// shutdown(false);
spi_transfer_8(SPI_BUS, OP_SHUTDOWN);
spi_transfer_8(SPI_BUS, 1);

// Set the brightness to a medium value
// setIntensity(7);
spi_transfer_8(SPI_BUS, OP_INTENSITY);
spi_transfer_8(SPI_BUS, 4);

spi_transfer_8(SPI_BUS, OP_DIGIT0);
spi_transfer_8(SPI_BUS, 4);

spi_transfer_8(SPI_BUS, OP_DIGIT0);
spi_transfer_8(SPI_BUS, 4);


gpio_write(CS_PIN, HIGH);
spi_set_settings(SPI_BUS, &old_settings);
//-init-------------------------------------------------------------------------

}
