/*
 * Example of using MAX7219 driver with 7 segment displays
 *
 * Part of esp-open-rtos
 * Copyright (C) 2017 Ruslan V. Uss <unclerus@gmail.com>
 * BSD Licensed as described in the file LICENSE
 */
#include <esp/uart.h>
#include <espressif/esp_common.h>
#include <stdio.h>
#include <max7219/max7219.h>
#include <FreeRTOS.h>
#include <task.h>
#include <stdio.h>
#include <esp/hwrand.h>

#include "httpGet.h"

#define CS_PIN 16
#define DELAY 1000

static max7219_display_t disp =
{
    .cs_pin       = CS_PIN,
    .digits       = 8,
    .cascade_size = 4,
    .mirrored     = true
};

extern void ledDisp_Init(void);

void sendFrameBuf(const max7219_display_t *disp, uint8_t *frameBuf);
uint8_t fb[4] = {1,2,3,4};
void user_init(void)
{
    uart_set_baud(0, 115200);
    printf("user_init\n");

    // ledDisp_Init();

    // max7219_init(&disp);
    max7219_matrix_init(&disp, 4, 1);

    sendFrameBuf(&disp, fb);

    // tempGet_init();
    httpGet_init();
#if 0
    char buf[9];
    while (true)
    {



        // max7219_clear(&disp);
        // max7219_draw_text(&disp, 0, "7219LEDS");
        vTaskDelay(DELAY / portTICK_PERIOD_MS);

        // max7219_clear(&disp);
        // sprintf(buf, "%2.4f A", 34.6782);
        // max7219_draw_text(&disp, 0, buf);
        // vTaskDelay(DELAY / portTICK_PERIOD_MS);
        //
        // max7219_clear(&disp);
        // sprintf(buf, "%08x", hwrand());
        // max7219_draw_text(&disp, 0, buf);
        // vTaskDelay(DELAY / portTICK_PERIOD_MS);

        write(&disp);
        vTaskDelay(DELAY / portTICK_PERIOD_MS);
        // disp.frameBuf[1]=3;

        setRotation(&disp, 0, 1);        // 1
        setRotation(&disp, 1, 1);        // 2
        setRotation(&disp, 2, 1);        // 3
        setRotation(&disp, 3, 1);        // 4
        fillScreen(&disp, 0);
        drawPixel(&disp, 0,  0, 1);
        drawPixel(&disp, 8,  1, 1);
        drawPixel(&disp, 16, 2, 1);
        drawPixel(&disp, 24, 3, 1);

    }
#endif
}
