# include it as 'apds9960/apds9960.h'
INC_DIRS += $(apds9960_ROOT)..

# args for passing into compile rule generation
apds9960_SRC_DIR = $(apds9960_ROOT)

$(eval $(call component_compile_rules,apds9960))
