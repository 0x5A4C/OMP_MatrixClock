/*
 * Driver for MAX7219/MAX7221
 * Serially Interfaced, 8-Digit LED Display Drivers
 *
 * Part of esp-open-rtos
 * Copyright (C) 2017 Ruslan V. Uss <unclerus@gmail.com>
 * BSD Licensed as described in the file LICENSE
 */
#include "max7219.h"
#include <esp/spi.h>
#include <esp/gpio.h>
#include <string.h>

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>


#include "max7219_priv.h"

#define SPI_BUS 1

//#define MAX7219_DEBUG

#ifdef MAX7219_DEBUG
#include <stdio.h>
#define debug(fmt, ...) printf("%s: " fmt "\n", "MAX7219", ## __VA_ARGS__)
#else
#define debug(fmt, ...)
#endif

#define ALL_CHIPS 0xff
#define ALL_DIGITS 8

#define REG_DIGIT_0      (1 << 8)
#define REG_DECODE_MODE  (9 << 8)
#define REG_INTENSITY    (10 << 8)
#define REG_SCAN_LIMIT   (11 << 8)
#define REG_SHUTDOWN     (12 << 8)
#define REG_DISPLAY_TEST (15 << 8)

#define VAL_CLEAR_BCD    0x0f
#define VAL_CLEAR_NORMAL 0x00

#define OP_NOOP         0
#define OP_DIGIT0       1
#define OP_DIGIT1       2
#define OP_DIGIT2       3
#define OP_DIGIT3       4
#define OP_DIGIT4       5
#define OP_DIGIT5       6
#define OP_DIGIT6       7
#define OP_DIGIT7       8

static const spi_settings_t bus_settings = {
    .mode         = SPI_MODE0,
    .freq_divider = SPI_FREQ_DIV_10M,
    .msb          = true,
    .minimal_pins = true,
    .endianness   = SPI_BIG_ENDIAN
};

static void send(const max7219_display_t *disp, uint8_t chip, uint16_t value)
{
    uint16_t buf[MAX7219_MAX_CASCADE_SIZE] = { 0 };
    if (chip == ALL_CHIPS)
    {
        for (uint8_t i = 0; i < disp->cascade_size; i++)
            buf[i] = value;
    }
    else buf[chip] = value;

    spi_settings_t old_settings;
    spi_get_settings(SPI_BUS, &old_settings);
    spi_set_settings(SPI_BUS, &bus_settings);
    gpio_write(disp->cs_pin, false);

    spi_transfer(SPI_BUS, buf, NULL, disp->cascade_size, SPI_16BIT);

    gpio_write(disp->cs_pin, true);
    spi_set_settings(SPI_BUS, &old_settings);
}

uint8_t fbf[] = {1,2,3,4,5,6,7,8,1,2,3,4,5,6,7,8,1,2,3,4,5,6,7,8,1,2,3,4,5,6,7,8};
void sendFrameBuf(const max7219_display_t *disp, uint8_t *frameBuf)
{
    // send(disp, ALL_CHIPS, (uint16_t)((1<<8)|1));
    // send(disp, ALL_CHIPS, (uint16_t)((2<<8)|1));
    // send(disp, ALL_CHIPS, (uint16_t)((3<<8)|1));
    // send(disp, ALL_CHIPS, (uint16_t)((4<<8)|1));
    // send(disp, ALL_CHIPS, (uint16_t)((5<<8)|1));
    // send(disp, ALL_CHIPS, (uint16_t)((6<<8)|1));
    // send(disp, ALL_CHIPS, (uint16_t)((7<<8)|1));
    // send(disp, ALL_CHIPS, (uint16_t)((8<<8)|1));

    // send(disp, 0, (uint16_t)((1<<8)|1));
    // send(disp, 1, (uint16_t)((2<<8)|1));
    // send(disp, 2, (uint16_t)((3<<8)|1));
    // send(disp, 3, (uint16_t)((4<<8)|1));

    spi_settings_t old_settings;
    spi_get_settings(SPI_BUS, &old_settings);
    spi_set_settings(SPI_BUS, &bus_settings);
    gpio_write(disp->cs_pin, false);
    spi_transfer_8(SPI_BUS, 1);
    spi_transfer_8(SPI_BUS, 1);
    spi_transfer_8(SPI_BUS, 1);
    spi_transfer_8(SPI_BUS, 1);
    spi_transfer_8(SPI_BUS, 1);
    spi_transfer_8(SPI_BUS, 1);
    spi_transfer_8(SPI_BUS, 1);
    spi_transfer_8(SPI_BUS, 1);
    gpio_write(disp->cs_pin, true);
    gpio_write(disp->cs_pin, false);
    spi_transfer_8(SPI_BUS, 2);
    spi_transfer_8(SPI_BUS, 3);
    spi_transfer_8(SPI_BUS, 2);
    spi_transfer_8(SPI_BUS, 3);
    spi_transfer_8(SPI_BUS, 2);
    spi_transfer_8(SPI_BUS, 3);
    spi_transfer_8(SPI_BUS, 2);
    spi_transfer_8(SPI_BUS, 3);
    gpio_write(disp->cs_pin, true);

    spi_set_settings(SPI_BUS, &old_settings);
}

void spiTransfer(const max7219_display_t *disp, uint8_t opcode, uint8_t data)
{
  spi_settings_t old_settings;
  spi_get_settings(SPI_BUS, &old_settings);
  spi_set_settings(SPI_BUS, &bus_settings);
  gpio_write(disp->cs_pin, false);

	uint8_t end = opcode - OP_DIGIT0;
	uint8_t start = disp->frameBufSize + end;
	do {
		  start -= 8;
		  spi_transfer_8(SPI_BUS, opcode);
		  spi_transfer_8(SPI_BUS, opcode <= OP_DIGIT7 ? disp->frameBuf[start] : data);
	}
	while ( start > end );

  gpio_write(disp->cs_pin, true);
  spi_set_settings(SPI_BUS, &old_settings);

}

void writeFb(const max7219_display_t *disp)
{
	// Send the bitmap buffer to the displays.

	for ( uint8_t row = OP_DIGIT7; row >= OP_DIGIT0; row-- )
  {
		spiTransfer(disp, row, 0xff);
	}
}

void drawPixel(const max7219_display_t *disp, int16_t xx, int16_t yy, uint16_t color)
{
	// Operating in bytes is faster and takes less code to run. We don't
	// need values above 200, so switch from 16 bit ints to 8 bit unsigned
	// ints (bytes).
	int8_t x = xx;
	uint8_t y = yy;
	uint8_t tmp;

	if ( disp->rotation ) {
		// Implement Adafruit's rotation.
		if ( disp->rotation >= 2 ) {										// rotation == 2 || rotation == 3
			x = disp->_width - 1 - x;
		}

		if ( disp->rotation == 1 || disp->rotation == 2 ) {		// rotation == 1 || rotation == 2
			y = disp->_height - 1 - y;
		}

		if ( disp->rotation & 1 ) {     								// rotation == 1 || rotation == 3
			tmp = x; x = y; y = tmp;
		}
	}

	if ( x < 0 || x >= disp->WIDTH || y < 0 || y >= disp->HEIGHT )
  {
		// Ignore pixels outside the canvas.
		return;
	}

	// Translate the x, y coordinate according to the layout of the
	// displays. They can be ordered and rotated (0, 90, 180, 270).

	uint8_t display = disp->matrixPosition[(x >> 3) + disp->hDisplays * (y >> 3)];
	x &= 0b111;
	y &= 0b111;

	uint8_t r = disp->matrixRotation[display];
	if ( r >= 2 ) {										   // 180 or 270 degrees
		x = 7 - x;
	}
	if ( r == 1 || r == 2 ) {				     // 90 or 180 degrees
		y = 7 - y;
	}
	if ( r & 1 ) {     								   // 90 or 270 degrees
		tmp = x; x = y; y = tmp;
	}

	uint8_t d = display / disp->hDisplays;
	x += (display - d * disp->hDisplays) << 3; // x += (display % hDisplays) * 8
	y += d << 3;												 // y += (display / hDisplays) * 8

	// Update the color bit in our bitmap buffer.

	uint8_t *ptr = disp->frameBuf + x + disp->WIDTH * (y >> 3);
	uint8_t val = 1 << (y & 0b111);

	if ( color ) {
		*ptr |= val;
	}
	else {
		*ptr &= ~val;
	}
}

void setRotation(max7219_display_t *disp, uint8_t display, uint8_t rotation)
{
	disp->matrixRotation[display] = rotation;
}

void fillScreen(max7219_display_t *disp, uint16_t color)
{
  memset(disp->frameBuf, color ? 0xff : 0, disp->frameBufSize);
}

bool max7219_init(max7219_display_t *disp)
{
    if (!disp->cascade_size || disp->cascade_size > MAX7219_MAX_CASCADE_SIZE)
    {
        debug("Invalid cascade size %d", disp->cascade_size);
        return false;
    }

    uint8_t max_digits = disp->cascade_size * ALL_DIGITS;
    if (!disp->digits || disp->digits > max_digits)
    {
        debug("Invalid digits count %d, max %d", disp->cascade_size, max_digits);
        return false;
    }

    gpio_enable(disp->cs_pin, GPIO_OUTPUT);
    gpio_write(disp->cs_pin, true);

    // Shutdown all chips
    max7219_set_shutdown_mode(disp, true);
    // Disable test
    send(disp, ALL_CHIPS, REG_DISPLAY_TEST);
    // Set max scan limit
    send(disp, ALL_CHIPS, REG_SCAN_LIMIT | (ALL_DIGITS - 1));
    // Set normal decode mode & clear display
    max7219_set_decode_mode(disp, false);
    // Set minimal brightness
    max7219_set_brightness(disp, 0);
    // Wake up
    max7219_set_shutdown_mode(disp, false);

    return true;
}

bool max7219_matrix_init(max7219_display_t *disp, uint8_t hDisplays, uint8_t vDisplays)
{
  disp->hDisplays = hDisplays;
  disp->vDisplays = vDisplays;

  disp->WIDTH = hDisplays * 8;
  disp->HEIGHT = vDisplays * 8;

  uint8_t displays = hDisplays * vDisplays;
	disp->frameBufSize = displays << 3;

  disp->frameBuf = (uint8_t*)malloc(disp->frameBufSize);

  disp->matrixRotation = (uint8_t*)malloc(displays);
  disp->matrixPosition = (uint8_t*)malloc(displays);

  for ( uint8_t display = 0; display < displays; display++ ) {
  	disp->matrixPosition[display] = display;
  	disp->matrixRotation[display] = 0;
  }

  max7219_init(disp);

  return true;
}


void max7219_set_decode_mode(max7219_display_t *disp, bool bcd)
{
    disp->bcd = bcd;
    send(disp, ALL_CHIPS, REG_DECODE_MODE | (bcd ? 0xff : 0));
    max7219_clear(disp);
}

void max7219_set_brightness(const max7219_display_t *disp, uint8_t value)
{
    send(disp, ALL_CHIPS, REG_INTENSITY | (value > MAX7219_MAX_BRIGHTNESS ? MAX7219_MAX_BRIGHTNESS : value));
}

void max7219_set_shutdown_mode(const max7219_display_t *disp, bool shutdown)
{
    send(disp, ALL_CHIPS, REG_SHUTDOWN | !shutdown);
}

bool max7219_set_digit(const max7219_display_t *disp, uint8_t digit, uint8_t val)
{
    if (digit >= disp->digits)
    {
        debug("Invalid digit: %d", digit);
        return false;
    }

    if (disp->mirrored)
        digit = disp->digits - digit - 1;

    uint8_t c = digit / ALL_DIGITS;
    uint8_t d = digit % ALL_DIGITS;

    send(disp, c, (REG_DIGIT_0 + ((uint16_t)d << 8)) | val);

    return true;
}

void max7219_clear(const max7219_display_t *disp)
{
    uint8_t val = disp->bcd ? VAL_CLEAR_BCD : VAL_CLEAR_NORMAL;
    for (uint8_t i = 0; i < ALL_DIGITS; i++)
        send(disp, ALL_CHIPS, (REG_DIGIT_0 + ((uint16_t)i << 8)) | val);
}

inline static uint8_t get_char(const max7219_display_t *disp, char c)
{
    if (disp->bcd)
    {
        if (c >= '0' && c <= '9')
            return c - '0';
        switch (c)
        {
            case '-':
                return 0x0a;
            case 'E':
            case 'e':
                return 0x0b;
            case 'H':
            case 'h':
                return 0x0c;
            case 'L':
            case 'l':
                return 0x0d;
            case 'P':
            case 'p':
                return 0x0e;
        }
        return VAL_CLEAR_BCD;
    }

    return font_7seg[(c - 0x20) & 0x7f];
}

void max7219_draw_text(const max7219_display_t *disp, uint8_t pos, const char *s)
{
    while (s && pos < disp->digits)
    {
        uint8_t c = get_char(disp, *s);
        if (*(s + 1) == '.')
        {
            c |= 0x80;
            s++;
        }
        max7219_set_digit(disp, pos, c);
        pos++;
        s++;
    }
}
