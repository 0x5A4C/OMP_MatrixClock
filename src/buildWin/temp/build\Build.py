import os
import hashlib
import lib.Compile as Compiler
import lib.Link as Linker
import lib.FilesFinder as Files
import lib.FileCreator as Executable



projName = "winTest"
projPath = "..\\..\\"
projPath = "D:\Projects\AppStrTest"



def CheckHeaderFiles():
    iterator = 0;

    filesDirectory = [ projPath ]
    exludeSerchDirectories = []

    hushedFiles = {}

    headerFiles = Files.Find(filesDirectory, exludeSerchDirectories, [ ".hpp", ".h" ])

    for file in headerFiles:
        hushedFiles[file] = hashlib.md5(open(file, 'rb').read()).hexdigest()

    JsonRead = Files.JsonRead("MD5.json")

    if JsonRead == False :
        Files.JsonWrite("MD5.json", hushedFiles)
        return True
    else:
        for file in headerFiles:
            if hushedFiles[file] != JsonRead[file]:
                iterator = iterator + 1

    Files.JsonWrite("MD5.json", {**JsonRead, **hushedFiles})

    if iterator != 0:
        return True;
    else:
        return False;



def Build(changedHeader = True):
    global projName
    global projPath
    global projPath

    os.environ["PATH"] += os.pathsep + 'D:\\etools\\CygWin64\\2.877\\bin'
    # os.environ["PATH"] += os.pathsep + 'D:\\Tools\\boost_1_64_0\\boost\\bin'
    # os.environ["PATH"] += os.pathsep + 'D:\\Tools\\UniversalIndentGUI_win32\\indenters'

    #has to be a string
    compiler = "gcc"

    #has to be a list
    compilationFlags = [ "--output_dir=" + os.path.join(projPath, "Output\\Obj")]

    #has to be a list
    compilationDefines = []

    #has to be a list
    includeDirectories = [
                           os.path.join(projPath, "App"),
                           os.path.join(projPath, "Bsp"),
                           os.path.join(projPath, "Lib"),
                           os.path.join(projPath, "Mcu"),
                         ]

    #has to be a string
    filesDirectory =    [
                        ]

    #has to be a string
    exludeSerchDirectories = [
                             ]

    # filesToCompile = Files.Find(filesDirectory, exludeSerchDirectories, [ ".cpp" ])
    filesToCompile = []
    filesToCompile.append(os.path.join(projPath, "main.cpp"))

    if changedHeader == True:
        Compiler.Compile (compiler, compilationFlags, compilationDefines, includeDirectories, filesToCompile)
    else:
        filesToCompile = Files.getChangedFiles("MD5.json", filesToCompile)
        if len(filesToCompile) != 0:
            Compiler.Compile (compiler, compilationFlags, compilationDefines, includeDirectories, filesToCompile)


def LinkProjectFiles():
    #has to be string
    linker = "gcc"
    linkerFlags = [
                    "-o ..\\..\\Out\\file.axf"
                  ]
    filesDirectory = [ "..\\..\\Output\\Obj", "..\\..\\Libs\\TouchGFX\\touchgfx\\lib\\core\\cortex_m3\\Keil"]
    exludeSerchDirectories = []
    filesToLink = Files.Find(filesDirectory, exludeSerchDirectories, [ ".o", ".lib" ])
    Linker.Link(linker, linkerFlags, filesToLink)



ChangedHeader = CheckHeaderFiles()
Build(ChangedHeader)

LinkProjectFiles()
