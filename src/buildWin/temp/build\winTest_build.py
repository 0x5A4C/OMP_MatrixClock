import os
import hashlib
import lib.Compile as Compiler
import lib.Link as Linker
import lib.FilesFinder as Files
import lib.FileCreator as Executable


def CheckHeaderFiles():
	iterator = 0;

	filesDirectory = [ "..\\..\\" ] 
	exludeSerchDirectories = []

	hushedFiles = {}

	headerFiles = Files.Find(filesDirectory, exludeSerchDirectories, [ ".hpp", ".h" ])

	for file in headerFiles:
		hushedFiles[file] = hashlib.md5(open(file, 'rb').read()).hexdigest()

	JsonRead = Files.JsonRead("MD5.json")

	if JsonRead == False :
		Files.JsonWrite("MD5.json", hushedFiles)
		return True
	else:
		for file in headerFiles:
			if hushedFiles[file] != JsonRead[file]:
				iterator = iterator + 1
		
	Files.JsonWrite("MD5.json",	{**JsonRead, **hushedFiles})

	if iterator != 0:
		return True;
	else:
		return False;


def BuildStartupFile(ChangedHeader):
	######################### Build Startup file #########################

	#has to be a string
	compiler = "armcc"

	#has to be a list
	compilationFlags = ["-c", "--cpu=Cortex-M3", "-o ..\\..\\Output\\Obj\\startup.o"] 

	#has to be a list
	compilationDefines = ["CORE_M3"]

	#has to be a list
	includeDirectories = [ "..\\..\\Controller\\LPCOpen\\LPC43xx_18xx\\chip_43xx_18xx\\inc", 
						   "..\\..\\Controller\\LPCOpen\\LPC43xx_18xx\\chip_43xx_18xx\\config_18xx"] 

	#has to be a list
	filesToCompile = [ "..\\..\\Controller\\LPCOpen\\LPC43xx_18xx\\chip_43xx_18xx\\startup\\keil_startup_lpc18xx43xx.s" ]


	if ChangedHeader == True:
		Compiler.Compile (compiler, compilationFlags, compilationDefines, includeDirectories, filesToCompile)
	else:
		filesToCompile = Files.getChangedFiles("MD5.json", filesToCompile)
		if len(filesToCompile) != 0:
			Compiler.Compile (compiler, compilationFlags, compilationDefines, includeDirectories, filesToCompile)	


def BuildLPCOpen(ChangedHeader):
	######################### Build LPCOpen ######################### 

	#has to be a string
	compiler = "armcc"

	#has to be a list
	compilationFlags = ["-c", "--cpu=Cortex-M3", "--output_dir=..\\..\\Output\\Obj"] 

	#has to be a list
	compilationDefines = ["CORE_M3"]

	#has to be a list
	includeDirectories = [ "..\\..\\Controller\\LPCOpen\\LPC43xx_18xx\\chip_43xx_18xx\\inc", 
						   "..\\..\\Controller\\LPCOpen\\LPC43xx_18xx\\chip_43xx_18xx\\config_18xx",
						   "..\\..\\Board\\UI.C\\inc",
						   "..\\..\\Libs\\FreeRTOS\\Source\\include", 
						   "..\\..\\Libs\\FreeRTOS\\Source\\portable\\RVDS\\ARM_CM3",
						   "..\\..\\Application\\platform\\os" ]  

	#has to be a list
	filesToCompile = [ "..\\..\\Controller\\LPCOpen\\LPC43xx_18xx\\chip_43xx_18xx\\startup\\sysinit.c",
					   "..\\..\\Controller\\LPCOpen\\LPC43xx_18xx\\chip_43xx_18xx\\src\\chip_18xx_43xx.c",
					   "..\\..\\Controller\\LPCOpen\\LPC43xx_18xx\\chip_43xx_18xx\\src\\gpio_18xx_43xx.c",
					   "..\\..\\Controller\\LPCOpen\\LPC43xx_18xx\\chip_43xx_18xx\\src\\sysinit_18xx_43xx.c",
					   "..\\..\\Controller\\LPCOpen\\LPC43xx_18xx\\chip_43xx_18xx\\src\\clock_18xx_43xx.c"]


	if ChangedHeader == True:
		Compiler.Compile (compiler, compilationFlags, compilationDefines, includeDirectories, filesToCompile)
	else:
		filesToCompile = Files.getChangedFiles("MD5.json", filesToCompile)
		if len(filesToCompile) != 0:
			Compiler.Compile (compiler, compilationFlags, compilationDefines, includeDirectories, filesToCompile)	


def BuildBSP(ChangedHeader):
	######################### Build BSP #########################

	#has to be a string
	compiler = "armcc"

	#has to be a list
	compilationFlags = ["-c", "--cpu=Cortex-M3", "--output_dir=..\\..\\Output\\Obj"] 

	#has to be a list
	compilationDefines = ["CORE_M3"]

	#has to be a list
	includeDirectories = [ "..\\..\\Controller\\LPCOpen\\LPC43xx_18xx\\chip_43xx_18xx\\inc", 
						   "..\\..\\Controller\\LPCOpen\\LPC43xx_18xx\\chip_43xx_18xx\\config_18xx",
						   "..\\..\\Board\\UI.C\\inc",
						   "..\\..\\Libs\\FreeRTOS\\Source\\include", 
						   "..\\..\\Libs\\FreeRTOS\\Source\\portable\\RVDS\\ARM_CM3",
						   "..\\..\\Application\\platform\\os" ]  

	#has to be a list
	filesToCompile = [ "..\\..\\Board\\UI.C\\src\\board.c", 
					   "..\\..\\Board\\UI.C\\src\\board_sysinit.c" ]

	if ChangedHeader == True:
		Compiler.Compile (compiler, compilationFlags, compilationDefines, includeDirectories, filesToCompile)
	else:
		filesToCompile = Files.getChangedFiles("MD5.json", filesToCompile)
		if len(filesToCompile) != 0:
			Compiler.Compile (compiler, compilationFlags, compilationDefines, includeDirectories, filesToCompile)	


def BuildFreeRTOS(ChangedHeader):
	########################## FreeRTOS ##########################

	#has to be a string
	compiler = "armcc"

	#has to be a list
	compilationFlags = ["-c", "--cpu=Cortex-M3", "--output_dir=..\\..\\Output\\Obj"] 

	#has to be a list
	compilationDefines = ["CORE_M3", "RVDS_ARMCM3_LM3S102"]

	#has to be a list
	includeDirectories = [ "..\\..\\Libs\\FreeRTOS\\Source\\include", 
						   "..\\..\\Libs\\FreeRTOS\\Source\\portable\\RVDS\\ARM_CM3",
						   "..\\..\\Application\\platform\\os" ]  

	#has to be a list
	filesToCompile = [ "..\\..\\Libs\\FreeRTOS\\Source\\croutine.c", 
					   "..\\..\\Libs\\FreeRTOS\\Source\\event_groups.c",
					   "..\\..\\Libs\\FreeRTOS\\Source\\list.c",
					   "..\\..\\Libs\\FreeRTOS\\Source\\queue.c",
					   "..\\..\\Libs\\FreeRTOS\\Source\\tasks.c",
					   "..\\..\\Libs\\FreeRTOS\\Source\\timers.c",
					   "..\\..\\Libs\\FreeRTOS\\Source\\portable\\MemMang\\heap_3.c",
					   "..\\..\\Libs\\FreeRTOS\\Source\\portable\\RVDS\\ARM_CM3\\port.c" ]

	if ChangedHeader == True:
		Compiler.Compile (compiler, compilationFlags, compilationDefines, includeDirectories, filesToCompile)
	else:
		filesToCompile = Files.getChangedFiles("MD5.json", filesToCompile)
		if len(filesToCompile) != 0:
			Compiler.Compile (compiler, compilationFlags, compilationDefines, includeDirectories, filesToCompile)	

def BuildApplication(ChangedHeader):
	######################### Build Application #########################

	#has to be a string
	compiler = "armcc"

	#has to be a list
	compilationFlags = ["--cpp", "-c", "--cpu=Cortex-M3", "--output_dir=..\\..\\Output\\Obj"] 

	#has to be a list
	compilationDefines = ["CORE_M3", "RVDS_ARMCM3_LM3S102"]

	#has to be a list
	includeDirectories = [ "..\\..\\Board\\UI.C\\inc",
						   "..\\..\\Controller\\LPCOpen\\LPC43xx_18xx\\chip_43xx_18xx\\inc",
						   "..\\..\\Controller\\LPCOpen\\LPC43xx_18xx\\chip_43xx_18xx\\config_18xx",
						   "..\\..\\Libs\\TouchGFX\\touchgfx\\framework\\include",
						   "..\\..\\Libs\\TouchGFX\\touchgfx\\framework\\include\\common",
						   "..\\..\\Application",
						   "..\\..\\Application\\gui\\include",
						   "..\\..\\Application\\generated\\fonts\\include",
						   "..\\..\\Application\\generated\\images\\include",
						   "..\\..\\Application\\generated\\texts\\include",
						   "..\\..\\Application\\platform\\os",
						   "..\\..\\Libs\\FreeRTOS\\Source\\include",
						   "..\\..\\Libs\\FreeRTOS\\Source\\portable\\RVDS\\ARM_CM3" ]  

	#has to be a string
	filesDirectory = [ "..\\..\\Application", "..\\..\\Board\\UI.C\\src" ]

	#has to be a string
	exludeSerchDirectories = [ "..\\..\\Application\\simulator" ]

	filesToCompile = Files.Find(filesDirectory, exludeSerchDirectories, [ ".cpp" ])
	filesToCompile.append("..\\..\\main.cpp")

	if ChangedHeader == True:
		Compiler.Compile (compiler, compilationFlags, compilationDefines, includeDirectories, filesToCompile)
	else:
		filesToCompile = Files.getChangedFiles("MD5.json", filesToCompile)
		if len(filesToCompile) != 0:
			Compiler.Compile (compiler, compilationFlags, compilationDefines, includeDirectories, filesToCompile)	


def LinkProjectFiles():
	######################### Link everything #########################

	#has to be string
	linker = "armlink"

	linkerFlags = [ "--no_debug",
					"--cpu Cortex-M3",
					"--strict",
					"--scatter=..\\..\\Config\\build\\sct\\ARMCC.sct",
					"--summary_stderr",
					"--info summarysizes",
					"--map",
					"--xref",
					"--callgraph",
					"--symbols",
					"--info sizes",
					"--info totals",
					"--info unused",
					"--info veneers",
					"--list ..\\..\\Output\\file.map",
					"-o ..\\..\\Output\\file.axf" ]


	filesDirectory = [ "..\\..\\Output\\Obj", "..\\..\\Libs\\TouchGFX\\touchgfx\\lib\\core\\cortex_m3\\Keil"]

	exludeSerchDirectories = []

	filesToLink = Files.Find(filesDirectory, exludeSerchDirectories, [ ".o", ".lib" ])

	Linker.Link(linker, linkerFlags, filesToLink)

######################### Create files #########################

def CreateHex():
	flags = [ "--i32",
			  "--i32combined",
			  "--output=..\\..\\Output\\file.hex",
			  "..\\..\\Output\\file.axf" ]

	Executable.Create("fromelf", flags)

def CreateBin():
	flags = [ "--bin",
			  "--output=..\\..\\Output\\file.bin",
			  "..\\..\\Output\\file.axf" ]

	Executable.Create("fromelf", flags)


#######################################################################

ChangedHeader = CheckHeaderFiles()

BuildStartupFile(ChangedHeader)
BuildLPCOpen(ChangedHeader)
BuildBSP(ChangedHeader)
BuildFreeRTOS(ChangedHeader)
BuildApplication(ChangedHeader)

LinkProjectFiles()

CreateHex()
CreateBin()
