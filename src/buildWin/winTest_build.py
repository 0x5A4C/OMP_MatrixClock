import os
import hashlib
import lib.compile as compiler
import lib.link as linker
import lib.filesFinder as files
import lib.fileCreator as executable

projecPath = "/home/ziemek/Projects/esp-open-rtos_sdk"



def CheckHeaderFiles():
    global projecPath

    iterator = 0;

    filesDirectory = [ "..\\..\\" ]
    exludeSerchDirectories = []

    hushedFiles = {}

    headerFiles = files.Find(filesDirectory, exludeSerchDirectories, [ ".hpp", ".h" ])

    for file in headerFiles:
        hushedFiles[file] = hashlib.md5(open(file, 'rb').read()).hexdigest()

    JsonRead = files.JsonRead("MD5.json")

    if JsonRead == False :
        files.JsonWrite("MD5.json", hushedFiles)
        return True
    else:
        for file in headerFiles:
            if hushedFiles[file] != JsonRead[file]:
                iterator = iterator + 1

    files.JsonWrite("MD5.json",    {**JsonRead, **hushedFiles})

    if iterator != 0:
        return True;
    else:
        return False;



def BuildApplication(ChangedHeader):
    os.environ["PATH"] += os.pathsep + '/usr/include/SDL2'

    global projecPath

    #has to be a string
    compilerCmd = "g++"

    #has to be a list
    #                        "-D="+os.path.join(projecPath, "out"),
    compilationFlags = [
                        '-c',
                        '-Wall',
                        '-pedantic',
                        '-g',
                        '-O0',
                        '-std=c++11'
                        ]

    #has to be a list
    compilationDefines = []

    #has to be a list
    includeDirectories =    [
                            projecPath,
                            os.path.join(projecPath, 'src/'),
                            "/usr/include/SDL2",
                            os.path.join("/home/ziemek/Projects/OMP_ClockMatrixWifi/lib/", "common"),
                            os.path.join("/home/ziemek/Projects/OMP_ClockMatrixWifi/lib/", "ntpTime")
                            ]

    #has to be a string
    filesDirectory  = []
    filesDirectory += [
                      os.path.join(projecPath, "src/empty"),
                      os.path.join(projecPath, "src/res"),
                      ]
    # filesDirectory += [
    #                     os.path.join(projecPath, "app"),
    #                     os.path.join(projecPath, "system")
    #                 ]

    #has to be a string
    exludeSerchDirectories = [ ]

    print(filesDirectory)
    filesToCompile = files.Find(filesDirectory, exludeSerchDirectories, [ ".cpp" ])
    filesToCompile.append(os.path.join(projecPath, "src/app/app.cpp"))
    filesToCompile.append(os.path.join(projecPath, "src/bsp/win/bsp.cpp"))
    filesToCompile.append(os.path.join(projecPath, "src/bsp/win/time.cpp"))
    filesToCompile.append(os.path.join(projecPath, "src/system/framework/sGfx/rect.cpp"))
    filesToCompile.append(os.path.join(projecPath, "src/system/framework/sGfx/line.cpp"))
    filesToCompile.append(os.path.join(projecPath, "src/system/framework/sGfx/bmpXbm.cpp"))
    filesToCompile.append(os.path.join(projecPath, "src/system/framework/sGfx/bmpXbmAnimated.cpp"))
    filesToCompile.append(os.path.join(projecPath, "src/system/framework/sGfx/viewPort.cpp"))
    filesToCompile.append(os.path.join(projecPath, "src/system/framework/sGfx/txt.cpp"))
    filesToCompile.append(os.path.join(projecPath, "src/system/framework/sGfx/easing/easing.cpp"))
    filesToCompile.append(os.path.join(projecPath, "src/main_win.cpp"))
    # filesToCompile.append(os.path.join(projecPath, "lib/common/floatToStr.cpp"))

    print(filesToCompile)
    if ChangedHeader == True:
        compiler.Compile(compilerCmd, compilationFlags, compilationDefines, includeDirectories, filesToCompile)
    else:
        filesToCompile = files.getChangedFiles("MD5.json", filesToCompile)
        if len(filesToCompile) != 0:
            compiler.Compile(compilerCmd, compilationFlags, compilationDefines, includeDirectories, filesToCompile)

def LinkProjectFiles():
    global projecPath
    #has to be string
    linkerCmd = "g++"
    # linkerFlags = ["-o " + os.path.join("/home/ziemek/Projects/OMP_ClockMatrixWifi/", "out", "winTest")]
    linkerFlags = [ "-o " + "winTest " ]

    filesDirectory = [os.path.join(projecPath, "src", "buildWin")]
    print(filesDirectory)
    exludeSerchDirectories = []

    filesToLink = files.Find(filesDirectory, exludeSerchDirectories, [ ".o", ".lib" ])
    print(filesToLink)
    linker.Link(linkerCmd, linkerFlags, filesToLink + ['-L/usr/lib/x86_64-linux-gnu -lSDL2 -lpthread'])



def FormatCode():
    global projecPath

    cmd = 'astyle '
    cmd += '--options='+os.path.join(projecPath, 'src/buildWin/astyle/format.opt ')

    appCmdCpp = projecPath + 'src/app/' +'\'*.cpp\''
    appCmdHpp = projecPath + 'src/app/' +'\'*.hpp\''
    appCmdH = projecPath + 'src/app/' +'\'*.h\''
    os.system(cmd+appCmdCpp)
    os.system(cmd+appCmdHpp)
    os.system(cmd+appCmdH)

    bspCmdCpp = projecPath + 'src/bsp/' +'\'*.cpp\''
    bspCmdHpp = projecPath + 'src/bsp/' +'\'*.hpp\''
    bspCmdH = projecPath + 'src/bsp/' +'\'*.h\''
    os.system(cmd+bspCmdCpp)
    os.system(cmd+bspCmdHpp)
    os.system(cmd+bspCmdH)

    sysCmdCpp = projecPath + 'src/system/' +'\'*.cpp\''
    sysCmdHpp = projecPath + 'src/system/' +'\'*.hpp\''
    sysCmdH = projecPath + 'src/system/' +'\'*.h\''
    os.system(cmd+sysCmdCpp)
    os.system(cmd+sysCmdHpp)
    os.system(cmd+sysCmdH)

    sysCmdCpp = projecPath + 'src/res/' +'\'*.cpp\''
    sysCmdHpp = projecPath + 'src/res/' +'\'*.hpp\''
    sysCmdH = projecPath + 'src/res/' +'\'*.h\''
    os.system(cmd+sysCmdCpp)
    os.system(cmd+sysCmdHpp)
    os.system(cmd+sysCmdH)


print(os.environ["PATH"])
os.environ["PATH"] += os.pathsep + '/usr/include/SDL2'
print(os.environ["PATH"])

FormatCode()

ChangedHeader = CheckHeaderFiles()
BuildApplication(True)
LinkProjectFiles()
