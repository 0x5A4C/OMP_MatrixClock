import os
import hashlib
import json
import io


def JsonRead(file):
	try:
		with open(file) as data_file:
			data_loaded = json.load(data_file)
			return data_loaded
	except:
		return False

def JsonWrite(file, hushedFiles):
	with io.open(file, 'w', encoding='utf8') as outfile:
		str_ = json.dumps(hushedFiles, indent=4, sort_keys=True, separators=(',', ': '), ensure_ascii=False)
		outfile.write(str_)

def getChangedFiles(Jsonfile, filesWithPath):
	
	hushedFiles = {}

	tmpFiles = filesWithPath[:]

	for file in filesWithPath:
		hushedFiles[file] = hashlib.md5(open(file, 'rb').read()).hexdigest()


	JsonData = JsonRead(Jsonfile)

	if JsonData == False :
		JsonWrite(Jsonfile, hushedFiles)
		JsonData = JsonRead(Jsonfile)
	else:
		JsonWrite(Jsonfile,	{**JsonData, **hushedFiles})

	for file in filesWithPath:
		if file in JsonData:
			if hushedFiles[file] == JsonData[file]:
				tmpFiles.remove(file)

	return tmpFiles

def Find(searchDirectories, excludedDirectories, fileTypes):

	filesWithPath = []
	hushedFiles = {}
	HppFiles = []
	hushedHppFiles = {}

	assert len(searchDirectories) != 0, "ERROR: search directories can not be empty"
	assert len(fileTypes) != 0, "ERROR: file types can not be empty"


	for directory in searchDirectories:
		for root, dirs, files in os.walk(directory):
			for file in files:
				for fileType in fileTypes:
					if file.endswith(fileType):
						if not excludedDirectories:
							filesWithPath.append(os.path.join(root, file))
						else:
							for excludedDirectorie in excludedDirectories:
								if excludedDirectorie not in root:
									filesWithPath.append(os.path.join(root, file))

	return filesWithPath