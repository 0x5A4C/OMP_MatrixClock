import os

def getObjName(fileName):
        import os
        return os.path.join("/home/ziemek/Projects/OMP_ClockMatrixWifi/out/", os.path.splitext(os.path.basename(fileName))[0] + ".o")

def getObjNames(fileNames):
	objNames = []

	for fileName in fileNames:
		objNames.append(getObjName(fileName))

	return objNames

def Compile (compiler, compilationFlags, compilationDefines, includeDirectories, filesToCompile):
	print(filesToCompile)

	combinedDefines = ["-D" + s for s in compilationDefines]
	combinedIncludeDirectories = ["-I " + s for s in includeDirectories]

	compilation = []
	compilation.append(compiler)
	compilation = compilation + compilationFlags
	compilation = compilation + combinedDefines
	compilation = compilation + combinedIncludeDirectories
	compilation = compilation + filesToCompile
	# compilation = compilation + ["-o"] + getObjNames(filesToCompile)

	compilationInstruction = " ".join(compilation)

	print(compilationInstruction)
	os.system(compilationInstruction)
