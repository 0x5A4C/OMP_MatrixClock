#include <stdint.h>

/**magic from:
* https://stackoverflow.com/questions/1041866/what-is-the-effect-of-extern-c-in-c
*/

template<char... digits>
struct conv2bin;

template<char high, char... digits>
struct conv2bin<high, digits...> {
    static_assert(high == '0' || high == '1', "no bin num!");
    static uint8_t const value = (high - '0') * (1 << sizeof...(digits)) +
                             conv2bin<digits...>::value;
};

template<char high>
struct conv2bin<high> {
    static_assert(high == '0' || high == '1', "no bin num!");
    static uint8_t const value = (high - '0');
};

template<char... digits>
constexpr uint8_t operator "" _b()
{
    return conv2bin<digits...>::value;
}

// uint8_t  sadBlinkImg[][8] =
// {    // Eye animation frames
// {
//   00100100_b,
//   01000010_b,
//   10000001_b,
//   00111100_b,
//   01111110_b,
//   11111111_b,
//   11111111_b,
//   11111111_b
// }
// };


// Bitmaps are stored in program memory
uint8_t sadBlinkImg[][8] =
{    // Eye animation frames
{
  00100100_b,         // Fully open sad eye
  01000010_b,
  10000001_b,
  00111100_b,
  01111110_b,
  11111111_b,
  11111111_b,
  11111111_b }
,
{
  00100100_b,
  01000010_b,
  10000001_b,
  00000000_b,
  01111110_b,
  11111111_b,
  11111111_b,
  11111111_b }
,
{
  00100100_b,
  01000010_b,
  10000001_b,
  00000000_b,
  00000000_b,
  01111110_b,
  11111111_b,
  11111111_b }
,
{
  00100100_b,
  01000010_b,
  10000001_b,
  00000000_b,
  00000000_b,
  00000000_b,
  01111110_b,
  11111111_b }
,
{
  00100100_b,         // Fully closed sad eye
  01000010_b,
  10000001_b,
  00000000_b,
  00000000_b,
  00000000_b,
  00000000_b,
  11111111_b }

};

uint8_t  // Bitmaps are stored in program memory
blinkImg[][8] = {    // Eye animation frames
{
  00111100_b,         // Fully open eye
  01111110_b,
  11111111_b,
  11111111_b,
  11111111_b,
  11111111_b,
  01111110_b,
  00111100_b }
,
{
  00000000_b,
  01111110_b,
  11111111_b,
  11111111_b,
  11111111_b,
  11111111_b,
  01111110_b,
  00111100_b }
,
{
  00000000_b,
  00000000_b,
  00111100_b,
  11111111_b,
  11111111_b,
  11111111_b,
  00111100_b,
  00000000_b }
,
{
  00000000_b,
  00000000_b,
  00000000_b,
  00111100_b,
  11111111_b,
  01111110_b,
  00011000_b,
  00000000_b }
,
{
  00000000_b,         // Fully closed eye
  00000000_b,
  00000000_b,
  00000000_b,
  10000001_b,
  01111110_b,
  00000000_b,
  00000000_b }
};

uint8_t  // Bitmaps are stored in program memory
happyBlinkImg[][8] = {    // Eye animation frames
{
  00111100_b,         // Fully open happy eye
  01111110_b,
  11111111_b,
  11111111_b,
  11111111_b,
  00000000_b,
  10000001_b,
  01111110_b }
,
{
  00000000_b,
  01111110_b,
  11111111_b,
  11111111_b,
  11111111_b,
  00000000_b,
  10000001_b,
  01111110_b }
,
{
  00000000_b,
  00000000_b,
  01111110_b,
  11111111_b,
  11111111_b,
  00000000_b,
  10000001_b,
  01111110_b }
,
{
  00000000_b,
  00000000_b,
  00000000_b,
  01111110_b,
  11111111_b,
  00000000_b,
  10000001_b,
  01111110_b }
,
{
  00000000_b,         // Fully closed happy eye
  00000000_b,
  00000000_b,
  01111110_b,
  10000001_b,
  00000000_b,
  10000001_b,
  01111110_b }
};

uint8_t // Bitmaps are stored in program memory
annoyedBlinkImg[][8] = {    // Eye animation frames
{
  10000001_b,         // Fully open annoyed eye
  01100110_b,
  00000000_b,
  11111111_b,
  11111111_b,
  11111111_b,
  01111110_b,
  00111100_b }
,
{
  10000001_b,
  01100110_b,
  00000000_b,
  11111111_b,
  11111111_b,
  11111111_b,
  01111110_b,
  00000000_b }
,
{
  10000001_b,
  01100110_b,
  00000000_b,
  11111111_b,
  11111111_b,
  01111110_b,
  00000000_b,
  00000000_b }
,
{
  10000001_b,
  01100110_b,
  00000000_b,
  11111111_b,
  01111110_b,
  00000000_b,
  00000000_b,
  00000000_b }
,
{
  10000001_b,         // Fully closed annoyed eye
  01100110_b,
  00000000_b,
  10000001_b,
  01111110_b,
  00000000_b,
  00000000_b,
  00000000_b }

};
