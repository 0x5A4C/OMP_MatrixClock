#ifndef __APP_HPP
#define __APP_HPP

#include "bsp/bsp.hpp"

namespace App
{
extern "C" void oneMs_tsk();
extern "C" void houndredMs_tsk();
extern "C" void oneS_tsk();

extern "C" void appInit();
extern "C" void appLoop();
}

#endif
