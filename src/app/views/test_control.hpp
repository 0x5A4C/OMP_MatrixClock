
#ifndef TEST_CONTROLL_HPP
#define TEST_CONTROLL_HPP

#include <stdint.h>

#include "system/framework/sGfx/drawable.hpp"
#include "system/framework/sGfx/control.hpp"
#include "system/framework/sGfx/rect.hpp"
#include "system/framework/sGfx/txt.hpp"
#include "system/framework/sGfx/transformXY.hpp"

namespace sGfx
{
class Test_Control: public Control
{
public:
   Test_Control(ViewPort* viewport):
      Control(viewport),
      rf00(Rect(viewport)),
      rf01(RectFill(viewport)),
      txt00(sGfx::TansformXY<sGfx::Txt>(viewport))
   {
      rf00.setParent(this);
      rf01.setParent(this);
      txt00.setParent(this);

      rf00.setColor(sGfx::Color::white());
      rf00.setX(-10);
      rf00.setY(-10);
      rf00.setW(70);
      rf00.setH(70);

      rf01.setColor(sGfx::Color::white());
      rf01.setX(40);
      rf01.setY(40);
      rf01.setW(70);
      rf01.setH(70);

      // txt00.setColor(sGfx::Color::white());
      // txt00.setX(10);
      // txt00.setY(10);
      // txt00.setW(5);
      // txt00.setH(7);
      // txt00.setChar('X');
      //
      // txt00.start(10, 110, 5000, &Easings::easeOutBounce);
   }

   virtual void draw()
   {
      rf00.draw();
      rf01.draw();
      txt00.draw();
   }

   void setColor(Color color)
   {
      this->color = color;
      rf00.setColor(color);
      rf01.setColor(color);
      txt00.setColor(color);
   }

   virtual void tick()
   {
      Drawable::tick();
      txt00.tick();
   }

   virtual bool isRunning()
   {
      return false;
   }

private:
   sGfx::Rect rf00;
   sGfx::RectFill rf01;
   sGfx::TansformXY<sGfx::Txt> txt00;
};
}

#endif
