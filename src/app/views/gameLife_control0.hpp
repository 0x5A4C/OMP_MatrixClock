#ifndef GAMELIFE_CONTROLL_HPP
#define GAMELIFE_CONTROLL_HPP

#include <stdint.h>

#include "system/framework/sGfx/drawable.hpp"
#include "system/framework/sGfx/control.hpp"
#include "system/framework/sGfx/rect.hpp"
#include "system/framework/sGfx/txt.hpp"
#include "system/framework/sGfx/transformXY.hpp"

namespace sGfx
{
class GameLife_Control: public Control
{
public:
   GameLife_Control()
   {
   }

   GameLife_Control(ViewPort* viewport):
      Control(viewport)
   {
   }

   virtual void draw()
   {
      if(ticks > 10)
      {
         ticks = 0;
      }

      setPixel(1,1,sGfx::Color::white());
   }

   virtual void tick()
   {
      Drawable::tick();

      ticks++;
   }

   void setColor(Color color)
   {
      this->color = color;
   }

   virtual bool isRunning()
   {
      return false;
   }

private:
   uint16_t ticks;

};
}

#endif
