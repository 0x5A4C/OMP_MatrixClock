
#ifndef CLOCKTEMP_VIEW_HPP
#define CLOCKTEMP_VIEW_HPP

#include "bsp/bsp.hpp"

// #include <floatToStr.hpp>

#include "system/framework/sGfx/viewPort.hpp"
#include "system/framework/sGfx/txt.hpp"
#include "system/framework/sGfx/rect.hpp"
#include "system/framework/sGfx/line.hpp"
#include "system/framework/sGfx/color.hpp"
#include "system/framework/sGfx/transformXY.hpp"
#include "system/framework/sGfx/easing/easing.hpp"

namespace ClockTemp_View
{
bool enabled = false;

#define FONTWIDTH 7
#define TEXT_X_OFFSET 1
#define TEXT_Y_OFFSET 0

sGfx::ViewPort mainViewPort;
sGfx::RectFill rf(&mainViewPort);
sGfx::Txt dH(&mainViewPort);
sGfx::Txt dh(&mainViewPort);
sGfx::Txt ds(&mainViewPort);
sGfx::Txt dM(&mainViewPort);
sGfx::Txt dm(&mainViewPort);
sGfx::TansformXY<sGfx::RectFill> secMarker(&mainViewPort);

void drawGrText(char* txt)
{
   dH.setChar(txt[0]);
   dh.setChar(txt[1]);
   ds.setChar(txt[2]);
   dM.setChar(txt[3]);
   dm.setChar(txt[4]);
}

void displayTime()
{
   static int dotsOnOff = 0;
   char timeStr[6]="12:34";

   if(Time::tsToHour(Time::timestamp) < 10)
   {
      timeStr[0] = '0';
      timeStr[1] = Time::tsToHour(Time::timestamp) + '0';
   }
   else
   {
      timeStr[0] = Time::tsToHour(Time::timestamp)/10 + '0';
      timeStr[1] = Time::tsToHour(Time::timestamp)%10 + '0';
   }
   timeStr[2] = (dotsOnOff++ & 1) ? ' ' : ':';
   if(Time::tsToMinute(Time::timestamp) < 10)
   {
      timeStr[3] = '0';
      timeStr[4] = Time::tsToMinute(Time::timestamp) + '0';
   }
   else
   {
      timeStr[3] = Time::tsToMinute(Time::timestamp)/10 + '0';
      timeStr[4] = Time::tsToMinute(Time::timestamp)%10 + '0';
   }

   drawGrText(timeStr);

   secMarker.setColor(sGfx::Color::white());
   int16_t markerPos = Easings::linearTween(Time::tsToSecond(Time::timestamp), 0, 8*4, 60);
   secMarker.setX(markerPos);
}

void displayTemp()
{
   char tempString[10] = {0};

   secMarker.setColor(sGfx::Color::black());
//>>-->
   // floatToString(tempString, Bsp::getTemp() ,2);
   drawGrText(tempString);
}

void oneS_tsk()
{
   static int index = 0;

   if(!enabled)
   {
      return;
   }

   if(index++ >= 5)
   {
      index = 0;
      displayTemp();
   }
   else
   {
      displayTime();
   }
}

void houndredMs_tsk()
{
   if(!enabled)
   {
      return;
   }
}

void oneMs_tsk()
{
   if(!enabled)
   {
      return;
   }

   secMarker.tick();
   mainViewPort.update();
}

void init()
{
   if(!enabled)
   {
      return;
   }

   rf.setColor(sGfx::Color::black());
   rf.setX(0);
   rf.setY(0);
   rf.setW(32);
   rf.setH(8);

   dH.setColor(sGfx::Color::white());
   dh.setColor(sGfx::Color::white());
   ds.setColor(sGfx::Color::white());
   dM.setColor(sGfx::Color::white());
   dm.setColor(sGfx::Color::white());
   secMarker.setColor(sGfx::Color::white());

   dH.setX(TEXT_X_OFFSET+0*FONTWIDTH+1);
   dH.setY(TEXT_Y_OFFSET);
   dh.setX(TEXT_X_OFFSET+1*FONTWIDTH+1);
   dh.setY(TEXT_Y_OFFSET);
   ds.setX(TEXT_X_OFFSET+2*FONTWIDTH-2);
   ds.setY(TEXT_Y_OFFSET);
   dM.setX(TEXT_X_OFFSET+3*FONTWIDTH-4);
   dM.setY(TEXT_Y_OFFSET);
   dm.setX(TEXT_X_OFFSET+4*FONTWIDTH-4);
   dm.setY(TEXT_Y_OFFSET);

   secMarker.set(0, 7, 2, 1);

   mainViewPort.add(&dH);
   dH.add(&dh);
   dh.add(&ds);
   ds.add(&dM);
   dM.add(&dm);
   dm.add(&secMarker);
   secMarker.add(&rf);
}
}

#endif
