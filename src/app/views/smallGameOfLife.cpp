#include <inttypes.h>
#include <stdlib.h>

#ifdef __AVR__
#include <util/delay.h>
#endif

#include "bits.h"

#include "smallGameOfLife.hpp"

void (*setLedOn)(uint8_t, uint8_t) = NULL;
void (*setLedOff)(uint8_t, uint8_t) = NULL;

void (*pSGameOfLife_Delay)(uint16_t) = NULL;

typedef uint8_t gameOfLifeBoard[NUMROWS][NUMCOLS];

gameOfLifeBoard gameBoard=
{
   { 0, 0, 0, 0, 0, 0, 0, 0 },
   { 0, 0, 1, 1, 0, 1, 0, 0 },
   { 0, 1, 1, 0, 0, 0, 1, 0 },
   { 0, 0, 1, 0, 0, 0, 0, 0 },
   { 0, 0, 0, 0, 1, 0, 1, 0 },
   { 0, 0, 0, 0, 0, 1, 0, 0 },
   { 0, 0, 0, 0, 0, 0, 0, 0 },
   { 0, 0, 0, 0, 0, 0, 1, 0 }
};

gameOfLifeBoard newGameBoard=
{
   { 0, 0, 0, 0, 0, 0, 0, 0 },
   { 0, 0, 1, 0, 0, 0, 0, 0 },
   { 0, 0, 0, 0, 1, 0, 0, 0 },
   { 0, 0, 0, 0, 0, 0, 0, 0 },
   { 0, 0, 0, 1, 0, 0, 0, 0 },
   { 0, 0, 1, 0, 0, 0, 0, 0 },
   { 0, 1, 0, 1, 0, 1, 0, 0 },
   { 0, 0, 0, 0, 0, 0, 0, 0 },
};

void SGameOfLife_SetDelyCallback(void (*pSGameOfLife_Delay_Callback)(uint16_t))
{
   pSGameOfLife_Delay = pSGameOfLife_Delay_Callback;
}

void SGameOfLife_SetLedOnCallback(void (*setLedOn_Callback)(uint8_t, uint8_t))
{
   setLedOn = setLedOn_Callback;
}

void SGameOfLife_SetLedOffCallback(void (*setLedOff_Callback)(uint8_t, uint8_t))
{
   setLedOff = setLedOff_Callback;
}

void displayGameBoard()
{
   for(uint8_t row = 0; row < NUMROWS; row++)
   {
      for(uint8_t col = 0; col < NUMCOLS; col++)
      {
         setLedOff(row, col);
         if(gameBoard[row][col])
         {
            setLedOn(row, col);
         }
      }
   }
}

/**
 * Counts the number of active cells surrounding the specified cell.
 * Cells outside the board are considered "off"
 * Returns a number in the range of 0 <= n < 9
 */
uint8_t countNeighbors(uint8_t row, uint8_t col)
{
   uint8_t count = 0;
   for(int8_t rowDelta = -1; rowDelta <= 1; rowDelta++)
   {
      for(int8_t colDelta = -1; colDelta <= 1; colDelta++)
      {
         // skip the center cell
         if(!(colDelta == 0 && rowDelta == 0))
         {
            if(isCellAlive(rowDelta + row, colDelta + col))
            {
               count++;
            }
         }
      }
   }
   return count;
}

/**
 * Returns whether or not the specified cell is on.
 * If the cell specified is outside the game board, returns 0x00.
 */
uint8_t isCellAlive(int8_t row, int8_t col)
{
   if(row < 0 || col < 0 || row >= NUMROWS || col >= NUMCOLS)
   {
      return 0x00;
   }

   return (gameBoard[row][col] == 1);
}

/**
 * Encodes the core rules of Conway's Game Of Life, and generates the next iteration of the board.
 * Rules taken from wikipedia.
 */
void calculateNewGameBoard(void)
{
   for(uint8_t row = 0; row < NUMROWS; row++)
   {
      for(uint8_t col = 0; col < NUMCOLS; col++)
      {
         uint8_t numNeighbors = countNeighbors(row, col);

         if(gameBoard[row][col] && numNeighbors < 2)
         {
            // Any live cell with fewer than two live neighbours dies, as if caused by under-population.
            newGameBoard[row][col] = 0x00;
         }
         else if(gameBoard[row][col] && (numNeighbors == 2 || numNeighbors == 3))
         {
            // Any live cell with two or three live neighbours lives on to the next generation.
            newGameBoard[row][col] = 0x01;
         }
         else if(gameBoard[row][col] && numNeighbors > 3)
         {
            // Any live cell with more than three live neighbours dies, as if by overcrowding.
            newGameBoard[row][col] = 0x00;
         }
         else if(!gameBoard[row][col] && numNeighbors == 3)
         {
            // Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
            newGameBoard[row][col] = 0x01;
         }
         else
         {
            // All other cells will remain off
            newGameBoard[row][col] = 0x00;
         }
      }
   }
}

/**
 * Copies the data from the new game board into the current game board array
 */
uint8_t swapGameBoards()
{
   uint8_t stableState = 0x01;

   for(uint8_t row = 0; row < NUMROWS; row++)
   {
      for(uint8_t col = 0; col < NUMCOLS; col++)
      {
         if(gameBoard[row][col] != newGameBoard[row][col])
            stableState = 0x00;
         gameBoard[row][col] = newGameBoard[row][col];
      }
   }

   return stableState;
}

void SGameOfLife_InitBoardWithRand(void)
{
   uint8_t randomNum;

   for(uint8_t row = 0; row < NUMROWS; row++)
   {
      randomNum = (rand()*109+89)%251;
      for(uint8_t col = 0; col < NUMCOLS; col++)
      {
         gameBoard[row][col] = (randomNum & BIT(col)) ? 0x01 : 0x00;
      }
   }
}

void SGameOfLife_ProcessBoard(void)
{
   static uint8_t genCounter = 0x00;
   static uint8_t stableState = 0x01;

   if(stableState || genCounter++ >= MAX_GENERATIONS)
   {
      SGameOfLife_InitBoardWithRand();
      genCounter = 0x00;
   }

//   while((genCounter++ <= MAX_GENERATIONS) && !stableState)
   {
      displayGameBoard();
      calculateNewGameBoard();
      stableState = swapGameBoards();

      pSGameOfLife_Delay(SLIDES_DELAY);
   }
}
