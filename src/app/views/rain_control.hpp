
#ifndef RAIN_CONTROLL_HPP
#define RAIN_CONTROLL_HPP

#include <stdint.h>
#include <stdlib.h>

#include "system/framework/sGfx/control.hpp"
#include "system/framework/sGfx/txt.hpp"

namespace sGfx
{
class Rn_Control: public Control
{
public:
   Rn_Control(ViewPort* viewport): Control(viewport)
   {
   }

   virtual void draw()
   {
      if(ticks > 10)
      {
         ticks = 0;
         frame = (++frame != 0) ? frame : ((rand()*109+89)%251);
      }
      render();
   }

   virtual void tick()
   {
      Drawable::tick();

      ticks++;
   }

private:
   uint16_t w;
   uint16_t h;

   uint16_t ticks;

   uint8_t frame;

   void render()
   {
      // int y;
      //
      // y = frame%19;
      // viewport->setPixel(0,y,color);
      //
      // y = frame%11;
      // viewport->setPixel(2,y,color);
      //
      // y = frame%17;
      // viewport->setPixel(4,y,color);
      //
      // y = frame%12;
      // viewport->setPixel(6,y,color);
      //
      //
      // y = frame%5;
      // viewport->setPixel(1,y,color);
      //
      // y = frame%9;
      // viewport->setPixel(3,y,color);
      //
      // y = frame%21;
      // viewport->setPixel(5,y,color);
      //
      // y = frame%15;
      // viewport->setPixel(7,y,color);

      const uint8_t dividers[] =
      {
         19, 5, 11, 9, 17, 21, 12, 15,
         21, 7, 13, 11, 19, 23, 14, 17,
         17, 3, 9, 7, 15, 19, 10, 13
      };

      uint16_t row = 0;
      uint8_t divIndex = 0;
      for(uint16_t column = 0; column < 32/*viewport->getScreenWidth()*/; column++ )
      {
         row = frame % dividers[divIndex];
         //viewport->setPixel(column, row, color);
         setPixel(column, row, color);
         if(++divIndex > 7*3)
         {
            divIndex = 0;
         }
      }
   }
};

//-------------

class Rain_Control: public Control
{
public:
   Rain_Control(ViewPort* viewport): Control(viewport), rain_c(viewport)
   {
      rain_c.setX(3);
      rain_c.setY(0);
      rain_c.setW(30);
      rain_c.setH(4);

      rain_c.setParent(this);
   }

   virtual void draw()
   {
      rain_c.draw();
   }

   virtual void tick()
   {
      rain_c.tick();
   }

private:
   Rn_Control rain_c;
};
}

#endif
