
#ifndef TEMPDISPLAY_CONTROL_HPP
#define TEMPDISPLAY_CONTROL_HPP

#include <stdint.h>

#include "system/framework/sGfx/drawable.hpp"
#include "system/framework/sGfx/rect.hpp"
#include "twoDigits_control.hpp"

// #include <floatToStr.hpp>
#include "bsp/bsp.hpp"

#define FONTWIDTH 7
#define TEXT_X_OFFSET 1
#define TEXT_Y_OFFSET 0
#define GET_TEMP_DELAY_TICKS 4

namespace sGfx
{
class TempDisplay_Control: public Drawable
{
public:
   TempDisplay_Control(ViewPort* viewport):
      Drawable(viewport),
      tdTtt(viewport),
      tdtTt(viewport),
      tdttT(viewport),
      tdDot(viewport),
      tdUnit(viewport)
   {
      tdTtt.setParent(this);
      tdtTt.setParent(this);
      tdttT.setParent(this);
      tdDot.setParent(this);
      tdUnit.setParent(this);

      tdTtt.setH(7);
      tdtTt.setH(7);
      tdttT.setH(7);
      tdDot.setH(7);
      tdUnit.setH(7);
      tdTtt.setW(5);
      tdtTt.setW(5);
      tdttT.setW(5);
      tdDot.setW(5);
      tdUnit.setW(5);

      tdTtt.setX(TEXT_X_OFFSET+0*FONTWIDTH+1);
      tdtTt.setX(TEXT_X_OFFSET+1*FONTWIDTH+1);
      tdttT.setX(TEXT_X_OFFSET+3*FONTWIDTH-4);
      tdUnit.setX(TEXT_X_OFFSET+4*FONTWIDTH-4);
      tdDot.setX(TEXT_X_OFFSET+2*FONTWIDTH-2);

      tdTtt.setY(TEXT_Y_OFFSET);
      tdtTt.setY(TEXT_Y_OFFSET);
      tdttT.setY(TEXT_Y_OFFSET);
      tdDot.setY(TEXT_Y_OFFSET);
      tdUnit.setY(TEXT_Y_OFFSET);

      ticks = Time::timestamp;
   }
#if 0
   void setX(int16_t x)
   {
      this->x = x;

      tdTtt.setX(x + (TEXT_X_OFFSET+0*FONTWIDTH+1));
      tdtTt.setX(x + (TEXT_X_OFFSET+1*FONTWIDTH+1));
      tdttT.setX(x + (TEXT_X_OFFSET+3*FONTWIDTH-4));
      tdUnit.setX(x + (TEXT_X_OFFSET+4*FONTWIDTH-4));
      tdDot.setX(x + TEXT_X_OFFSET+2*FONTWIDTH-2);
   }

   void setY(int16_t y)
   {
      this->y = y;

      tdTtt.setY(y + TEXT_Y_OFFSET);
      tdtTt.setY(y + TEXT_Y_OFFSET);
      tdttT.setY(y + TEXT_Y_OFFSET);
      tdDot.setY(y + TEXT_Y_OFFSET);
      tdUnit.setY(y + TEXT_Y_OFFSET);
   }

   void setW(int16_t w)
   {
      this->w = w;
   }

   void setH(int16_t h)
   {
      this->h = h;
   }
#endif
   void setColor(Color color)
   {
      tdTtt.setColor(color);
      tdtTt.setColor(color);
      tdttT.setColor(color);
      tdDot.setColor(color);
      tdUnit.setColor(color);
   }

   virtual void tick()
   {
      Drawable::tick();

      tdTtt.tick();
      tdtTt.tick();
      tdttT.tick();
      tdDot.tick();
      tdUnit.tick();

      if(Time::timestamp - ticks > GET_TEMP_DELAY_TICKS)
      {
         displayTemp();
         ticks = Time::timestamp;
      }
   }

   virtual bool isRunning()
   {
      return
         tdTtt.isRunning() ||
         tdtTt.isRunning() ||
         tdttT.isRunning();
   }

   virtual void draw()
   {
      tdTtt.draw();
      tdtTt.draw();
      tdttT.draw();
      tdDot.draw();
      tdUnit.draw();
   }

   void displayTemp()
   {
      int16_t temp;
      int16_t tempHds;
      int16_t tempTens;
      int16_t tempOnes;

      temp = Bsp::getTemp() * 10;
      tempHds = temp / 100;
      tempTens = (temp % 100) / 10;
      tempOnes = (temp % 100) % 10;

      tdTtt.displayDigit(tempHds);
      tdtTt.displayDigit(tempTens);
      tdttT.displayDigit(tempOnes);

      tdDot.setChar('.');
      tdUnit.setChar('C');
   }

private:
   TwoDigits_Control<sGfx::Txt5x7> tdTtt;
   TwoDigits_Control<sGfx::Txt5x7> tdtTt;
   TwoDigits_Control<sGfx::Txt5x7> tdttT;
   Txt tdDot;
   Txt tdUnit;

   int16_t w;
   int16_t h;

   time_t ticks;
};
}

#endif
