
#ifndef FIRE_CONTROLL_HPP
#define FIRE_CONTROLL_HPP

#include <stdint.h>

#include "system/framework/sGfx/txt.hpp"

#define SCREENSIZE_COLS 32
#define SECTION_SIZE 8

namespace sGfx
{
class Fire_Control: public Drawable
{
public:
   Fire_Control(ViewPort* viewport):
      Drawable(viewport)
   {
      // this->viewport = viewport;
   }

   virtual void draw()
   {
      if(ticks > 10)
      {
         ticks = 0;
         ++frame;
      }
      renderFire();
   }

   void setW(int16_t w)
   {
      this->w = w;
   }

   void setH(int16_t h)
   {
      this->h = h;
   }

   virtual void tick()
   {
      Drawable::tick();

      ticks++;
   }

private:
   uint16_t w;
   uint16_t h;

   uint16_t ticks;

   uint8_t frame;

   void renderFire()
   {
      uint16_t r;
      for(uint8_t section = 0; section < 4; section++)
      {
         // fire body
         r = (frame+0)%3;
         viewport->setPixel(0  + (section*SECTION_SIZE), 7-r, color);
         r = (frame+1)%2;
         viewport->setPixel(1  + (section*SECTION_SIZE), 7-r, color);
         r = (frame+0)%2;
         viewport->setPixel(2  + (section*SECTION_SIZE), 7-r, color);
         r = (frame+1)%2;
         viewport->setPixel(3  + (section*SECTION_SIZE), 7-r, color);
         r = (frame+1)%3;
         viewport->setPixel(4  + (section*SECTION_SIZE), 7-r, color);
         r = (frame+0)%2;
         viewport->setPixel(5  + (section*SECTION_SIZE), 7-r, color);
         r = (frame+1)%2;
         viewport->setPixel(6  + (section*SECTION_SIZE), 7-r, color);
         r = (frame+1)%3;
         viewport->setPixel(7  + (section*SECTION_SIZE), 7-r, color);

         r = (frame+1)%5;
         viewport->setPixel(1  + (section*SECTION_SIZE), 7-r, color);
         r = (frame+0)%3;
         viewport->setPixel(2  + (section*SECTION_SIZE), 7-r, color);
         r = (frame+2)%5;
         viewport->setPixel(3  + (section*SECTION_SIZE), 7-r, color);
         r = (frame+1)%5;
         viewport->setPixel(5  + (section*SECTION_SIZE), 7-r, color);
         r = (frame+0)%3;
         viewport->setPixel(6  + (section*SECTION_SIZE), 7-r, color);
         r = (frame+2)%5;
         viewport->setPixel(7  + (section*SECTION_SIZE), 7-r, color);

         r = (frame+4)%4;
         viewport->setPixel(0  + (section*SECTION_SIZE), 7-r, color);
         r = (frame+1)%4;
         viewport->setPixel(1  + (section*SECTION_SIZE), 7-r, color);
         r = (frame+0)%4;
         viewport->setPixel(2  + (section*SECTION_SIZE), 7-r, color);
         r = (frame+3)%4;
         viewport->setPixel(3  + (section*SECTION_SIZE), 7-r, color);
         r = (frame+2)%4;
         viewport->setPixel(4  + (section*SECTION_SIZE), 7-r, color);
         r = (frame+0)%4;
         viewport->setPixel(5  + (section*SECTION_SIZE), 7-r, color);
         r = (frame+3)%4;
         viewport->setPixel(6  + (section*SECTION_SIZE), 7-r, color);
         r = (frame+4)%4;
         viewport->setPixel(7  + (section*SECTION_SIZE), 7-r, color);
         // sparks
         r = (frame+0)%19;
         if (r<SCREENSIZE_COLS) viewport->setPixel(0  + (section*SECTION_SIZE), 7-r, color);
         r = (frame+0)%6;
         if (r<SCREENSIZE_COLS) viewport->setPixel(1  + (section*SECTION_SIZE), 7-r, color);
         r = (frame+0)%7;
         if (r<SCREENSIZE_COLS) viewport->setPixel(2  + (section*SECTION_SIZE), 7-r, color);
         r = (frame+2)%6;
         if (r<SCREENSIZE_COLS) viewport->setPixel(3  + (section*SECTION_SIZE), 7-r, color);
         r = (frame+0)%17;
         if (r<SCREENSIZE_COLS) viewport->setPixel(4  + (section*SECTION_SIZE), 7-r, color);
         r = (frame+0)%9;
         if (r<SCREENSIZE_COLS) viewport->setPixel(5  + (section*SECTION_SIZE), 7-r, color);
         r = (frame+2)%11;
         if (r<SCREENSIZE_COLS) viewport->setPixel(6  + (section*SECTION_SIZE), 7-r, color);
         r = (frame+0)%15;
         if (r<SCREENSIZE_COLS) viewport->setPixel(7  + (section*SECTION_SIZE), 7-r, color);
      }
   }
};
}

#endif
