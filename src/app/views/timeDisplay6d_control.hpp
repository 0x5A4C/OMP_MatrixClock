
#ifndef TIMEDISPLAY6D_CONTROL_HPP
#define TIMEDISPLAY6D_CONTROL_HPP

#include <stdint.h>

#include "system/framework/sGfx/drawable.hpp"
#include "system/framework/sGfx/control.hpp"
#include "system/framework/sGfx/rect.hpp"
#include "twoDigits_control.hpp"

#include "bsp/time.hpp"
using namespace Time;

#define FONTWIDTH_6D 4
#define TEXT_X_OFFSET 1
#define TEXT_Y_OFFSET 0

namespace sGfx
{
class Time6dDisplay_Control: public Control
{
public:
   Time6dDisplay_Control(ViewPort* viewport):
      Control(viewport),
      tdH(viewport),
      tdh(viewport),
      tdM(viewport),
      tdm(viewport),
      tdS(viewport),
      tds(viewport),
      tdD(viewport),
      tdd(viewport),
      pmMarker(viewport),
      tickCounter(0)
   {
     pmMarker.setX(0);
     pmMarker.setY(0);
     pmMarker.setW(2);
     pmMarker.setH(3);

      tdH.setParent(this);
      tdh.setParent(this);
      tdM.setParent(this);
      tdm.setParent(this);
      tdS.setParent(this);
      tds.setParent(this);
      tdD.setParent(this);
      tdd.setParent(this);

      tdH.setH(8);
      tdh.setH(8);
      tdM.setH(8);
      tdm.setH(8);
      tdS.setH(8);
      tds.setH(8);
      tdD.setH(8);
      tdd.setH(8);
      tdH.setW(4);
      tdh.setW(4);
      tdM.setW(4);
      tdm.setW(4);
      tdS.setW(4);
      tds.setW(4);
      tdD.setW(4);
      tdd.setW(4);

      tdH.setX(TEXT_X_OFFSET+0*FONTWIDTH_6D-1);
      tdh.setX(TEXT_X_OFFSET+1*FONTWIDTH_6D-2);
      tdD.setX(TEXT_X_OFFSET+2*FONTWIDTH_6D-3);
      tdM.setX(TEXT_X_OFFSET+3*FONTWIDTH_6D-3);
      tdm.setX(TEXT_X_OFFSET+4*FONTWIDTH_6D-2);
      tdd.setX(TEXT_X_OFFSET+5*FONTWIDTH_6D-3);
      tdS.setX(TEXT_X_OFFSET+6*FONTWIDTH_6D-3);
      tds.setX(TEXT_X_OFFSET+7*FONTWIDTH_6D-2);

      tdH.setY(TEXT_Y_OFFSET);
      tdh.setY(TEXT_Y_OFFSET);
      tdM.setY(TEXT_Y_OFFSET);
      tdm.setY(TEXT_Y_OFFSET);
      tdS.setY(TEXT_Y_OFFSET);
      tds.setY(TEXT_Y_OFFSET);
      tdD.setY(TEXT_Y_OFFSET);
      tdd.setY(TEXT_Y_OFFSET);
   }

   void setColor(Color color)
   {
      this->color = color;
      tdH.setColor(color);
      tdh.setColor(color);
      tdM.setColor(color);
      tdm.setColor(color);
      tdS.setColor(color);
      tds.setColor(color);
      tdD.setColor(color);
      tdd.setColor(color);
   }

   virtual void tick()
   {
      Drawable::tick();

      tdH.tick();
      tdh.tick();
      tdM.tick();
      tdm.tick();
      tdS.tick();
      tds.tick();
      tdD.tick();
      tdd.tick();

      if(Time::timestamp - ticks >= 1)
      {
         displayTime();
         ticks = Time::timestamp;
      }
   }

   virtual bool isRunning()
   {
      return
         tdH.isRunning() ||
         tdh.isRunning() ||
         tdM.isRunning() ||
         tdm.isRunning();
   }

   virtual void draw()
   {
      pmMarker.draw();
      tdH.draw();
      tdh.draw();
      tdM.draw();
      tdm.draw();
      tdS.draw();
      tds.draw();
      tdD.draw();
      tdd.draw();
   }

   void displayTime()
   {
      uint8_t hH = Time::tsToHour(Time::timestamp);
      hH = (hH > 12) ? hH - 12 : hH;
      uint8_t dH = hH/10;
      uint8_t dh = hH%10;
      uint8_t dM = Time::tsToMinute(Time::timestamp)/10;
      uint8_t dm = Time::tsToMinute(Time::timestamp)%10;
      uint8_t dS = Time::tsToSecond(Time::timestamp)/10;
      uint8_t ds = Time::tsToSecond(Time::timestamp)%10;

      if(dH != 0)
      {
        pmMarker.setColor(sGfx::Color::black());
        tdH.setColor(sGfx::Color::white());
        tdH.displayDigit(dH);
      }
     else
     {
       pmMarker.setColor(sGfx::Color::white());
      tdH.setColor(sGfx::Color::black());
      this->viewport->setPixel(0, 0, sGfx::Color::white());
     }

      tdh.displayDigit(dh);
      tdM.displayDigit(dM);
      tdm.displayDigit(dm);
      tdS.displayDigit(dS);
      tds.displayDigit(ds);

      dotsOnOff++;
      tdD.setChar((dotsOnOff & 1) ? ' ' : ':');
      tdd.setChar((dotsOnOff & 1) ? ' ' : ':');
   }

private:
   TwoDigits_Control<sGfx::Txt4x8> tdH;
   TwoDigits_Control<sGfx::Txt4x8> tdh;
   TwoDigits_Control<sGfx::Txt4x8> tdM;
   TwoDigits_Control<sGfx::Txt4x8> tdm;
   TwoDigits_Control<sGfx::Txt4x8> tdS;
   TwoDigits_Control<sGfx::Txt4x8> tds;

   Txt tdD;
   Txt tdd;

   sGfx::RectFill pmMarker;

   int16_t w;
   int16_t h;

   uint8_t dotsOnOff = 0;

   time_t tickCounter;
   time_t ticks;
};
}

#endif
