/*
 * SmallGameOfLifeApp.h
 *
 *  Created on: 03-05-2013
 *      Author: ziemek
 */

#include "AppConfig.h"
#ifdef GAmeOfLife

#ifndef SMALLGAMEOFLIFEAPP_H_
#define SMALLGAMEOFLIFEAPP_H_

#define  NUMROWS  8
#define  NUMCOLS  8
#define  MICROS   100;

#define MAX_GENERATIONS 50
#define SLIDES_DELAY 250

#endif /* SMALLGAMEOFLIFEAPP_H_ */

#endif
