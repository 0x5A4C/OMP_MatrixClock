
#ifndef TIMEDISPLAY_CONTROL_HPP
#define TIMEDISPLAY_CONTROL_HPP

#include <stdint.h>

#include "system/framework/sGfx/drawable.hpp"
#include "system/framework/sGfx/control.hpp"
#include "system/framework/sGfx/rect.hpp"
#include "twoDigits_control.hpp"

#include "bsp/time.hpp"
using namespace Time;

#define FONTWIDTH 7
#define TEXT_X_OFFSET 1
#define TEXT_Y_OFFSET 0

namespace sGfx
{
class TimeDisplay_Control: public Control
{
public:
   TimeDisplay_Control(ViewPort* viewport):
      Control(viewport),
      tdH(viewport),
      tdh(viewport),
      tdM(viewport),
      tdm(viewport),
      tds(viewport),
      secMarker(viewport),
      tickCounter(0)
   {
      tdH.setParent(this);
      tdh.setParent(this);
      tdM.setParent(this);
      tdm.setParent(this);
      tds.setParent(this);
      secMarker.setParent(this);

      tdH.setH(7);
      tdh.setH(7);
      tdM.setH(7);
      tdm.setH(7);
      tds.setH(7);
      tdH.setW(5);
      tdh.setW(5);
      tdM.setW(5);
      tdm.setW(5);
      tds.setW(5);

      tdH.setX(TEXT_X_OFFSET+0*FONTWIDTH+1);
      tdh.setX(TEXT_X_OFFSET+1*FONTWIDTH+1);
      tdM.setX(TEXT_X_OFFSET+3*FONTWIDTH-4);
      tdm.setX(TEXT_X_OFFSET+4*FONTWIDTH-4);
      tds.setX(TEXT_X_OFFSET+2*FONTWIDTH-2);

      tdH.setY(TEXT_Y_OFFSET);
      tdh.setY(TEXT_Y_OFFSET);
      tdM.setY(TEXT_Y_OFFSET);
      tdm.setY(TEXT_Y_OFFSET);
      tds.setY(TEXT_Y_OFFSET);

      secMarker.set(0, 7, 2, 1);
   }

   void setColor(Color color)
   {
      this->color = color;
      tdH.setColor(color);
      tdh.setColor(color);
      tdM.setColor(color);
      tdm.setColor(color);
      tds.setColor(color);
      secMarker.setColor(color);
   }

   virtual void tick()
   {
      Drawable::tick();

      tdH.tick();
      tdh.tick();
      tdM.tick();
      tdm.tick();
      tds.tick();
      secMarker.tick();

      if(Time::timestamp - ticks >= 1)
      {
         displayTime();
         ticks = Time::timestamp;
      }
   }

   virtual bool isRunning()
   {
      return
         tdH.isRunning() ||
         tdh.isRunning() ||
         tdM.isRunning() ||
         tdm.isRunning();
   }

   virtual void draw()
   {
      tdH.draw();
      tdh.draw();
      tdM.draw();
      tdm.draw();
      tds.draw();
      secMarker.draw();
   }

   void displayTime()
   {
      uint8_t dH = Time::tsToHour(Time::timestamp)/10;
      uint8_t dh = Time::tsToHour(Time::timestamp)%10;
      uint8_t dM = Time::tsToMinute(Time::timestamp)/10;
      uint8_t dm = Time::tsToMinute(Time::timestamp)%10;

      tdH.displayDigit(dH);
      tdh.displayDigit(dh);
      tdM.displayDigit(dM);
      tdm.displayDigit(dm);

      tds.setChar((dotsOnOff++ & 1) ? ' ' : ':');

      secMarker.setColor(sGfx::Color::white());
      int16_t markerPos = Easings::linearTween(Time::tsToSecond(Time::timestamp), 0, 8*4, 60);
      // secMarker.set(x + markerPos, y + 7, 2, 1);
      secMarker.setX(markerPos);
      secMarker.setY(7);
   }

private:
   TwoDigits_Control<sGfx::Txt5x7> tdH;
   TwoDigits_Control<sGfx::Txt5x7> tdh;
   TwoDigits_Control<sGfx::Txt5x7> tdM;
   TwoDigits_Control<sGfx::Txt5x7> tdm;
   Txt tds;
   RectFill secMarker;

   int16_t w;
   int16_t h;

   uint8_t dotsOnOff = 0;

   time_t tickCounter;
   time_t ticks;
};
}

#endif
