
#ifndef BASICTIMEDISP_CTRL_HPP
#define BASICTIMEDISP_CTRL_HPP

#include <stdint.h>

#include "system/framework/sGfx/drawable.hpp"
#include "system/framework/sGfx/txt.hpp"

#define FONTWIDTH 7
#define TEXT_X_OFFSET 1
#define TEXT_Y_OFFSET 0

namespace sGfx
{
class BasicTimeDisp_Ctrl: public Control
{
public:
   BasicTimeDisp_Ctrl(ViewPort* viewport):
      Control(viewport),
      tdH(viewport),
      tdh(viewport),
      tdM(viewport),
      tdm(viewport),
      tds(viewport),
      secMarker(viewport)
   {
      tdH.setParent(this);
      tdh.setParent(this);
      tdM.setParent(this);
      tdm.setParent(this);
      tds.setParent(this);
      secMarker.setParent(this);

      tdH.setH(7);
      tdh.setH(7);
      tdM.setH(7);
      tdm.setH(7);
      tds.setH(7);
      tdH.setW(5);
      tdh.setW(5);
      tdM.setW(5);
      tdm.setW(5);
      tds.setW(5);

      tdH.setX(TEXT_X_OFFSET+0*FONTWIDTH+1);
      tdh.setX(TEXT_X_OFFSET+1*FONTWIDTH+1);
      tdM.setX(TEXT_X_OFFSET+3*FONTWIDTH-4);
      tdm.setX(TEXT_X_OFFSET+4*FONTWIDTH-4);
      tds.setX(TEXT_X_OFFSET+2*FONTWIDTH-2);

      tdH.setY(TEXT_Y_OFFSET);
      tdh.setY(TEXT_Y_OFFSET);
      tdM.setY(TEXT_Y_OFFSET);
      tdm.setY(TEXT_Y_OFFSET);
      tds.setY(TEXT_Y_OFFSET);

      secMarker.set(0, 7, 2, 1);
   }

   void setColor(Color color)
   {
      this->color = color;
      tdH.setColor(color);
      tdh.setColor(color);
      tdM.setColor(color);
      tdm.setColor(color);
      tds.setColor(color);
      secMarker.setColor(color);
   }

   virtual void tick()
   {
      Drawable::tick();

      tdH.tick();
      tdh.tick();
      tdM.tick();
      tdm.tick();
      tds.tick();
      secMarker.tick();

      if(Time::timestamp - ticks >= 1)
      {
         displayTime();
         ticks = Time::timestamp;
      }
   }

   virtual void draw()
   {
      tdH.draw();
      tdh.draw();
      tdM.draw();
      tdm.draw();
      tds.draw();
      secMarker.draw();
   }

   void displayTime()
   {
      static int dotsOnOff = 0;
      char timeStr[6]="12:34";

      if(Time::tsToHour(Time::timestamp) < 10)
      {
         timeStr[0] = '0';
         timeStr[1] = Time::tsToHour(Time::timestamp) + '0';
      }
      else
      {
         timeStr[0] = Time::tsToHour(Time::timestamp)/10 + '0';
         timeStr[1] = Time::tsToHour(Time::timestamp)%10 + '0';
      }
      timeStr[2] = (dotsOnOff++ & 1) ? ' ' : ':';
      if(Time::tsToMinute(Time::timestamp) < 10)
      {
         timeStr[3] = '0';
         timeStr[4] = Time::tsToMinute(Time::timestamp) + '0';
      }
      else
      {
         timeStr[3] = Time::tsToMinute(Time::timestamp)/10 + '0';
         timeStr[4] = Time::tsToMinute(Time::timestamp)%10 + '0';
      }

      drawGrText(timeStr);

      secMarker.setColor(sGfx::Color::white());
      int16_t markerPos = Easings::linearTween(Time::tsToSecond(Time::timestamp), 0, 8*4, 60);
      secMarker.setX(markerPos);
   }

private:
   Txt tdH;
   Txt tdh;
   Txt tdM;
   Txt tdm;
   Txt tds;
   RectFill secMarker;

   uint8_t dotsOnOff = 0;

   time_t ticks;

   void drawGrText(char* txt)
   {
      tdH.setChar(txt[0]);
      tdh.setChar(txt[1]);
      tds.setChar(txt[2]);
      tdM.setChar(txt[3]);
      tdm.setChar(txt[4]);
   }
};
}

#endif
