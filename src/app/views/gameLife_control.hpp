#ifndef GAMELIFE_CONTROLL_HPP
#define GAMELIFE_CONTROLL_HPP

#include <stdint.h>
#include <cstdlib>

#include "system/framework/sGfx/drawable.hpp"
#include "system/framework/sGfx/control.hpp"
#include "system/framework/sGfx/rect.hpp"
#include "system/framework/sGfx/txt.hpp"
#include "system/framework/sGfx/transformXY.hpp"

#include "bits.h"

#define  NUMROWS  (8*4)
#define  NUMCOLS  (8)

#define MAX_GENERATIONS 50

namespace sGfx
{
class GameLife_Control: public Control
{
public:
   GameLife_Control()
   {
   }

   GameLife_Control(ViewPort* viewport):
      Control(viewport)
   {
   }

   virtual void draw()
   {
      ticks++;
      if(ticks > 40)
      {
         ticks = 0;
         processBoard();
      }

      displayGameBoard();
   }

   virtual void tick()
   {
      Drawable::tick();

      ticks++;
   }

   void setColor(Color color)
   {
      this->color = color;
   }

   virtual bool isRunning()
   {
      return false;
   }

private:
   uint16_t ticks;

   typedef uint8_t gameOfLifeBoard[NUMROWS][NUMCOLS];

   gameOfLifeBoard gameBoard=
   {
      { 0, 0, 0, 0, 0, 0, 0, 0 },
      { 0, 0, 1, 1, 0, 1, 0, 0 },
      { 0, 1, 1, 0, 0, 0, 1, 0 },
      { 0, 0, 1, 0, 0, 0, 0, 0 },
      { 0, 0, 0, 0, 1, 0, 1, 0 },
      { 0, 0, 0, 0, 0, 1, 0, 0 },
      { 0, 0, 0, 0, 0, 0, 0, 0 },
      { 0, 0, 0, 0, 0, 0, 1, 0 }
   };

   gameOfLifeBoard newGameBoard=
   {
      { 0, 0, 0, 0, 0, 0, 0, 0 },
      { 0, 0, 1, 0, 0, 0, 0, 0 },
      { 0, 0, 0, 0, 1, 0, 0, 0 },
      { 0, 0, 0, 0, 0, 0, 0, 0 },
      { 0, 0, 0, 1, 0, 0, 0, 0 },
      { 0, 0, 1, 0, 0, 0, 0, 0 },
      { 0, 1, 0, 1, 0, 1, 0, 0 },
      { 0, 0, 0, 0, 0, 0, 0, 0 },
   };

   /**
    * Copies the data from the new game board into the current game board array
    */
   uint8_t swapGameBoards()
   {
      uint8_t stableState = 0x01;

      for(uint8_t row = 0; row < NUMROWS; row++)
      {
         for(uint8_t col = 0; col < NUMCOLS; col++)
         {
            if(gameBoard[row][col] != newGameBoard[row][col])
               stableState = 0x00;
            gameBoard[row][col] = newGameBoard[row][col];
         }
      }

      return stableState;
   }

   /**
   * Returns whether or not the specified cell is on.
   * If the cell specified is outside the game board, returns 0x00.
   */
   uint8_t isCellAlive(int8_t row, int8_t col)
   {
      if(row < 0 || col < 0 || row >= NUMROWS || col >= NUMCOLS)
      {
         return 0x00;
      }

      return (gameBoard[row][col] == 1);
   }

   /**
    * Counts the number of active cells surrounding the specified cell.
    * Cells outside the board are considered "off"
    * Returns a number in the range of 0 <= n < 9
    */
   uint8_t countNeighbors(uint8_t row, uint8_t col)
   {
      uint8_t count = 0;
      for(int8_t rowDelta = -1; rowDelta <= 1; rowDelta++)
      {
         for(int8_t colDelta = -1; colDelta <= 1; colDelta++)
         {
            // skip the center cell
            if(!(colDelta == 0 && rowDelta == 0))
            {
               if(isCellAlive(rowDelta + row, colDelta + col))
               {
                  count++;
               }
            }
         }
      }
      return count;
   }

   /**
    * Encodes the core rules of Conway's Game Of Life, and generates the next iteration of the board.
    * Rules taken from wikipedia.
    */
   void calculateNewGameBoard(void)
   {
      for(uint8_t row = 0; row < NUMROWS; row++)
      {
         for(uint8_t col = 0; col < NUMCOLS; col++)
         {
            uint8_t numNeighbors = countNeighbors(row, col);

            if(gameBoard[row][col] && numNeighbors < 2)
            {
               // Any live cell with fewer than two live neighbours dies, as if caused by under-population.
               newGameBoard[row][col] = 0x00;
            }
            else if(gameBoard[row][col] && (numNeighbors == 2 || numNeighbors == 3))
            {
               // Any live cell with two or three live neighbours lives on to the next generation.
               newGameBoard[row][col] = 0x01;
            }
            else if(gameBoard[row][col] && numNeighbors > 3)
            {
               // Any live cell with more than three live neighbours dies, as if by overcrowding.
               newGameBoard[row][col] = 0x00;
            }
            else if(!gameBoard[row][col] && numNeighbors == 3)
            {
               // Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
               newGameBoard[row][col] = 0x01;
            }
            else
            {
               // All other cells will remain off
               newGameBoard[row][col] = 0x00;
            }
         }
      }
   }

   void displayGameBoard(void)
   {
      for(uint8_t row = 0; row < NUMROWS; row++)
      {
         for(uint8_t col = 0; col < NUMCOLS; col++)
         {
            // setPixel(row, col, sGfx::Color::black());
            if(gameBoard[row][col])
            {
               setPixel(row, col, sGfx::Color::white());
            }
         }
      }
   }

   void initBoardWithRand(void)
   {
      uint8_t randomNum;

      for(uint8_t row = 0; row < NUMROWS; row++)
      {
         randomNum = (rand()*109+89)%251;
         for(uint8_t col = 0; col < NUMCOLS; col++)
         {
            gameBoard[row][col] = (randomNum & BIT(col)) ? 0x01 : 0x00;
         }
      }
   }

   void processBoard(void)
   {
      static uint8_t genCounter = 0x00;
      static uint8_t stableState = 0x01;

      if(stableState || genCounter++ >= MAX_GENERATIONS)
      {
         initBoardWithRand();
         genCounter = 0x00;
      }

      calculateNewGameBoard();
      stableState = swapGameBoards();
   }
};
}

#endif
