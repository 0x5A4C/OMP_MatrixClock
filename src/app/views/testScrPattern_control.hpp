#ifndef TEST_SCR_CONTROLL_HPP
#define TEST_SCR_CONTROLL_HPP

#include <stdint.h>

#include "system/framework/sGfx/drawable.hpp"
#include "system/framework/sGfx/control.hpp"
#include "system/framework/sGfx/rect.hpp"
#include "system/framework/sGfx/txt.hpp"
#include "system/framework/sGfx/transformXY.hpp"

namespace sGfx
{
class TestScrPattern_Control: public Control
{
public:
   TestScrPattern_Control()
   {
   }

   TestScrPattern_Control(ViewPort* viewport):
      Control(viewport),
      background(RectFill(viewport))
   {
      background.setParent(this);

      background.setColor(sGfx::Color::black());
      background.setX(0);
      background.setY(0);
      background.setW(32);
      background.setH(8);
   }

   virtual void draw()
   {
      // background.draw();
   }

   void setColor(Color color)
   {
      this->color = color;
      background.setColor(color);
   }

   virtual void tick()
   {
      Drawable::tick();
   }

   virtual bool isRunning()
   {
      return false;
   }

   void drawOddTestPattern(void)
   {
      drawTestPattern(true);
   }

   void drawEvenTestPattern(void)
   {
      drawTestPattern(false);
   }

private:
   sGfx::RectFill background;

   void drawTestPattern(bool pos = true)
   {
      sGfx::Color colorA;
      sGfx::Color colorB;

      if(pos)
      {
         colorA = sGfx::Color::white();
         colorB = sGfx::Color::black();
      }
      else
      {
         colorA = sGfx::Color::black();
         colorB = sGfx::Color::white();
      }

      background.draw();

      for(uint16_t i = 0; i < viewport->getWidth(); i++)
      {
         for(uint16_t j = 0; j < viewport->getHeight(); j++)
         {
            if(j & 1)
            {
               setPixel(i, j, (i & 1) ? colorA : colorB);
            }
            else
            {
               setPixel(i, j, (i & 1) ? colorB : colorA);
            }
         }
      }
   }
};
}

#endif
