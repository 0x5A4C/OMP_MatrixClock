#ifndef SPRITE_CONTROLL_HPP
#define SPRITE_CONTROLL_HPP

#include <stdint.h>

#include "system/framework/sGfx/drawable.hpp"
#include "system/framework/sGfx/control.hpp"
#include "system/framework/sGfx/rect.hpp"
#include "system/framework/sGfx/txt.hpp"
#include "system/framework/sGfx/transformXY.hpp"

#include "timer.hpp"
#include "bits.h"

namespace sGfx
{
class Sprite_Control: public Control
{
public:
   Sprite_Control(ViewPort* viewport): Control(viewport)
   {
      aTimer.start();

      frame = 0;
   }

   virtual void draw()
   {
      renederSprite(pSprites);
   }

   virtual void tick()
   {
      Drawable::tick();
   }

   void setColor(Color color)
   {
      this->color = color;
   }

   virtual bool isRunning()
   {
      return false;
   }

   void setSprites(uint8_t* pAnimSprites, uint16_t numOfAnimFrames, uint16_t midAnimFrameDelay)
   {
      pSprites = pAnimSprites;
      numOfFrames = numOfAnimFrames;
      midFrameDelay = midAnimFrameDelay;
   }

private:
   Timer aTimer;

   uint8_t* pSprites;
   uint16_t numOfFrames;
   uint16_t midFrameDelay;

   uint16_t frame;

   uint16_t sizeOfRow(void)
   {
      return 1;
   }

   uint16_t sizeOfFrame(void)
   {
      return sizeOfRow() * 8;
   }

   void renderRaw(uint8_t *pRow, uint16_t rowIndex)
   {
      for(uint8_t i = 0; i < 8; i++)
      {
         setPixel(i, rowIndex, BITVAL(*pRow, i) ? sGfx::Color::white() : sGfx::Color::black());
      }
   }

   void renderFrame(uint8_t *pFrame, uint16_t frameIndex)
   {
      for(uint8_t i = 0; i < 8; i++)
      {
         renderRaw((pFrame + (frameIndex * sizeOfFrame())) + i, i);
      }
   }

   void renederSprite(uint8_t *pSprite)
   {
      if(aTimer.getTicks() > midFrameDelay)
      {
         aTimer.reset();
         frame++;
         if(frame > numOfFrames - 1)
         {
            frame = 0;
         }
      }
      renderFrame(pSprite, frame);
   }
};
}

#endif
