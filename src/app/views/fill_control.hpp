
#ifndef FILLUPDOWN_CONTROLL_HPP
#define FILLUPDOWN_CONTROLL_HPP

#include <stdint.h>

#include "system/framework/sGfx/easing/easing.hpp"
#include "system/framework/sGfx/viewPort.hpp"
#include "system/framework/sGfx/rect.hpp"

#define SCREENSIZE_COLS 32
#define SCREENSIZE_ROWS 8

#define SECTION_SIZE 8

namespace sGfx
{
class Fill_Control: public Drawable
{
public:
   Fill_Control(ViewPort* viewport):
      Drawable(viewport),
      rf(sGfx::RectFill (viewport))
   {
      // this->viewport = viewport;
   }

   virtual void draw()
   {
      rf.draw();
   }

   void setW(int16_t w)
   {
      this->w = w;
   }

   void setH(int16_t h)
   {
      this->h = h;
   }

   virtual void tick()
   {
      Drawable::tick();

      render01();

      ticks++;
   }

private:
   sGfx::RectFill rf;
   uint8_t buffer[SCREENSIZE_COLS][SCREENSIZE_ROWS];

   uint16_t w;
   uint16_t h;

   uint16_t ticks;
   uint8_t frame;

   uint8_t row = 0;
   uint8_t col = 0;

   uint16_t duration = 5000;

   void render00()
   {
      rf.setX(0);
      rf.setY(0);
      rf.setW(Easings::linearTween(ticks, 0, SCREENSIZE_COLS, duration));
      rf.setH(Easings::linearTween(ticks, 0, SCREENSIZE_ROWS, duration));
   }

   void render01()
   {
      //return c*t/d + b;
      //  t, float b, float c, float d);
      rf.setX(Easings::Easings::easeOutBounce(ticks, 2, SCREENSIZE_COLS/2, duration));
      rf.setY(Easings::Easings::easeOutBounce(ticks, 2, SCREENSIZE_ROWS/2, duration));
      rf.setW(Easings::Easings::easeOutBounce(ticks, 1, SCREENSIZE_COLS,  duration));
      rf.setH(Easings::Easings::easeOutBounce(ticks, 1, SCREENSIZE_ROWS,  duration));
   }
};
}

#endif
