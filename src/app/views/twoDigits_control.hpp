
#ifndef TWODIGITS_CONTROLL_HPP
#define TWODIGITS_CONTROLL_HPP

#include <stdint.h>

#include "system/framework/sGfx/drawable.hpp"
#include "system/framework/sGfx/control.hpp"
#include "system/framework/sGfx/txt.hpp"
#include "system/framework/sGfx/transformXY.hpp"

namespace sGfx
{
template<class T>
class TwoDigits_Control: public Control
{
public:
   TwoDigits_Control(ViewPort* viewport):
      Control(viewport),
      digitDisplayed('0'),
      digitToDisplay('0'),
      dUp(sGfx::TansformXY<T>(viewport)),
      dLow(sGfx::TansformXY<T>(viewport))
   {
      dUp.setParent(this);
      dLow.setParent(this);

      dUp.setX(0);
      dLow.setX(0);
      dUp.setY(0);
      dLow.setY(0+8);

      dUp.setH(dUp.getHeight());
      dLow.setH(dLow.getHeight());
      dUp.setW(dUp.getWidth());
      dLow.setW(dUp.getWidth());
   }

   virtual void draw()
   {
      dUp.draw();
      dLow.draw();
   }

   void displayDigit(uint8_t digit)
   {
      digitToDisplay = digit + 0;
   }

   void setColor(Color color)
   {
      this->color = color;
      dUp.setColor(color);
      dLow.setColor(color);
   }

   virtual void tick()
   {
      Drawable::tick();

      dUp.tick();
      dLow.tick();

      if(!isRunning())
      {
         if(digitDisplayed != digitToDisplay)
         {
            digitDisplayed++;
            digitDisplayed = (digitDisplayed > 9 ? 0 : digitDisplayed);

            if(digitDisplayed >= 1 && digitDisplayed <= 9)
            {
               dUp.setChar('0' + digitDisplayed - 1);
               dLow.setChar('0' + digitDisplayed);
            }
            else if(digitDisplayed == 0)
            {
               dUp.setChar('9');
               dLow.setChar('0');
            }

            dUp.setY(0);
            dLow.setY(0+8);

            dUp.start(0, 0-8, 50, &Easings::easeOutBounce);
            dLow.start(0, 0+0, 50, &Easings::easeOutBounce);
         }
      }
   }

   virtual bool isRunning()
   {
      return(dUp.isRunning() || dLow.isRunning());
   }

private:
   uint8_t digitDisplayed;
   uint8_t digitToDisplay;

   sGfx::TansformXY<T> dUp;
   sGfx::TansformXY<T> dLow;

};
}

#endif
