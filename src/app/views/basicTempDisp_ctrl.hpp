
#ifndef BASICTEMPDISP_CTRL_HPP
#define BASICTEMPDISP_CTRL_HPP

#include <stdint.h>

// #include <floatToStr.hpp>
// #include <time.hpp>

#include "system/framework/sGfx/control.hpp"
#include "system/framework/sGfx/txt.hpp"

#define FONTWIDTH 7
#define TEXT_X_OFFSET 1
#define TEXT_Y_OFFSET 0

#define GET_TEMP_DELAY_TICKS 4

namespace sGfx
{
class BasicTempDisp_Ctrl: public Control
{
public:
   BasicTempDisp_Ctrl(ViewPort* viewport):
      Control(viewport),
      tdH(viewport),
      tdh(viewport),
      tdM(viewport),
      tdm(viewport),
      tds(viewport)
   {
      tdH.setParent(this);
      tdh.setParent(this);
      tdM.setParent(this);
      tdm.setParent(this);
      tds.setParent(this);

      tdH.setH(7);
      tdh.setH(7);
      tdM.setH(7);
      tdm.setH(7);
      tds.setH(7);
      tdH.setW(10);
      tdh.setW(10);
      tdM.setW(10);
      tdm.setW(10);
      tds.setW(10);

      tdH.setX(TEXT_X_OFFSET+0*FONTWIDTH+1);
      tdh.setX(TEXT_X_OFFSET+1*FONTWIDTH+1);
      tdM.setX(TEXT_X_OFFSET+3*FONTWIDTH-4);
      tdm.setX(TEXT_X_OFFSET+4*FONTWIDTH-4);
      tds.setX(TEXT_X_OFFSET+2*FONTWIDTH-2);

      tdH.setY(TEXT_Y_OFFSET);
      tdh.setY(TEXT_Y_OFFSET);
      tdM.setY(TEXT_Y_OFFSET);
      tdm.setY(TEXT_Y_OFFSET);
      tds.setY(TEXT_Y_OFFSET);

      ticks = Time::timestamp;
   }

   void setColor(Color color)
   {
      this->color = color;
      tdH.setColor(color);
      tdh.setColor(color);
      tdM.setColor(color);
      tdm.setColor(color);
      tds.setColor(color);
   }

   virtual void tick()
   {
      Drawable::tick();

      tdH.tick();
      tdh.tick();
      tdM.tick();
      tdm.tick();
      tds.tick();

      if(Time::timestamp - ticks > GET_TEMP_DELAY_TICKS)
      {
         displayTemp();
         ticks = Time::timestamp;
      }
   }

   virtual void draw()
   {
      tdH.draw();
      tdh.draw();
      tdM.draw();
      tdm.draw();
      tds.draw();
   }

   void displayTemp()
   {
      char tempString[10] = "25.6";

      // floatToString(tempString, Bsp::getTemp() ,2);
      drawGrText(tempString);
   }

private:
   Txt tdH;
   Txt tdh;
   Txt tdM;
   Txt tdm;
   Txt tds;

   time_t ticks;

   void drawGrText(char* txt)
   {
      tdH.setChar(txt[0]);
      tdh.setChar(txt[1]);
      tds.setChar(txt[2]);
      tdM.setChar(txt[3]);
      tdm.setChar(txt[4]);
   }
};
}

#endif
