
#ifndef EYES_CONTROLL_HPP
#define EYES_CONTROLL_HPP

#include <stdint.h>
#include <stdlib.h>

#include <tuple>

#include "system/framework/sGfx/control.hpp"
#include "system/framework/sGfx/txt.hpp"

#include "bsp/bsp.hpp"

#include "eyes_control_bmp.hpp"

extern "C" uint8_t const sadBlinkImg[][8];
extern "C" uint8_t const blinkImg[][8];
extern "C" uint8_t const happyBlinkImg[][8];
extern "C" uint8_t const annoyedBlinkImg[][8];

namespace sGfx
{

class Eye_Control: public Control
{
public:
  Eye_Control(ViewPort* viewport):
    Control(viewport),
    rf(viewport),
    mood(moods::neutralMood),
    blinkIndex { 1, 2, 3, 4, 3, 2, 1 }
  {
    rf.setParent(this);
    rf.setX(5);
    rf.setY(3);
    rf.setH(2);
    rf.setW(2);
    rf.setColor(sGfx::Color::black());
  }

  virtual void draw()
  {
    process(mood);

    rf.draw();
    checkTaps();
  }

  virtual void tick()
  {
    ticks++;
    rf.tick();
  }

  void setColor(Color color)
  {
     this->color = color;
  }

  void tap(void)
  {
    tapNum++;
  }

private:
  int tapNum = 15;
  uint64_t checkMillis = 0;
  uint64_t tapMillis = 0;
  uint64_t songMillis = 0;
  uint64_t currentMillis = 0;
  uint64_t gameMillis = 0;
  uint64_t previousMillis = 0;
  const uint64_t decay = 1000;//30000;

  sGfx::RectFill rf;

  enum class moods: uint8_t
  {
    sadMood,
    neutralMood,
    happyMOod,
    annoyedMood
  };
  moods mood;

  uint8_t blinkIndex[7] ;  // Blink bitmap sequence
  uint8_t blinkCountdown = 100; // Countdown to next blink (in frames)
  uint8_t gazeCountdown  =  75; // Countdown to next eye movement
  uint8_t gazeFrames     =  50; // Duration of eye movement (smaller = faster)
  int8_t  eyeX = 3;
  int8_t  eyeY = 3;   // Current eye position
  int8_t  newX = 3;
  int8_t  newY = 3;   // Next eye position
  int8_t  dX   = 0;
  int8_t  dY   = 0;   // Distance from prior to new position

  uint64_t ticks = 0;

  void gazeProcess(moods mood)
  {
    // Add a pupil (2x2 black square) atop the blinky eyeball bitmap.
    // Periodically, the pupil moves to a new position...
    if(--gazeCountdown <= gazeFrames)
    {
      // Eyes are in motion - draw pupil at interim position
      rf.setX(newX - (dX * gazeCountdown / gazeFrames));
      rf.setY(newY - (dY * gazeCountdown / gazeFrames));

      if(gazeCountdown == 0)
      {    // Last frame?
        eyeX = newX;
        eyeY = newY; // Yes.  What's new is old, then...
        do
        { // Pick random positions until one is within the eye circle
          newX = random(0,7);
          newY = random(5,7);

          switch(mood)
          {
            case moods::sadMood:
              newX = random(0,7);
              newY = random(5,7);
            break;
            case moods::neutralMood:
              newX = random(7);
              newY = random(7);
            break;
            case moods::happyMOod:
              newX = random(7);
              newY = random(4);
            break;
            case moods::annoyedMood:
              newX = random(7);
              newY = random(3,7);
            break;
            default:
            break;
          }

          dX   = newX-3;
          dY   = newY-3;
        }
        while((dX * dX + dY * dY) >= 10);        // Thank you Pythagoras
        dX            = newX - eyeX;             // Horizontal distance to move
        dY            = newY - eyeY;             // Vertical distance to move
        gazeFrames    = random(3, 15);           // Duration of eye movement
        gazeCountdown = random(gazeFrames, 120); // Count to end of next movement
      }
    }
    else
    {
      // Not in motion yet -- draw pupil at current static position
      rf.setX(eyeX);
      rf.setY(eyeY);
    }
  }

  void noiseProcess(moods mood)
  {
    if(ticks - checkMillis > (uint64_t)random(1000,3000))
    {
      switch(mood)
      {
        case moods::sadMood:
          sadMoodNoise();
        break;
        case moods::neutralMood:
          neutralMoodNoise();
        break;
        case moods::happyMOod:
          happyMOodNoise();
        break;
        case moods::annoyedMood:
          annoyedMoodNoise();
        break;
        default:
        break;
      }
      checkMillis = ticks;
    }
  }

  void modProcess(moods mood)
  {
    switch(mood)
    {
      case moods::sadMood:
        drawBlink(sadBlinkImg, (blinkCountdown < sizeof(blinkIndex)) ? blinkIndex[blinkCountdown] : 0);
      break;
      case moods::neutralMood:
        drawBlink(blinkImg, (blinkCountdown < sizeof(blinkIndex)) ? blinkIndex[blinkCountdown] : 0);
      break;
      case moods::happyMOod:
        drawBlink(happyBlinkImg, (blinkCountdown < sizeof(blinkIndex)) ? blinkIndex[blinkCountdown] : 0);
      break;
      case moods::annoyedMood:
        drawBlink(annoyedBlinkImg, (blinkCountdown < sizeof(blinkIndex)) ? blinkIndex[blinkCountdown] : 0);
      break;
      default:
      break;
    }
    // Decrement blink counter.  At end, set random time for next blink.
    if(ticks % 5 == 0)
    {
      if(--blinkCountdown == 0) blinkCountdown = random(5, 180);
    }
  }

  void process(moods mood)
  {
    modProcess(mood);
    gazeProcess(mood);
    noiseProcess(mood);

    // tapMillis = millis();
    //
    // while(millis()-tapMillis < 40)
    // {
      // checkTaps();
    // }
    // songMillis = millis();
  }

  int random(int min, int max)
  {
      int tmp;
      if (max>=min)
          max-= min;
      else
      {
          tmp= min - max;
          min= max;
          max= tmp;
      }
      return max ? (rand() % max + min) : min;
  }

  int random(int max)
  {
    return random(0, max);
  }

  void drawBlink(uint8_t const blinksArray[][8], uint8_t blinkIndex)
  {
    for (std::size_t i = 0; i < 8; i++)
    {
      uint8_t line = blinksArray[blinkIndex][i];
      for(std::size_t j = 0; j < 8; j++)
      {
        if((line >> j) & 0x01)
        {
          setPixel(j, i, sGfx::Color::white());
        }
      }
    }
  }

  void sadMoodNoise(void)
  {
    Tone::BeepEvent sig;
    sig.beepCmd = Tone::BeepCmd::bip;
    sig.numOfReps = 1;
    Bsp::tone.signal(sig);
  }

  void neutralMoodNoise(void)
  {

  }

  void happyMOodNoise(void)
  {

  }

  void annoyedMoodNoise(void)
  {

  }

  void checkTaps()
  {
    if(tapNum <= 10)
      mood = moods::sadMood;
    else if(tapNum <= 20)
      mood = moods::neutralMood;
    else if(tapNum <= 30)
      mood = moods::happyMOod;
    else if(tapNum > 30)
      mood = moods::annoyedMood;

    currentMillis = ticks;

    if(currentMillis - previousMillis > decay)
    {
      // save the last time you blinked the LED
      previousMillis = currentMillis;
      tapNum--;
      if(tapNum < 0) tapNum = 0;
      if(tapNum == 30) tapNum = 15;
      if(tapNum > 40) tapNum = 40;
    }

    currentMillis = ticks;

    if(currentMillis - songMillis > (uint64_t)random(30000,60000) && mood == moods::happyMOod)
    {
      // matrix.drawBitmap(0, 0, happyBlinkImg[0], 8, 8, LED_ON);
      // eyeX = 3;
      // eyeY = 3;
      // matrix.fillRect(eyeX, eyeY, 2, 2, LED_OFF);

      // play_rtttl(songs[random(7)]);
      previousMillis = ticks;
      checkMillis = ticks;
      songMillis = ticks;
    }
  }
};

class Eyes_Control: public Control
{
public:
   Eyes_Control(ViewPort* viewport):
   Control(viewport),
   eyeLeft(viewport),
   eyeRight(viewport)
   {
     eyeLeft.setParent(this);
     eyeLeft.setX(0);
     eyeLeft.setY(0);
     eyeLeft.setH(8);
     eyeLeft.setW(8);
     eyeLeft.setColor(sGfx::Color::white());
     eyeRight.setParent(this);
     eyeRight.setX(16);
     eyeRight.setY(0);
     eyeRight.setH(8);
     eyeRight.setW(8);
     eyeRight.setColor(sGfx::Color::white());
   }

   virtual void draw()
   {
     eyeLeft.draw();
     eyeRight.draw();
   }

   virtual void tick()
   {
      Drawable::tick();

      eyeLeft.tick();
      eyeRight.tick();
   }

   void tap(void)
   {
     eyeLeft.tap();
     eyeRight.tap();
   }

private:
  Eye_Control eyeLeft;
  Eye_Control eyeRight;

};

}

#endif
