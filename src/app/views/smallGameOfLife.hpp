/*
 * SmallGameOfLife.h
 *
 *  Created on: 03-05-2013
 *      Author: ziemek
 */
#include "AppConfig.h"
#ifdef GAmeOfLife

#ifndef SMALLGAMEOFLIFE_H_
#define SMALLGAMEOFLIFE_H_

#include <inttypes.h>
#include "SmallGameOfLifeApp.h"

void SGameOfLife_SetDelyCallback(void (*pSGameOfLife_Delay_Callback)(uint16_t));
void SGameOfLife_SetLedOnCallback(void (*setLedOn_Callback)(uint8_t, uint8_t));
void SGameOfLife_SetLedOffCallback(void (*setLedOff_Callback)(uint8_t, uint8_t));
void SGameOfLife_ProcessBoard();

uint8_t isCellAlive(int8_t row, int8_t col);
uint8_t countNeighbors(uint8_t row, uint8_t col);
void calculateNewGameBoard(void);

#endif /* SMALLGAMEOFLIFE_H_ */

#endif
