#ifndef IPINFO_VIEW_HPP
#define IPINFO_VIEW_HPP

#include "bsp/bsp.hpp"

// #include <floatToStr.hpp>
// #include <time.hpp>

#include "system/framework/sGfx/viewPort.hpp"
#include "system/framework/sGfx/txt.hpp"
#include "system/framework/sGfx/rect.hpp"
#include "system/framework/sGfx/line.hpp"
#include "system/framework/sGfx/color.hpp"
#include "system/framework/sGfx/transformXY.hpp"
#include "system/framework/sGfx/easing/easing.hpp"

#include "twoDigits_control.hpp"
#include "timeDisplay_control.hpp"
#include "tempDisplay_control.hpp"
#include "rain_control.hpp"
#include "fire_control.hpp"
#include "fill_control.hpp"
#include "basicTimeDisp_ctrl.hpp"
#include "basicTempDisp_ctrl.hpp"

#define SCREEN_WIDTH (8*4)
#define SCREEN_HEIGHT (8)

namespace IpInfo_View
{
bool enabled = false;

void update()
{
   if(!enabled)
   {
      return;
   }

}

void oneS_tsk()
{
   if(!enabled)
   {
      return;
   }
   update();
}

void houndredMs_tsk()
{
   if(!enabled)
   {
      return;
   }
   // animTestViewPort.update();
}

void oneMs_tsk()
{
   if(!enabled)
   {
      return;
   }
   // animTestViewPort.tick();
}

void init()
{
   if(!enabled)
   {
      return;
   }
}
}

#endif
