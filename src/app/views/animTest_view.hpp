
#ifndef ANIMTEST_VIEW_HPP
#define ANIMTEST_VIEW_HPP

#include "bsp/bsp.hpp"

// #include <floatToStr.hpp>
// #include <time.hpp>

#include "system/framework/sGfx/viewPort.hpp"
#include "system/framework/sGfx/txt.hpp"
#include "system/framework/sGfx/rect.hpp"
#include "system/framework/sGfx/line.hpp"
#include "system/framework/sGfx/color.hpp"
#include "system/framework/sGfx/transformXY.hpp"
#include "system/framework/sGfx/easing/easing.hpp"

#include "twoDigits_control.hpp"
#include "timeDisplay_control.hpp"
#include "tempDisplay_control.hpp"
#include "rain_control.hpp"
#include "fire_control.hpp"
#include "fill_control.hpp"
#include "basicTimeDisp_ctrl.hpp"
#include "basicTempDisp_ctrl.hpp"

#define SCREEN_WIDTH (8*4)
#define SCREEN_HEIGHT (8)

namespace AnimTest_View
{
bool enabled = false;

sGfx::ViewPort animTestViewPort;
sGfx::RectFill rf(&animTestViewPort);
sGfx::TwoDigits_Control<sGfx::Txt5x7> td_c(&animTestViewPort);
//-----------------------------
// sGfx::TansformXY<sGfx::BasicTimeDisp_Ctrl> timeDisp_c(&animTestViewPort);
sGfx::TansformXY<sGfx::TimeDisplay_Control> timeDisp_c(&animTestViewPort);
// sGfx::TansformXY<sGfx::BasicTempDisp_Ctrl> tempDisp_c(&animTestViewPort);
sGfx::TansformXY<sGfx::TempDisplay_Control> tempDisp_c(&animTestViewPort);

void update()
{
   if(!enabled)
   {
      return;
   }

   if((Time::tsToSecond(Time::timestamp) == 15) || (Time::tsToSecond(Time::timestamp) == 45))
   {
      timeDisp_c.start(0, -16, 50, &Easings::linearTween);
      tempDisp_c.start(0, 0, 50, &Easings::linearTween);
   }
   else if((Time::tsToSecond(Time::timestamp) == 15 + 2) || (Time::tsToSecond(Time::timestamp) == 45 + 2))
   {
      timeDisp_c.start(0, 0, 50, &Easings::linearTween);
      tempDisp_c.start(0, 16, 50, &Easings::linearTween);
   }
}

void oneS_tsk()
{
   if(!enabled)
   {
      return;
   }
   update();
}

void houndredMs_tsk()
{
   if(!enabled)
   {
      return;
   }
   animTestViewPort.update();
}

void oneMs_tsk()
{
   if(!enabled)
   {
      return;
   }
   animTestViewPort.tick();
}

void init()
{
   if(!enabled)
   {
      return;
   }

   rf.setColor(sGfx::Color::black());
   rf.setX(0);
   rf.setY(0);
   rf.setW(32);
   rf.setH(8);

   // td_c.setColor(sGfx::Color::white());
   // td_c.setX(0);
   // td_c.setY(0);
   // td_c.setW(5);
   // td_c.setH(8);

   timeDisp_c.setColor(sGfx::Color::white());
   timeDisp_c.setX(0);
   timeDisp_c.setY(0);
   timeDisp_c.setW(8*4);
   timeDisp_c.setH(8);

   tempDisp_c.setColor(sGfx::Color::white());
   tempDisp_c.setX(0);
   tempDisp_c.setY(8);
   tempDisp_c.setW(8*4);
   tempDisp_c.setH(8);

   animTestViewPort.add(&td_c);
   animTestViewPort.add(&timeDisp_c);
   animTestViewPort.add(&tempDisp_c);
   animTestViewPort.add(&rf);
}
}

#endif
