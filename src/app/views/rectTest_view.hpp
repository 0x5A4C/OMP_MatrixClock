
#ifndef RECTTEST_VIEW_HPP
#define RECTTEST_VIEW_HPP

#include "bsp/bsp.hpp"

// #include <floatToStr.hpp>

#include "system/framework/sGfx/viewPort.hpp"
#include "system/framework/sGfx/rect.hpp"
#include "system/framework/sGfx/txt.hpp"
#include "test_control.hpp"

namespace RecrTest_View
{
bool enabled = false;

#define FONTWIDTH 7
#define TEXT_X_OFFSET 1
#define TEXT_Y_OFFSET 0

sGfx::ViewPort mainViewPort;
sGfx::RectFill backGround(&mainViewPort);
sGfx::Rect rf(&mainViewPort);
sGfx::Test_Control tc(&mainViewPort);

void oneS_tsk()
{
}

void oneMs_tsk()
{
   if(!enabled)
   {
      return;
   }

   mainViewPort.update();
   tc.tick();
}

void init()
{
   if(!enabled)
   {
      return;
   }

   // backGround.setColor(sGfx::Color::black());
   // backGround.setX(10);
   // backGround.setY(10);
   // backGround.setW(100);
   // backGround.setH(100);

   // rf.setColor(sGfx::Color::white());
   // rf.setX(10);
   // rf.setY(10);
   // rf.setW(100);
   // rf.setH(100);

   tc.setColor(sGfx::Color::white());
   tc.setX(10);
   tc.setY(10);
   tc.setW(100);
   tc.setH(100);

   mainViewPort.add(&rf);
   rf.add(&tc);
   tc.add(&backGround);

   mainViewPort.update();
}

void update()
{
   if(!enabled)
   {
      return;
   }
}

}

#endif
