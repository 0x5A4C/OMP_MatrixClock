
#ifndef BMPXBMTEST_VIEW_HPP
#define BMPXBMTEST_VIEW_HPP

#include "bsp/bsp.hpp"

#include "system/framework/sGfx/viewPort.hpp"
#include "system/framework/sGfx/txt.hpp"
#include "system/framework/sGfx/rect.hpp"
#include "system/framework/sGfx/line.hpp"
#include "system/framework/sGfx/color.hpp"
#include "system/framework/sGfx/bmpXbm.hpp"
#include "system/framework/sGfx/bmpXbmAnimated.hpp"
#include "system/framework/sGfx/transformXY.hpp"
#include "system/framework/sGfx/easing/easing.hpp"

#include "twoDigits_control.hpp"
#include "timeDisplay_control.hpp"
#include "tempDisplay_control.hpp"
#include "rain_control.hpp"
#include "fire_control.hpp"
#include "fill_control.hpp"
#include "basicTimeDisp_ctrl.hpp"
#include "basicTempDisp_ctrl.hpp"

#define SCREEN_WIDTH (8*4)
#define SCREEN_HEIGHT (8)

extern unsigned char testMatrixA_bits[];
extern unsigned char testMatrixB_bits[];
extern unsigned char testMatrixAB_bits[];
extern unsigned char face_bits[];
extern unsigned char wifi_bits[];
extern unsigned char alien00_bits[];
extern unsigned char scull_bits[];
extern unsigned char snowFlake_bits[];
extern unsigned char cat_bits[];
extern unsigned char bomb_bits[];
extern unsigned char eye_bits[];
extern unsigned char coffe_bits[];

namespace BmpXbm_View
{
bool enabled = false;

typedef enum displayed_e
{
   unknown,
   time,
   temp,
} displayed;

sGfx::ViewPort bmpXbmTestViewPort;
sGfx::BmpXbm bmpXbm(&bmpXbmTestViewPort);
sGfx::BmpXbmAnimated bmpXbmAnim(&bmpXbmTestViewPort);
sGfx::RectFill rf(&bmpXbmTestViewPort);

void update()
{
   static int index = 0;

   if(!enabled)
   {
      return;
   }

   if(index++ & 1)
   {
      bmpXbm.setBitmap(testMatrixA_bits);
   }
   else
   {
      bmpXbm.setBitmap(testMatrixB_bits);
   }

   bmpXbmTestViewPort.update();
}

void oneS_tsk()
{
   if(!enabled)
   {
      return;
   }

   // update();
}

void houndredMs_tsk()
{
   if(!enabled)
   {
      return;
   }

   update();
}

void oneMs_tsk()
{
   if(!enabled)
   {
      return;
   }

   bmpXbmTestViewPort.tick();
}

void init()
{
   if(!enabled)
   {
      return;
   }

   rf.setColor(sGfx::Color::black());
   rf.setX(0);
   rf.setY(0);
   rf.setW(32);
   rf.setH(8);

   bmpXbm.setColor(sGfx::Color::white());
   bmpXbm.setX(0);
   bmpXbm.setY(0);
   bmpXbm.setW(8*4);
   bmpXbm.setH(8);
   bmpXbm.setBitmap(testMatrixA_bits);

   bmpXbmAnim.setColor(sGfx::Color::white());
   bmpXbmAnim.setX(0);
   bmpXbmAnim.setY(0);
   bmpXbmAnim.setW(8);
   bmpXbmAnim.setH(8);
   bmpXbmAnim.setBitmap(coffe_bits);
   bmpXbmAnim.setDuration(100);
   bmpXbmAnim.setNumOfSlides(4);
   bmpXbmAnim.start();

   // bmpXbmTestViewPort.add(&bmpXbm);
   bmpXbmTestViewPort.add(&bmpXbmAnim);
   bmpXbmTestViewPort.add(&rf);
}
}

#endif
