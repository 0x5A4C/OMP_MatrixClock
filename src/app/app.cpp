
#include "app/app.hpp"

#include "app/views/animTest_view.hpp"
#include "app/views/clockTemp_view.hpp"
#include "app/views/rectTest_view.hpp"
#include "app/views/bmpXbm_view.hpp"
#include "app/views/ipInfo_view.hpp"

// #include "system/framework/Stm/Stm.hpp"
#include "app/states/stateEv.hpp"
#include "app/states/stateApp.hpp"
#include "app/states/stateDispTest.hpp"
#include "app/states/stateAnim.hpp"
#include "app/states/stateTimeTemp.hpp"

#include "espressif/esp_common.h"
#include "esp/uart.h"
#include "esp8266.h"

namespace App
{
StmEv appStm;

void oneMs_tsk()
{
   // RecrTest_View::oneMs_tsk();
   // ClockTemp_View::oneMs_tsk();
   AnimTest_View::oneMs_tsk();
   // BmpXbm_View::oneMs_tsk();
}

void houndredMs_tsk()
{
   // ClockTemp_View::houndredMs_tsk();
   AnimTest_View::houndredMs_tsk();
   // BmpXbm_View::houndredMs_tsk();
}

void oneS_tsk()
{
   // RecrTest_View::oneS_tsk();
   // ClockTemp_View::oneS_tsk();
   AnimTest_View::oneS_tsk();
   // BmpXbm_View::oneS_tsk();

   if(Time::tsToSecond(Time::timestamp) == 30)
   {
      Time::timestamp = time(NULL);
   }
}

void appUpEventHdl()
{
   appStm.evHdl(StEvent::Up);
}
void appDownEventHdl()
{
   appStm.evHdl(StEvent::Down);
}
void appLeftEventHdl()
{
   appStm.evHdl(StEvent::Left);
}
void appRightEventHdl()
{
   appStm.evHdl(StEvent::Right);
}
void appInEventHdl()
{
   appStm.evHdl(StEvent::In);
}
void appOutEventHdl()
{
   appStm.evHdl(StEvent::Out);
}

void appInit()
{
   // extern "C" void appUpEventHdl();
   // extern "C" void appDownEventHdl();
   // extern "C" void appLeftEventHdl();
   // extern "C" void appRightEventHdl();
   // extern "C" void appInEventHdl();
   // extern "C" void appOutEventHdl();

   Bsp::bspUpEventHdl = appUpEventHdl;
   Bsp::bspDownEventHdl = appDownEventHdl;
   Bsp::bspLeftEventHdl = appLeftEventHdl;
   Bsp::bspRightEventHdl = appRightEventHdl;
   Bsp::bspInEventHdl = appInEventHdl;
   Bsp::bspOutEventHdl = appOutEventHdl;

   RecrTest_View::enabled = false;
   RecrTest_View::init();
   ClockTemp_View::enabled = false;
   ClockTemp_View::init();
   AnimTest_View::enabled = true;
   AnimTest_View::init();
   BmpXbm_View::enabled = false;
   BmpXbm_View::init();
//states
   appState.setParentStm(&appStm);
   stateDispTest.setParentStm(&appStm);
   timeTempState.setParentStm(&appStm);
   stateAnim.setParentStm(&appStm);
   stateRain.setParentStm(&appStm);
   stateGameLife.setParentStm(&appStm);
   stateSprites.setParentStm(&appStm);
   stateSpritesGif.setParentStm(&appStm);
   stateLongTime.setParentStm(&appStm);
   stEyes.setParentStm(&appStm);

   appStm.transitionTo((St*)&stateDispTest);
   // appStm.transitionTo((St*)&stateAnim);
   // appStm.transitionTo((St*)&stateGameLife);
}

void appLoop()
{
   appStm.process();
}
}
