#include "stateDispTest.hpp"
#include "states.hpp"

void StateDispTest::onEnter(void)
{
   aTimer.start();
   Bsp::tone.signal(Tone::SignalCmd::phone);
}

void StateDispTest::process(void)
{
   testScrPtrn.drawOddTestPattern();
   if(aTimer.getTicks() < 1000)
   {
      testScrPtrn.drawOddTestPattern();
   }
   if((aTimer.getTicks() > 1000) && (aTimer.getTicks() < 2000))
   {
      testScrPtrn.drawEvenTestPattern();
   }
   if(aTimer.getTicks() > 2000)
   {
      stm->transitionTo((St*)&timeTempState);
   }
   vp.update();
   vp.tick();
}

void StateDispTest::onExit(void)
{
}

StateDispTest stateDispTest;
