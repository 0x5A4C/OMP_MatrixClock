#ifndef STATELONGTIME_HPP
#define STATELONGTIME_HPP

#include "system/framework/Stm/Stm.hpp"
#include "stateEv.hpp"

#include "app/views/timeDisplay6d_control.hpp"


#include "states.hpp"

class StLongTime: public StEv
{
public:
   StLongTime(void): StEv()
   {
      timeDisp_c.setColor(sGfx::Color::white());
      timeDisp_c.setX(0);
      timeDisp_c.setY(0);
      timeDisp_c.setW(8*4);
      timeDisp_c.setH(8);

      rf.setColor(sGfx::Color::black());
      rf.setX(0);
      rf.setY(0);
      rf.setW(8*4);
      rf.setH(8);

      vp.add(&timeDisp_c);
      vp.add(&rf);
   }

   StLongTime(Stm* parentStm): StEv(parentStm)
   {
      vp.add(&rf);
   }

   virtual void onEnter(void);
   virtual void process(void);
   virtual void onExit(void);

   virtual void evHdl(StEvent ev);

protected:
private:
   sGfx::ViewPort vp;
   sGfx::RectFill rf{sGfx::RectFill(&vp)};
   sGfx::TansformXY<sGfx::Time6dDisplay_Control> timeDisp_c{sGfx::TansformXY<sGfx::Time6dDisplay_Control>(&vp)};
};

extern StLongTime stateLongTime;

#endif
