#ifndef STATEGAMELIFE_HPP
#define STATEGAMELIFE_HPP

#include "system/framework/Stm/Stm.hpp"
#include "stateEv.hpp"
#include "system/framework/sGfx/viewPort.hpp"
#include "app/views/fire_control.hpp"
#include "app/views/gameLife_control.hpp"

#include "time.hpp"
#include "timer.hpp"

class StateGameLife: public StEv
{
public:
   StateGameLife(): StEv()
   {
      gameLife.setColor(sGfx::Color::white());
      gameLife.setX(0);
      gameLife.setY(0);
      gameLife.setW(8*4);
      gameLife.setH(8);

      rf.setColor(sGfx::Color::black());
      rf.setX(0);
      rf.setY(0);
      rf.setW(8*4);
      rf.setH(8);

      vp.add(&gameLife);
      vp.add(&rf);
   }

   StateGameLife(Stm* parentStm): StEv(parentStm)
   {
   }

   virtual void onEnter(void);
   virtual void process(void);
   virtual void onExit(void);

   void evHdl(StEvent ev);

protected:
private:
   sGfx::ViewPort vp;
   sGfx::GameLife_Control gameLife{sGfx::GameLife_Control(&vp)};
   sGfx::RectFill rf{sGfx::RectFill(&vp)};
};

extern StateGameLife stateGameLife;

#endif
