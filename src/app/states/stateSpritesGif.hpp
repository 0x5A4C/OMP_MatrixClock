#ifndef STATESPRITESGIF_HPP
#define STATESPRITESGIF_HPP

#include "system/framework/Stm/Stm.hpp"
#include "stateEv.hpp"
#include "system/framework/sGfx/viewPort.hpp"
#include "app/views/sprite_control.hpp"

#include "time.hpp"
#include "timer.hpp"

#include "sprites.hpp"

#define ANIM_TICKS 500

class StateSpritesGif: public StEv
{
public:
   StateSpritesGif(): StEv()
   {
      rf.setColor(sGfx::Color::black());
      rf.setX(0);
      rf.setY(0);
      rf.setW(8*4);
      rf.setH(8);

      sprite00.setSprites((uint8_t*)&aGifSprite00, 30, 100);
      sprite00.addEndHandler(std::bind(&StateSpritesGif::sprite00EndCalback, this));
      sprite01.setSprites((uint8_t*)&aGifSprite01, 30, 100);
      sprite01.addEndHandler(std::bind(&StateSpritesGif::sprite01EndCalback, this));
      sprite02.setSprites((uint8_t*)&aGifSprite02, 30, 100);
      sprite02.addEndHandler(std::bind(&StateSpritesGif::sprite02EndCalback, this));
      sprite03.setSprites((uint8_t*)&aGifSprite03, 30, 100);
      sprite03.addEndHandler(std::bind(&StateSpritesGif::sprite03EndCalback, this));

      vp.add(&sprite00);
      vp.add(&sprite01);
      vp.add(&sprite02);
      vp.add(&sprite03);
      vp.add(&rf);
   }

   StateSpritesGif(Stm* parentStm): StEv(parentStm)
   {
      // vp.add(&fire);
   }

   virtual void onEnter(void);
   virtual void process(void);
   virtual void onExit(void);

   void evHdl(StEvent ev);

protected:
private:
   sGfx::ViewPort vp;
   sGfx::RectFill rf{sGfx::RectFill(&vp)};

   sGfx::TansformXY<sGfx::Sprite_Control> sprite00{sGfx::TansformXY<sGfx::Sprite_Control>(&vp)};
   sGfx::TansformXY<sGfx::Sprite_Control> sprite01{sGfx::TansformXY<sGfx::Sprite_Control>(&vp)};
   sGfx::TansformXY<sGfx::Sprite_Control> sprite02{sGfx::TansformXY<sGfx::Sprite_Control>(&vp)};
   sGfx::TansformXY<sGfx::Sprite_Control> sprite03{sGfx::TansformXY<sGfx::Sprite_Control>(&vp)};

   void sprite00EndCalback(void)
   {
      sprite00.addEndHandler(NULL);
      sprite01.start(0-8, 0, ANIM_TICKS, &Easings::linearTween);
   }

   void sprite01EndCalback(void)
   {
      sprite01.addEndHandler(NULL);
      sprite02.start(0-8, 0, ANIM_TICKS, &Easings::linearTween);
   }

   void sprite02EndCalback(void)
   {
      sprite02.addEndHandler(NULL);
      sprite03.start(0-8, 0, ANIM_TICKS, &Easings::linearTween);
   }

#define S11 (8*4)
#define S12 (0-8)
#define S21 (8*5+3)
#define S22 (0-8)
#define S31 (8*6+6)
#define S32 (0-8)
#define S41 (8*7+9)
#define S42 (0-8)

   const uint16_t T1 = ANIM_TICKS;
   const uint16_t T2 = (uint16_t)((float)(S21-S22)/(float)((float)(S11-S12)/T1));
   const uint16_t T3 = (uint16_t)((float)(S31-S32)/(float)((float)(S11-S12)/T1));
   const uint16_t T4 = (uint16_t)((float)(S41-S42)/(float)((float)(S11-S12)/T1));
   void sprite03EndCalback(void)
   {
      sprite00.setX(S11);
      sprite00.setY(0);

      sprite01.setX(S21);
      sprite01.setY(0);

      sprite02.setX(S31);
      sprite02.setY(0);

      sprite03.setX(S41);
      sprite03.setY(0);

      sprite00.start(0-8, 0, T1, &Easings::linearTween);
      sprite01.start(0-8, 0, T2, &Easings::linearTween);
      sprite02.start(0-8, 0, T3, &Easings::linearTween);
      sprite03.start(0-8, 0, T4, &Easings::linearTween);
   }
};

extern StateSpritesGif stateSpritesGif;

#endif
