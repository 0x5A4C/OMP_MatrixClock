#include "stateSprites.hpp"
#include "states.hpp"
#include "sprites.hpp"

void StateSprites::onEnter(void)
{
   sprite00.cancel();
   sprite01.cancel();
   sprite02.cancel();

   sprite00.setX(8*4+1);
   sprite00.setY(0);
   sprite00.setW(8);
   sprite00.setH(8);

   sprite01.setX(8*4+1);
   sprite01.setY(0);
   sprite01.setW(8);
   sprite01.setH(8);

   sprite02.setX(8*4+1);
   sprite02.setY(0);
   sprite02.setW(8);
   sprite02.setH(8);

   sprite00.start(0-8, 0, 200, &Easings::linearTween);
}

void StateSprites::process(void)
{
   vp.update();
   vp.tick();
}

void StateSprites::onExit(void)
{
}

void StateSprites::evHdl(StEvent ev)
{
   switch(ev)
   {
   case StEvent::Unknown:
      break;
   case StEvent::None:
      break;
   case StEvent::Up:
      break;
   case StEvent::Down:
      stm->transitionTo(&stateSpritesGif);
      break;
   case StEvent::Left:
      break;
   case StEvent::Right:
      break;
   case StEvent::In:
      break;
   case StEvent::Out:
      stm->transitionTo(&timeTempState);
      break;
   default:
      break;
   }
}

StateSprites stateSprites;
