#include "stateEyes.hpp"

void StEyes::onEnter(void)
{
}

void StEyes::process(void)
{
   vp.update();
   vp.tick();
}

void StEyes::onExit(void)
{
}

void StEyes::evHdl(StEvent ev)
{
   switch(ev)
   {
   case StEvent::Unknown:
      break;
   case StEvent::None:
      break;
   case StEvent::Up:
      break;
   case StEvent::Down:
      stm->transitionTo(&stateAnim);
      break;
   case StEvent::Left:
      break;
   case StEvent::Right:
      break;
   case StEvent::In:
      Tone::BeepEvent sig;
      sig.beepCmd = Tone::BeepCmd::bip;
      sig.numOfReps = 1;
      Bsp::tone.signal(sig);
      eyes_Control.tap();
      break;
   case StEvent::Out:
      stm->transitionTo(&timeTempState);
      break;
   default:
      break;
   }
}

StEyes stEyes;
