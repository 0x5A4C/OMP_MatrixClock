#include "stateAnim.hpp"
#include "states.hpp"

void StateAnim::onEnter(void)
{
   // aTimer.start();
   // timemarker = Time::timestamp;
   // counter = 0;
}

void StateAnim::process(void)
{
   vp.update();
   vp.tick();

   // while( aTimer.getTicks() < 1000 );
   // aTimer.reset();
   // testScrPtrn.drawOddTestPattern();
   // while( aTimer.getTicks() < 1000 );
   // aTimer.reset();
   // testScrPtrn.drawEvenTestPattern();
   // while( aTimer.getTicks() < 1000 );
   // aTimer.stop();
   // stm->transitionTo((St*)&timeTempState);


   // if(counter == 50)
   // {
   //   testScrPtrn.drawOddTestPattern();
   //   counter++;
   // }
   // else if(counter == 100)
   // {
   //   testScrPtrn.drawEvenTestPattern();
   //   counter++;
   // }
   // else if(counter == 200)
   // {
   //   stm->transitionTo((St*)&timeTempState);
   // }
   // else
   // {
   //   counter++;
   // }
}

void StateAnim::onExit(void)
{
}

void StateAnim::evHdl(StEvent ev)
{
   switch(ev)
   {
   case StEvent::Unknown:
      break;
   case StEvent::None:
      break;
   case StEvent::Up:
      break;
   case StEvent::Down:
      stm->transitionTo(&stateRain);
      break;
   case StEvent::Left:
      break;
   case StEvent::Right:
      break;
   case StEvent::In:
      break;
   case StEvent::Out:
      stm->transitionTo(&timeTempState);
      break;
   default:
      break;
   }
}

StateAnim stateAnim;
