#include "stateRain.hpp"
#include "states.hpp"

void StateRain::onEnter(void)
{
}

void StateRain::process(void)
{
   vp.update();
   vp.tick();
}

void StateRain::onExit(void)
{
}

void StateRain::evHdl(StEvent ev)
{
   switch(ev)
   {
   case StEvent::Unknown:
      break;
   case StEvent::None:
      break;
   case StEvent::Up:
      break;
   case StEvent::Down:
      stm->transitionTo(&stateGameLife);
      break;
   case StEvent::Left:
      break;
   case StEvent::Right:
      break;
   case StEvent::In:
      break;
   case StEvent::Out:
      stm->transitionTo(&timeTempState);
      break;
   default:
      break;
   }
}

StateRain stateRain;
