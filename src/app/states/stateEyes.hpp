#ifndef STATEEYES_HPP
#define STATEEYES_HPP

#include "system/framework/Stm/Stm.hpp"
#include "stateEv.hpp"

#include "states.hpp"

#include "app/views/eyes_control.hpp"

class StEyes: public StEv
{
public:
   StEyes(void): StEv()
   {
      eyes_Control.setColor(sGfx::Color::white());
      eyes_Control.setX(4);
      eyes_Control.setY(0);
      eyes_Control.setW(8*3);
      eyes_Control.setH(8);

      rf.setColor(sGfx::Color::black());
      rf.setX(0);
      rf.setY(0);
      rf.setW(8*4);
      rf.setH(8);

      vp.add(&eyes_Control);
      vp.add(&rf);
   }

   StEyes(Stm* parentStm): StEv(parentStm)
   {
      vp.add(&rf);
   }

   virtual void onEnter(void);
   virtual void process(void);
   virtual void onExit(void);

   virtual void evHdl(StEvent ev);

protected:
private:
   sGfx::ViewPort vp;
   sGfx::RectFill rf{sGfx::RectFill(&vp)};
   sGfx::Eyes_Control eyes_Control{sGfx::Eyes_Control(&vp)};

};

extern StEyes stEyes;

#endif
