#ifndef STATERAIN_HPP
#define STATERAIN_HPP

#include "system/framework/Stm/Stm.hpp"
#include "stateEv.hpp"
#include "system/framework/sGfx/viewPort.hpp"
#include "system/framework/sGfx/rect.hpp"
#include "app/views/rain_control.hpp"

#include "time.hpp"
#include "timer.hpp"

class StateRain: public StEv
{
public:
   StateRain(): StEv()
   {
      rain.setColor(sGfx::Color::white());
      rain.setX(0);
      rain.setY(0);
      rain.setW(8*4);
      rain.setH(8);

      rf.setColor(sGfx::Color::black());
      rf.setX(0);
      rf.setY(0);
      rf.setW(8*4);
      rf.setH(8);

      vp.add(&rain);
      vp.add(&rf);
   }

   StateRain(Stm* parentStm): StEv(parentStm)
   {
      vp.add(&rain);
   }

   virtual void onEnter(void);
   virtual void process(void);
   virtual void onExit(void);

   void evHdl(StEvent ev);

protected:
private:
   sGfx::ViewPort vp;
   sGfx::Rn_Control rain{sGfx::Rn_Control(&vp)};
   sGfx::RectFill rf{sGfx::RectFill(&vp)};

   // Timer aTimer;
   // time_t timemarker;
   // uint8_t counter;
};

extern StateRain stateRain;

#endif
