#ifndef STATESPRITES_HPP
#define STATESPRITES_HPP

#include "system/framework/Stm/Stm.hpp"
#include "stateEv.hpp"
#include "system/framework/sGfx/viewPort.hpp"
#include "app/views/sprite_control.hpp"

#include "time.hpp"
#include "timer.hpp"

#include "sprites.hpp"

class StateSprites: public StEv
{
public:
   StateSprites(): StEv()
   {
      rf.setColor(sGfx::Color::black());
      rf.setX(0);
      rf.setY(0);
      rf.setW(8*4);
      rf.setH(8);

      sprite00.setSprites((uint8_t*)&aSprite00, 2, 300);
      sprite00.addEndHandler(std::bind(&StateSprites::sprite00EndCalback, this));
      sprite01.setSprites((uint8_t*)&aSprite01, 2, 300);
      sprite01.addEndHandler(std::bind(&StateSprites::sprite01EndCalback, this));
      sprite02.setSprites((uint8_t*)&aSprite03, 2, 300);
      sprite02.addEndHandler(std::bind(&StateSprites::sprite02EndCalback, this));

      vp.add(&sprite00);
      vp.add(&sprite01);
      vp.add(&sprite02);
      vp.add(&rf);
   }

   StateSprites(Stm* parentStm): StEv(parentStm)
   {
      // vp.add(&fire);
   }

   virtual void onEnter(void);
   virtual void process(void);
   virtual void onExit(void);

   void evHdl(StEvent ev);

protected:
private:
   sGfx::ViewPort vp;
   sGfx::RectFill rf{sGfx::RectFill(&vp)};

   sGfx::TansformXY<sGfx::Sprite_Control> sprite00{sGfx::TansformXY<sGfx::Sprite_Control>(&vp)};
   sGfx::TansformXY<sGfx::Sprite_Control> sprite01{sGfx::TansformXY<sGfx::Sprite_Control>(&vp)};
   sGfx::TansformXY<sGfx::Sprite_Control> sprite02{sGfx::TansformXY<sGfx::Sprite_Control>(&vp)};

   void sprite00EndCalback(void)
   {
      sprite00.addEndHandler(NULL);
      sprite01.start(0-8, 0, 200, &Easings::linearTween);
   }

   void sprite01EndCalback(void)
   {
      sprite01.addEndHandler(NULL);
      sprite02.start(0-8, 0, 200, &Easings::linearTween);
   }

#define S11 (8*4)
#define S12 (0-8)
#define S21 (8*5+3)
#define S22 (0-8)
#define S31 (8*6+6)
#define S32 (0-8)
   const uint16_t T1 = 200;
   const uint16_t T2 = (uint16_t)((float)(S21-S22)/(float)((float)(S11-S12)/T1));
   const uint16_t T3 = (uint16_t)((float)(S31-S32)/(float)((float)(S11-S12)/T1));
   void sprite02EndCalback(void)
   {
      sprite00.setX(S11);
      sprite00.setY(0);

      sprite01.setX(S21);
      sprite01.setY(0);

      sprite02.setX(S31);
      sprite02.setY(0);

      sprite00.start(0-8, 0, T1, &Easings::linearTween);
      sprite01.start(0-8, 0, T2, &Easings::linearTween);
      sprite02.start(0-8, 0, T3, &Easings::linearTween);
   }
};

extern StateSprites stateSprites;

#endif
