#ifndef STATEEV_HPP
#define STATEEV_HPP

#include "system/framework/Stm/Stm.hpp"

enum class StEvent
{
   Unknown,
   None,
   Up,
   Down,
   Left,
   Right,
   In,
   Out
};

class StEv: public St
{
public:
   StEv(): St()
   {}

   StEv(Stm* parentStm): St(parentStm)
   {}

   virtual void evHdl(StEvent ev)
   {}

protected:
private:

};

class StmEv: public Stm
{
public:
   StmEv(): Stm()
   {
   };

   void evHdl(StEvent ev)
   {
      if(activeState != NULL)
      {
         ((StEv*)activeState)->evHdl(ev);
      }
   }

protected:
private:

};

#endif
