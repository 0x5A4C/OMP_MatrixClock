#include "stateLongTime.hpp"

#include "espressif/esp_common.h"
#include "esp/uart.h"

void StLongTime::onEnter(void)
{
}

void StLongTime::process(void)
{
   vp.update();
   vp.tick();
}

void StLongTime::onExit(void)
{
}

void StLongTime::evHdl(StEvent ev)
{
   switch(ev)
   {
   case StEvent::Unknown:
      break;
   case StEvent::None:
      break;
   case StEvent::Up:
      stm->transitionTo(&timeTempState);
      break;
   case StEvent::Down:
      stm->transitionTo(&timeTempState);
      break;
   case StEvent::Left:
      break;
   case StEvent::Right:
      break;
   case StEvent::In:
      break;
   case StEvent::Out:
      stm->transitionTo(&timeTempState);
      break;
   default:
      break;
   }
}

StLongTime stateLongTime;
