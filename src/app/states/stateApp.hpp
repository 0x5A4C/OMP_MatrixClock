#ifndef STATEAPP_HPP
#define STATEAPP_HPP

#include "system/framework/Stm/Stm.hpp"
#include "stateEv.hpp"

class StApp: public StEv
{
public:
   StApp(): StEv()
   {}

   StApp(Stm* parentStm): StEv(parentStm)
   {}

   virtual void onEnter(void);
   virtual void process(void);
   virtual void onExit(void);

protected:
private:
};

extern StApp appState;

#endif
