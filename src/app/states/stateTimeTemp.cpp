#include "stateTimeTemp.hpp"

#include "espressif/esp_common.h"
#include "esp/uart.h"

sGfx::ViewPort vpp;
sGfx::RectFill rf(&vpp);
sGfx::TansformXY<sGfx::TimeDisplay_Control> timeDisp_c(&vpp);
sGfx::TansformXY<sGfx::TempDisplay_Control> tempDisp_c(&vpp);

StTimeTemp::StTimeTemp(void): StEv(), rttlCmd(Tone::RttlCmd::unknown)
{
   rf.setColor(sGfx::Color::black());
   rf.setX(0);
   rf.setY(0);
   rf.setW(32);
   rf.setH(8);

   timeDisp_c.setColor(sGfx::Color::white());
   timeDisp_c.setX(0);
   timeDisp_c.setY(0);
   timeDisp_c.setW(8*4);
   timeDisp_c.setH(8);

   tempDisp_c.setColor(sGfx::Color::white());
   tempDisp_c.setX(0);
   tempDisp_c.setY(8);
   tempDisp_c.setW(8*4);
   tempDisp_c.setH(8);

   vpp.add(&timeDisp_c);
   vpp.add(&tempDisp_c);
   vpp.add(&rf);
}

void StTimeTemp::onEnter(void)
{
}

void StTimeTemp::process(void)
{
   vpp.update();
   vpp.tick();

   if((Time::tsToSecond(Time::timestamp) == 15) || (Time::tsToSecond(Time::timestamp) == 45))
   {
      timeDisp_c.start(0, -16, 50, &Easings::linearTween);
      tempDisp_c.start(0, 0, 50, &Easings::linearTween);
   }
   else if((Time::tsToSecond(Time::timestamp) == 15 + 2) || (Time::tsToSecond(Time::timestamp) == 45 + 2))
   {
      timeDisp_c.start(0, 0, 50, &Easings::linearTween);
      tempDisp_c.start(0, 16, 50, &Easings::linearTween);
   }
}

void StTimeTemp::onExit(void)
{
}

void StTimeTemp::nextMelody(void)
{
   switch(rttlCmd)
   {
   case Tone::RttlCmd::The_Simpsons:
      rttlCmd = Tone::RttlCmd::Indiana;
      break;
   case Tone::RttlCmd::Indiana:
      rttlCmd = Tone::RttlCmd::TakeOnMe;
      break;
   case Tone::RttlCmd::TakeOnMe:
      rttlCmd = Tone::RttlCmd::Entertainer;
      break;
   case Tone::RttlCmd::Entertainer:
      rttlCmd = Tone::RttlCmd::Muppets;
      break;
   case Tone::RttlCmd::Muppets:
      rttlCmd = Tone::RttlCmd::Xfiles;
      break;
   case Tone::RttlCmd::Xfiles:
      rttlCmd = Tone::RttlCmd::Looney;
      break;
   case Tone::RttlCmd::Looney:
      rttlCmd = Tone::RttlCmd::Studio20thCenFox;
      break;
   case Tone::RttlCmd::Studio20thCenFox:
      rttlCmd = Tone::RttlCmd::Bond;
      break;
   case Tone::RttlCmd::Bond:
      rttlCmd = Tone::RttlCmd::MASH;
      break;
   case Tone::RttlCmd::MASH:
      rttlCmd = Tone::RttlCmd::StarWars;
      break;
   case Tone::RttlCmd::StarWars:
      rttlCmd = Tone::RttlCmd::GoodBad;
      break;
   case Tone::RttlCmd::GoodBad:
      rttlCmd = Tone::RttlCmd::TopGun;
      break;
   case Tone::RttlCmd::TopGun:
      rttlCmd = Tone::RttlCmd::A_Team;
      break;
   case Tone::RttlCmd::A_Team:
      rttlCmd = Tone::RttlCmd::Flinstones;
      break;
   case Tone::RttlCmd::Flinstones:
      rttlCmd = Tone::RttlCmd::Jeopardy;
      break;
   case Tone::RttlCmd::Jeopardy:
      rttlCmd = Tone::RttlCmd::Gadget;
      break;
   case Tone::RttlCmd::Gadget:
      rttlCmd = Tone::RttlCmd::Smurfs;
      break;
   case Tone::RttlCmd::Smurfs:
      rttlCmd = Tone::RttlCmd::MahnaMahna;
      break;
   case Tone::RttlCmd::MahnaMahna:
      rttlCmd = Tone::RttlCmd::LeisureSuit;
      break;
   case Tone::RttlCmd::LeisureSuit:
      rttlCmd = Tone::RttlCmd::MissionImp;
      break;
   case Tone::RttlCmd::MissionImp:
      rttlCmd = Tone::RttlCmd::The_Simpsons;
      break;
   default:
      rttlCmd = Tone::RttlCmd::The_Simpsons;
      break;
   }
}

void StTimeTemp::evHdl(StEvent ev)
{
   switch(ev)
   {
   case StEvent::Unknown:
      break;
   case StEvent::None:
      break;
   case StEvent::Up:
      stm->transitionTo(&stateLongTime);
      break;
   case StEvent::Down:
      stm->transitionTo(&stateLongTime);
      break;
   case StEvent::Left:
      break;
   case StEvent::Right:
      stm->transitionTo(&stEyes);
      break;
   case StEvent::In:
      break;
   case StEvent::Out:
      nextMelody();
      Bsp::tone.signal(rttlCmd);
      break;
   default:
      break;
   }
}

StTimeTemp timeTempState;
