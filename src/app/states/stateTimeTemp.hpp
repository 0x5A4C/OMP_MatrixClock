#ifndef STATETIMETEMP_HPP
#define STATETIMETEMP_HPP

#include "system/framework/Stm/Stm.hpp"
#include "stateEv.hpp"
#include "system/framework/sGfx/viewPort.hpp"
#include "app/views/testScrPattern_control.hpp"

#include "system/framework/sGfx/rect.hpp"
#include "app/views/timeDisplay_control.hpp"
#include "app/views/tempDisplay_control.hpp"

#include "states.hpp"

class StTimeTemp: public StEv
{
public:
   StTimeTemp(void);

   StTimeTemp(Stm* parentStm): StEv(parentStm), rttlCmd(Tone::RttlCmd::unknown)
   {}

   virtual void onEnter(void);
   virtual void process(void);
   virtual void onExit(void);

   virtual void evHdl(StEvent ev);

protected:
private:
   Tone::RttlCmd rttlCmd;
   void nextMelody(void);
};

extern StTimeTemp timeTempState;

#endif
