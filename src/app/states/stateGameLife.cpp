#include "stateGameLife.hpp"
#include "states.hpp"

void StateGameLife::onEnter(void)
{
}

void StateGameLife::process(void)
{
   vp.update();
   vp.tick();
}

void StateGameLife::onExit(void)
{
}

void StateGameLife::evHdl(StEvent ev)
{
   switch(ev)
   {
   case StEvent::Unknown:
      break;
   case StEvent::None:
      break;
   case StEvent::Up:
      break;
   case StEvent::Down:
      stm->transitionTo(&stateSprites);
      break;
   case StEvent::Left:
      break;
   case StEvent::Right:
      break;
   case StEvent::In:
      break;
   case StEvent::Out:
      stm->transitionTo(&timeTempState);
      break;
   default:
      break;
   }
}

StateGameLife stateGameLife;
