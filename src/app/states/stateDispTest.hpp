#ifndef STATEDISPTEST_HPP
#define STATEDISPTEST_HPP

#include "system/framework/Stm/Stm.hpp"
#include "stateEv.hpp"
#include "system/framework/sGfx/viewPort.hpp"
#include "app/views/testScrPattern_control.hpp"

#include "bsp/bsp.hpp"
#include "bsp/delay.hpp"

#include "time.hpp"
#include "timer.hpp"

class StateDispTest: public StEv
{
public:
   StateDispTest(): StEv()
   {
      testScrPtrn.setColor(sGfx::Color::white());
      testScrPtrn.setX(0);
      testScrPtrn.setY(0);
      testScrPtrn.setW(8*4);
      testScrPtrn.setH(8);

      vp.add(&testScrPtrn);
   }

   StateDispTest(Stm* parentStm): StEv(parentStm)
   {
      vp.add(&testScrPtrn);
   }

   virtual void onEnter(void);
   virtual void process(void);
   virtual void onExit(void);

protected:
private:
   sGfx::ViewPort vp;
   sGfx::TestScrPattern_Control testScrPtrn{sGfx::TestScrPattern_Control(&vp)};

   Timer aTimer;
};

extern StateDispTest stateDispTest;

#endif
