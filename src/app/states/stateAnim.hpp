#ifndef STATEANIM_HPP
#define STATEANIM_HPP

#include "system/framework/Stm/Stm.hpp"
#include "stateEv.hpp"
#include "system/framework/sGfx/viewPort.hpp"
#include "system/framework/sGfx/rect.hpp"
#include "app/views/fire_control.hpp"

#include "time.hpp"
#include "timer.hpp"

class StateAnim: public StEv
{
public:
   StateAnim(): StEv()
   {
      fire.setColor(sGfx::Color::white());
      fire.setX(0);
      fire.setY(0);
      fire.setW(8*4);
      fire.setH(8);

      rf.setColor(sGfx::Color::black());
      rf.setX(0);
      rf.setY(0);
      rf.setW(8*4);
      rf.setH(8);

      vp.add(&fire);
      vp.add(&rf);
   }

   StateAnim(Stm* parentStm): StEv(parentStm)
   {
      vp.add(&fire);
   }

   virtual void onEnter(void);
   virtual void process(void);
   virtual void onExit(void);

   void evHdl(StEvent ev);

protected:
private:
   sGfx::ViewPort vp;
   sGfx::Fire_Control fire{sGfx::Fire_Control(&vp)};
   sGfx::RectFill rf{sGfx::RectFill(&vp)};
};

extern StateAnim stateAnim;

#endif
