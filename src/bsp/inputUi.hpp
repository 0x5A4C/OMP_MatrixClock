#ifndef INPUTUI_HPP
#define INPUTUI_HPP

#include "button.hpp"

enum class InputUiEvent
{
   Unknown,
   None,
   Up,
   Down,
   Left,
   Right,
   In,
   Out
};

class InputUi
{
public:
   InputUi(void): state(InputUiEvent::Unknown)
   {
   }

   void init(void);
   InputUiEvent getState(void);
   void process();

protected:
private:
   InputUiEvent state;

   Button button;
};

#endif
