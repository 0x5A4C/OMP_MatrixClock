
#include "bsp/bsp.hpp"
#include "Arduino.h"

#include <H4.h>

#include "floatToStr.hpp"
#include "wifiInterface.hpp"
#include "thinkSpkLogger.hpp"
#include "ntpSynchr.hpp"
#include "pressHumSensor.hpp"

#include <system/device/display/displayGraphic.hpp>

//>>--->platform
#include "bsp/esp/led8x32Display.hpp"
typedef Led8x32Display display_t;
//>>--->platform
// #include "bsp/esp/dummyDisplay.hpp"
// typedef DummyDisplay display_t;

display_t bspDisplay;

DisplayGraphic* display = &bspDisplay;

namespace  Bsp
{
H4 h4;

float getTemp()
{
   return PresHumSensor::getTemp();
}

void thinkSpeakTemp_tsk()
{
   Serial.println("bsp::thinkSpeakTemp_tsk");
   ThingSpeakLogger::log(PresHumSensor::getTemp());
}

void synchTime_tsk()
{
   Serial.println("bsp::synchTime_tsk");
   Time::timestamp = NtpSynchr::getTime();
}

void init()
{
   Serial.println("bsp::init");

   PresHumSensor::init();
   WifiInt::init();
   ThingSpeakLogger::init();

   Time::timestamp = NtpSynchr::getTime();

   // led8x32Display.setPixel(0, 0, sGfx::Color::white());
   // led8x32Display.update();
   delay(1000);

   h4.every(1000*30,thinkSpeakTemp_tsk);
   h4.every(1000*60*60, synchTime_tsk);
}

void loop()
{
   h4.loop();
}

}
