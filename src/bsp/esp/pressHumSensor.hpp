#ifndef PRESS_HUM_SENSOR_HPP
#define PRESS_HUM_SENSOR_HPP

#include <Wire.h>
#include <Adafruit_BMP085.h>

namespace PresHumSensor
{

Adafruit_BMP085 bmp;

void init()
{
   Wire.begin(2, 0);
   if (!bmp.begin())
   {
      // while (1) {}
   }
}

int32_t getPress()
{
   return bmp.readPressure();
}

float getTemp()
{
   return bmp.readTemperature();
}

}

#endif
