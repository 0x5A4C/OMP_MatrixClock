#ifndef NTP_SYNCHR_HPP
#define NTP_SYNCHR_HPP

#include <NTPClient.h>
#include <WiFiUdp.h>
#include <Time.h>
#include <TimeLib.h>
#include <Timezone.h>

namespace NtpSynchr
{
//udp ntp
// Define NTP properties
#define NTP_OFFSET   60 * 60      // In seconds
#define NTP_INTERVAL 60 * 1000    // In miliseconds
#define NTP_ADDRESS  "ca.pool.ntp.org"  // change this to whatever pool is closest (see ntp.org)

// Set up the NTP UDP client
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, NTP_ADDRESS, NTP_OFFSET, NTP_INTERVAL);

time_t  getTime()
{
   // update the NTP client and get the UNIX UTC timestamp
   timeClient.update();
   unsigned long epochTime =  timeClient.getEpochTime();

   // convert received time stamp to time_t object
   time_t local, utc;
   utc = epochTime;

   // Then convert the UTC UNIX timestamp to local time
   TimeChangeRule usEDT = {"EDT", Second, Sun, Mar, 2, 60};
   TimeChangeRule usEST = {"EST", First, Sun, Nov, 2, 0};
   Timezone usEastern(usEDT, usEST);
   local = usEastern.toLocal(utc);
   return local;
}
}

#endif
