#ifndef __THINKSPKLOGGER_HPP
#define __THINKSPKLOGGER_HPP

#include <ThingSpeak.h>

namespace ThingSpeakLogger
{

//thing speak
unsigned long myChannelNumber = 143268;
const char * myWriteAPIKey = "A4RLMXL6LYHWZAX4";

void init()
{
   ThingSpeak.begin(WifiInt::client);
}

void log(float data)
{
   ThingSpeak.writeField(myChannelNumber, 1, data, myWriteAPIKey);
}

}

#endif
