#ifndef LED8x32DISPLAY_HPP
#define LED8x32DISPLAY_HPP

// #include <Adafruit_GFX.h>
#include <Max72xxPanel.h>

#include "system/device/display/displayGraphic.hpp"
#include "system/framework/sGfx/color.hpp"

class Led8x32Display: public DisplayGraphic
{
public:
   Led8x32Display()
   {
      ledMatrix.setRotation(0, 1);        // 1
      ledMatrix.setRotation(1, 1);        // 2
      ledMatrix.setRotation(2, 1);        // 3
      ledMatrix.setRotation(3, 1);        // 4
      ledMatrix.fillScreen(LOW);
      ledMatrix.drawPixel(0, 0, HIGH);
      ledMatrix.write();
      ledMatrix.setIntensity(4); // Set brightness between 0 and 15
   }

   virtual void setPixel(int16_t x, int16_t y, sGfx::Color color)
   {
      ledMatrix.drawPixel(x, y, color == sGfx::Color::white() ? HIGH : LOW);
   };

   virtual uint16_t getWidth()
   {
      return numberOfHorizontalDisplays * 8;
   }

   virtual uint16_t getHeight()
   {
      return numberOfVerticalDisplays * 8;
   }

   void update()
   {
      ledMatrix.write();
   };
protected:

private:
   const int pinCS = 16; // Attach CS to this pin, DIN to MOSI and CLK to SCK (cf http://arduino.cc/en/Reference/SPI )
   const int numberOfHorizontalDisplays = 4;
   const int numberOfVerticalDisplays = 1;

   Max72xxPanel ledMatrix = Max72xxPanel(pinCS, numberOfHorizontalDisplays, numberOfVerticalDisplays);
};

#endif
