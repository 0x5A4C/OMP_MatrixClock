#ifndef DUMMYDISPLAY_HPP
#define DUMMYDISPLAY_HPP

#include "system/device/display/displayGraphic.hpp"
#include "system/framework/sGfx/color.hpp"

class DummyDisplay: public DisplayGraphic
{
public:
   DummyDisplay()
   {
   }

   virtual void setPixel(int16_t x, int16_t y, sGfx::Color color)
   {
   };

   virtual uint16_t getWidth()
   {
      return 0;
   }

   virtual uint16_t getHeight()
   {
      return 0;
   }

   void update()
   {
   };
protected:

private:
};

#endif
