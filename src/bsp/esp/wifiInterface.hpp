#ifndef __WIFI_INTERFACE_HPP
#define __WIFI_INTERFACE_HPP

#include <ESP8266WiFi.h>

namespace WifiInt
{
char ssid[] = "335_01";    //  your network SSID (name)
char pass[] = "121119710987";   // your network password
int status = WL_IDLE_STATUS;
WiFiClient  client;

void init()
{
   WiFi.begin(ssid, pass);


   Serial.print("Connecting");
   while (WiFi.status() != WL_CONNECTED)
   {
      delay(500);
      Serial.print(".");
   }
   Serial.println();

   Serial.print("Connected, IP address: ");
   Serial.println(WiFi.localIP());
}
}
#endif
