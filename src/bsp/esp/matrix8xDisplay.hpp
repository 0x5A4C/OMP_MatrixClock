#ifndef MATRIX_8X_DISPLAY_HPP
#define MATRIX_8X_DISPLAY_HPP

#include <string.h>
#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Max72xxPanel.h>
#include <bmpSpaceInv.h>
#include <Fonts/7x5.h>

#include "floatToStr.hpp"

namespace Matrix8xDisplay
{
#define FONTNAME fnt7x55pt7b
#define FONTWIDTH 7
#define TEXT_X_OFFSET 1
#define TEXT_Y_OFFSET 0

//system
char strBuffer[10];

//io
int pinCS = 16; // Attach CS to this pin, DIN to MOSI and CLK to SCK (cf http://arduino.cc/en/Reference/SPI )
int numberOfHorizontalDisplays = 4;
int numberOfVerticalDisplays = 1;

Max72xxPanel matrix = Max72xxPanel(pinCS, numberOfHorizontalDisplays, numberOfVerticalDisplays);

void init()
{
   matrix.setRotation(0, 1);        // 1
   matrix.setRotation(1, 1);        // 2
   matrix.setRotation(2, 1);        // 3
   matrix.setRotation(3, 1);        // 4
   matrix.fillScreen(LOW);
   matrix.drawPixel(0, 0, HIGH);
   matrix.write();
   matrix.setIntensity(4); // Set brightness between 0 and 15
}

void drawGrText(char* txt)
{
   uint8_t index = 0;

   matrix.fillScreen(LOW);
   // matrix.setFont(&FONTNAME);
   matrix.drawChar(TEXT_X_OFFSET+0*FONTWIDTH+1, TEXT_Y_OFFSET, txt[0], HIGH, LOW, 1);
   matrix.drawChar(TEXT_X_OFFSET+1*FONTWIDTH+1, TEXT_Y_OFFSET, txt[1], HIGH, LOW, 1);
   matrix.drawChar(TEXT_X_OFFSET+2*FONTWIDTH-1, TEXT_Y_OFFSET, txt[2], HIGH, LOW, 1);
   matrix.drawChar(TEXT_X_OFFSET+3*FONTWIDTH-4, TEXT_Y_OFFSET, txt[3], HIGH, LOW, 1);
   matrix.drawChar(TEXT_X_OFFSET+4*FONTWIDTH-4, TEXT_Y_OFFSET, txt[4], HIGH, LOW, 1);
   matrix.write();
}

void displayTemp()
{
   floatToString(strBuffer, PresHumSensor::getTemp(), 1, 4);
   strBuffer[4] = 'C';
   strBuffer[5] = '\0';

   drawGrText(strBuffer);
}

void displayTime()
{
   static int dotsOnOff = 0;
   String timeString = "";

   if(hour(Time::timestamp) < 10)
      timeString += "0";
   timeString += hour(Time::timestamp);
   timeString += (dotsOnOff++ & 1) ? " " : ":";
   if(minute(Time::timestamp) < 10)
      timeString += "0";
   timeString += minute(Time::timestamp);

   drawGrText((char*)timeString.c_str());
}

void displaySpaceInv()
{
   static int index = 0;

   matrix.fillScreen(LOW);
   if(index == 0)
   {
      matrix.drawBitmap(0, 0, invader1a, 8, 8, HIGH);
      index = 1;
   }
   else
   {
      matrix.drawBitmap(0, 0, invader1b, 8, 8, HIGH);
      index = 0;
   }
   matrix.write();
}

}

#endif
