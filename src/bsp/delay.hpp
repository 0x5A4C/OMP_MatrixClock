#ifndef DELAY_HPP
#define DELAY_HPP

#include "stdint.h"

extern void sysDelay(uint32_t period);

#endif
