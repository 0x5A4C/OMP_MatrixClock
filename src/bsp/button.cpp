#include "button.hpp"

#include <cstddef>

#include "espressif/esp_common.h"
#include "esp/uart.h"
#include "esp8266.h"

#define MAXIMUM 5

#define PRESSED_PERIOD 10
#define RELEASED_PERIOD 200
#define PRESS_PERIOD 500
#define LONGPRESS_PERIOD (PRESS_PERIOD*3)

void Button::buttonDebounce(bool active)
{
   if(active)
   {
      if (this->integrator > 0)
      {
         this->integrator--;
      }
   }
   else
   {
      if (this->integrator < MAXIMUM)
      {
         this->integrator++;
      }
   }

   if(this->integrator == 0)
   {
      timerPressed.stop();
      if(this->state == eButtonStatePresed)
      {
         this->state = eButtonStateDown;
         timerReleased.start();
      }
      if(this->state == eButtonStateDown)
      {
         if((timerReleased.isActive()) && (timerReleased.getTicks() > RELEASED_PERIOD))
         {
            timerReleased.stop();
            printf("------------------------RELEASED\n\r");
            this->state = eButtonStateReleased;

            if(numOfPressed == 1)
            {
               printf("------------------------DOUBLEPRESSED\n\r");
               this->state = eButtonStateDoublePressed;
            }
            else
            {
               // printf("------------------------RELEASED\n\r");
               // this->state = eButtonStateReleased;
            }
         }
      }
      numOfPressed = 0;
   }
   else
   {
      if(this->integrator >= MAXIMUM)
      {
         if(this->state == eButtonStateDown)
         {
            this->state = eButtonStateUp;
         }

         if(!timerPressed.isActive())
         {
            timerPressed.start();
         }
         else
         {
            if((timerPressed.getTicks() > LONGPRESS_PERIOD) && this->state != eButtonStateLongPressed)
            {
               printf("------------------------LONGPRESSED\n\r");
               this->state = eButtonStateLongPressed;
            }
            else if((timerPressed.getTicks() > PRESSED_PERIOD) && (timerPressed.getTicks() <= LONGPRESS_PERIOD) && (this->state != eButtonStatePresed) /*((this->state < eButtonStatePresed) && (this->state > eButtonStateContLongPressed))*/)
            {
               if((this->state != eButtonStateReleased) && (this->state != eButtonStateDoublePressed))
               {
                  numOfPressed++;
               }

               printf("------------------------PRESSED\n\r");
               this->state = eButtonStatePresed;

               printf("------------------------%d\n\r", numOfPressed);
            }
         }
      }
   }
}

ButtonState Button::buttonGetState()
{
   ButtonState stateToRet = eButtonStateNone;

   if(this->state != this->stateEv)
   {
      this->stateEv = this->state;
      stateToRet = this->state;
   }

   return stateToRet;
}
