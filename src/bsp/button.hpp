#ifndef BUTTON_HPP
#define BUTTON_HPP

#include "time.hpp"
#include "timer.hpp"

#include "stdint.h"

typedef enum
{
   eButtonStateNone,
   eButtonStateUp,
   eButtonStateDown,
   eButtonStateReleased,
   eButtonStatePresed,
   eButtonStateDoublePressed,
   eButtonStateLongPressed,
} ButtonState;

class Button
{
private:
   uint16_t integrator;  /* Will range from 0 to the specified MAXIMUM */
   ButtonState state;
   ButtonState stateEv;

protected:

public:
   Button(): state(eButtonStateNone), numOfPressed(0)
   {}

   void buttonDebounce(bool active);
   ButtonState buttonGetState();

   Timer timerPressed;
   Timer timerReleased;

   uint8_t numOfPressed;
};

#endif /* BUTTON_H_ */
