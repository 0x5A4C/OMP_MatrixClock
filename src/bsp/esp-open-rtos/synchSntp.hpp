#ifndef SYNCH_SNTP_HPP
#define SYNCH_SNTP_HPP

#include <espressif/esp_common.h>
#include <esp/uart.h>

#include <string.h>
#include <stdio.h>

#include <FreeRTOS.h>
#include <task.h>

#include <lwip/err.h>
#include <lwip/sockets.h>
#include <lwip/sys.h>
#include <lwip/netdb.h>
#include <lwip/dns.h>

#include <ssid_config.h>

/* Add extras/sntp component to makefile for this include to work */
#include <sntp.h>
#include <time.h>

#define SNTP_SERVERS 	(char *)"0.pool.ntp.org", (char *)"1.pool.ntp.org", \
						(char *)"2.pool.ntp.org", (char *)"3.pool.ntp.org"

#define vTaskDelayMs(ms)	vTaskDelay((ms)/portTICK_PERIOD_MS)
#define UNUSED_ARG(x)	(void)x

namespace synchNtp
{
void setCallback(synchCallBack_t synchCallBack)
{
   setSynchCallback(synchCallBack);
}

void init(void)
{
   char *servers[] = {SNTP_SERVERS};

   /* Wait until we have joined AP and are assigned an IP */
   while (sdk_wifi_station_get_connect_status() != STATION_GOT_IP)
   {
      vTaskDelayMs(100);
   }

   /* Start SNTP */
   printf("Starting SNTP... ");
   /* SNTP will request an update each 5 minutes */
   sntp_set_update_delay(5*60000);
   /* Set GMT+1 zone, daylight savings off */
   const struct timezone tz = {1*60, 1};
   /* SNTP initialization */
   sntp_initialize(&tz);
   /* Servers must be configured right after initialization */
   sntp_set_servers(servers, sizeof(servers) / sizeof(char*));
   printf("DONE!\n");
}

void process(void)
{
   vTaskDelayMs(5000);
   time_t ts = time(NULL);
   printf("TIME: %s", ctime(&ts));
}
}

#endif
