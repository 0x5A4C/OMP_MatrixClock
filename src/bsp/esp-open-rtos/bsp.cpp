#include "system/device/display/displayGraphic.hpp"
#include "bsp/bsp.hpp"

#include "wifiCtrl.hpp"
#include "tempSensor.hpp"
#include "thinkSpeak.hpp"
#include "synchSntp.hpp"
#include "matrixDisp.hpp"
#include "i2c.hpp"
#include "rtc.hpp"
#include "tcpipCom.hpp"
#include "irPilot.hpp"

#include <stdio.h>

#include "FreeRTOS.h"
#include "task.h"

#include "freeRTOSCpp.hpp"

#define TASK_STACK 256

typedef Led8x32Display display_t;
display_t bspDisplay;
DisplayGraphic* display = &matrixDisp::led8x32Display;

#define vTaskDelayMs(ms)	vTaskDelay((ms)/portTICK_PERIOD_MS)

time_t getSystemTicks(void)
{
   return (time_t)(xTaskGetTickCount() * portTICK_PERIOD_MS);
}

//------------------------------------------------------------------------------
/**
 * @brief Names for Base set of Priorities.
 *
 * Assigns used based names to priority levels, optimized for configMAX_PRIORITIES = 6 for maximal distinctions
 * with reasonable collapsing for smaller values. If configMAX_PRIORITIES is >6 then some values won't have names here, but
 * values could be created and cast to the enum type.
 *
 * | configMAX_PRIORITIES: | 1 | 2 | 3 | 4 | 5 | 6 | N>6 | Use                                                |
 * | --------------------: | - | - | - | - | - | - | :-: | :------------------------------------------------- |
 * | TaskPrio_Idle         | 0 | 0 | 0 | 0 | 0 | 0 |  0  | Non-Real Time operations, Tasks that don't block   |
 * | TaskPrio_Low          | 0 | 1 | 1 | 1 | 1 | 1 |  1  | Non-Critical operations                            |
 * | TaskPrio_HMI          | 0 | 1 | 1 | 1 | 1 | 2 |  2  | Normal User Interface                              |
 * | TaskPrio_Mid          | 0 | 1 | 1 | 2 | 2 | 3 | N/2 | Semi-Critical, Deadlines, not much processing      |
 * | TaskPrio_High         | 0 | 1 | 2 | 3 | 3 | 4 | N-2 | Urgent, Short Deadlines, not much processing       |
 * | TaskPrio_Highest      | 0 | 1 | 2 | 3 | 4 | 5 | N-1 | Critical, do NOW, must be quick (Used by FreeRTOS) |
 *
 * @ingroup FreeRTOSCpp
 */
enum BspTaskPriority
{
   BspTaskPrio_Idle = 0,                                                        ///< Non-Real Time operations. tasks that don't block
   BspTaskPrio_Low = ((configMAX_PRIORITIES)>1),                                ///< Non-Critical operations
   BspTaskPrio_HMI = (TaskPrio_Low + ((configMAX_PRIORITIES)>5)),               ///< Normal User Interface Level
   BspTaskPrio_Mid = ((configMAX_PRIORITIES)/2),                                ///< Semi-Critical, have deadlines, not a lot of processing
   BspTaskPrio_High = ((configMAX_PRIORITIES)-1-((configMAX_PRIORITIES)>4)),    ///< Urgent tasks, short deadlines, not much processing
   BspTaskPrio_Highest = ((configMAX_PRIORITIES)-1)                             ///< Critical Tasks, Do NOW, must be quick (Used by FreeRTOS)
};

class BspTaskCommon
{
public:
   virtual void create(void) = 0;
protected:
   virtual void task() = 0;

   static void taskWrap(void* pvParameters)
   {
      ((BspTaskCommon*)(pvParameters))->task();
      // if(pvParameters != 0)
      // {
      // }
   }
private:
};

template <uint16_t stackSize, uint8_t priority>
class BspTask: public BspTaskCommon
{
public:
   BspTask() {};

   void create(void)
   {
      xTaskCreate(BspTask::taskWrap, "", stackSize, this, priority, NULL);
   }
protected:
private:
};

template <uint16_t stackSize, uint8_t priority>
class BspStaticTask: public BspTaskCommon
{
public:
   BspStaticTask() {};

   void create(void)
   {
      xTaskCreateStatic(
         BspStaticTask::taskWrap, /* Function that implements the task. */
         "", /* Text name for the task. */
         stackSize, /* Number of indexes in the xStack array. */
         this, /* Parameter passed into the task. */
         priority, /* Priority at which the task is created. */
         taskStack, /* Array to use as the task's stack. */
         &taskDataStr ); /* Variable to hold the task's data structure. */
   }
protected:
private:
   StaticTask_t taskDataStr;
   StackType_t taskStack[stackSize];
};

namespace  Bsp
{
TimerHandle_t i2cDev_timer, synchNtp_timer, synchThinkSpeak_timer;

std::function<void(void)> bspUpEventHdl;
std::function<void(void)> bspDownEventHdl;
std::function<void(void)> bspLeftEventHdl;
std::function<void(void)> bspRightEventHdl;
std::function<void(void)> bspInEventHdl;
std::function<void(void)> bspOutEventHdl;

InputUi inputUi;
Tone tone;

float getTemp()
{
   return tempSenesor::getTemp();
}

class BspI2cDevTask: public BspStaticTask<(TASK_STACK), BspTaskPrio_Low>
{
private:
   int8_t i = 60;

   virtual void task()
   {
      tempSenesor::init(i2c::getI2cId());
      rtc::init(i2c::getI2cId());

      while(1)
      {
         printf("getTemPress_task\n");
         tempSenesor::process();
         if(i == 60)
         {
            i = 0;
            rtc::process();
            printf("rtc::process\n");
         }
         i++;
         vTaskDelayMs(1000);
      }
   }
};
static BspI2cDevTask bspI2cDevTask;

class BspSynchNtpTask: public BspStaticTask<(TASK_STACK*2), BspTaskPrio_Low>
{
private:
   static void sntpSynchCallback(void)
   {
      time_t timeTs = time(NULL);
      struct tm *ltime = localtime((const time_t*)&timeTs);

      printf("Sntp date/time is: %s", asctime(ltime));

      rtc::setTime(ltime);

      Time::timestamp = time(NULL);
   }

   virtual void task()
   {
      synchNtp::setCallback((synchCallBack_t)BspSynchNtpTask::sntpSynchCallback);
      synchNtp::init();

      while(1)
      {
         printf("synchNtp_task\n");
         synchNtp::process();
         vTaskDelayMs(10000);
      }
   }
};
static BspSynchNtpTask bspSynchNtpTask;

class BspSynchTSpeakTask: public BspStaticTask<(TASK_STACK*4), BspTaskPrio_Low>
{
private:
   virtual void task()
   {
      thinkSpeak::init();

      while(1)
      {
         printf("synchThinkSpeak_task\n");
         thinkSpeak::process();
         vTaskDelayMs(60000);
      }
   }
};
static BspSynchTSpeakTask bspSynchTSpeakTask;

class BspTcpipComTask: public BspStaticTask<(TASK_STACK*2), BspTaskPrio_Low>
{
private:
   virtual void task()
   {
      tcpipCom::init();

      while(1)
      {
         // printf("tcpipCom_task\n");
         tcpipCom::process();
      }
   }
};
static BspTcpipComTask bspTcpipComTask;

class BspTimestampTask: public BspStaticTask<TASK_STACK, BspTaskPrio_Low>
{
private:
   virtual void task()
   {
      while(1)
      {
         Time::timestamp++;
         vTaskDelayMs(1000);
      }
   }
};
static BspTimestampTask bspTimestampTask;

class BspInputUITask: public BspStaticTask<TASK_STACK, BspTaskPrio_Low>
{
private:
   virtual void task()
   {
      while(1)
      {
         Bsp::inputUi.process();
         vTaskDelayMs(10);
      }
   }
};
static BspInputUITask bspInputUITask;

class BspEventTask: public BspStaticTask<TASK_STACK, BspTaskPrio_Low>
{
private:
   virtual void task()
   {
      while(1)
      {
#if 0
         switch(Bsp::inputUi.getState())
         {
         case InputUiEvent::Unknown:
            break;
         case InputUiEvent::None:
            break;
         case InputUiEvent::Up:
            if(Bsp::bspUpEventHdl)
            {
               Bsp::bspUpEventHdl();
            }
            break;
         case InputUiEvent::Down:
            if(Bsp::bspDownEventHdl)
            {
               Bsp::tone.signal(Tone::BeepCmd::bip);
               Bsp::bspDownEventHdl();
            }
            break;
         case InputUiEvent::Left:
            if(Bsp::bspLeftEventHdl)
            {
               Bsp::bspLeftEventHdl();
            }
            break;
         case InputUiEvent::Right:
            if(Bsp::bspRightEventHdl)
            {
               Bsp::tone.signal(Tone::BeepCmd::bop);
               Bsp::bspRightEventHdl();
            }
            break;
         case InputUiEvent::In:
            if(Bsp::bspInEventHdl)
            {
               Bsp::bspInEventHdl();
            }
            break;
         case InputUiEvent::Out:
            if(Bsp::bspOutEventHdl)
            {
               Bsp::tone.signal(Tone::BeepCmd::bleep);
               Bsp::bspOutEventHdl();
            }
            break;
         default:
            break;
         }
#endif
#if 0
         switch(iRPilot::getIrCode())
         {
         case IrPilot::ButtonCode::Unknown:
            printf("ir button: Unknown\n");
            break;
         case IrPilot::ButtonCode::OnOff:
            printf("ir button: OnOff\n");
            break;
         case IrPilot::ButtonCode::Source:
            printf("ir button: Source\n");
            break;
         case IrPilot::ButtonCode::NoSound:
            printf("ir button: NoSound\n");
            break;
         case IrPilot::ButtonCode::Record:
            printf("ir button: Record\n");
            break;
         case IrPilot::ButtonCode::ChPlus:
            printf("ir button: ChPlus\n");
            break;
         case IrPilot::ButtonCode::TimeShift:
            printf("ir button: TimeShift\n");
            break;
         case IrPilot::ButtonCode::VolMin:
            printf("ir button: VolMin\n");
            break;
         case IrPilot::ButtonCode::FullScr:
            printf("ir button: FullScr\n");
            break;
         case IrPilot::ButtonCode::VolPlus:
            printf("ir button: VolPlus\n");
            break;
         case IrPilot::ButtonCode::But0:
            printf("ir button: But0\n");
            break;
         case IrPilot::ButtonCode::ChMinus:
            printf("ir button: ChMinus\n");
            break;
         case IrPilot::ButtonCode::Recall:
            printf("ir button: Recall\n");
            break;
         case IrPilot::ButtonCode::But1:
            printf("ir button: But1\n");
            break;
         case IrPilot::ButtonCode::But2:
            printf("ir button: But2\n");
            break;
         case IrPilot::ButtonCode::But3:
            printf("ir button: But3\n");
            break;
         case IrPilot::ButtonCode::But4:
            printf("ir button: But4\n");
            break;
         case IrPilot::ButtonCode::But5:
            printf("ir button: But5\n");
            break;
         case IrPilot::ButtonCode::But6:
            printf("ir button: But6\n");
            break;
         case IrPilot::ButtonCode::But7:
            printf("ir button: But7\n");
            break;
         case IrPilot::ButtonCode::But8:
            printf("ir button: But8\n");
            break;
         case IrPilot::ButtonCode::But9:
            printf("ir button: But9\n");
            break;
         case IrPilot::ButtonCode::RepeatedLastButton:
            printf("ir button: RepeatedLastButton\n");
            break;
         case IrPilot::ButtonCode::None:
            // printf("ir button: None\n");
            break;
         default:
            // printf("default\n");
            break;
         }
#endif
         switch(iRPilot::getIrCode())
         {
         case  IrPilot::ButtonCode::ChPlus:
            if(Bsp::bspUpEventHdl)
            {
               Bsp::bspUpEventHdl();
            }
            break;
         case  IrPilot::ButtonCode::ChMinus:
            if(Bsp::bspDownEventHdl)
            {
               Bsp::tone.signal(Tone::BeepCmd::bip);
               Bsp::bspDownEventHdl();
            }
            break;
         case  IrPilot::ButtonCode::VolMin:
            if(Bsp::bspLeftEventHdl)
            {
               Bsp::bspLeftEventHdl();
            }
            break;
         case  IrPilot::ButtonCode::VolPlus:
            if(Bsp::bspRightEventHdl)
            {
               Bsp::tone.signal(Tone::BeepCmd::bop);
               Bsp::bspRightEventHdl();
            }
            break;
         case  IrPilot::ButtonCode::FullScr:
            if(Bsp::bspInEventHdl)
            {
               Bsp::bspInEventHdl();
            }
            break;
         case  IrPilot::ButtonCode::Source:
            if(Bsp::bspOutEventHdl)
            {
               Bsp::tone.signal(Tone::BeepCmd::bleep);
               Bsp::bspOutEventHdl();
            }
            break;
         default:
            break;
         }

         vTaskDelayMs(100);
      }
   }
};
static BspEventTask bspEventTask;

class BspBeeperTask: public BspStaticTask<TASK_STACK, BspTaskPrio_Low>
{
private:
   //  Tone tone;
   virtual void task()
   {
      // tone.init();
      while(1)
      {
         Bsp::tone.process();
         vTaskDelayMs(10);
      }
   }
};
static BspBeeperTask bspBeeperTask;

void bspInit()
{
   wifiCtrl::init();
   i2c::init();
   iRPilot::init();

   //  inputUi.init();
   tone.init();

   bspI2cDevTask.create();
   bspSynchNtpTask.create();
   bspSynchTSpeakTask.create();
   bspTcpipComTask.create();
   bspTimestampTask.create();
   //  bspInputUITask.create();
   bspEventTask.create();
   bspBeeperTask.create();
}

void bspLoop()
{
}
}
