#ifndef WIFI_CTRL_H
#define WIFI_CTRL_H

#include <espressif/esp_common.h>
#include <esp/uart.h>

#include <string.h>
#include <stdio.h>

#include <FreeRTOS.h>
#include <task.h>

#include <lwip/err.h>
#include <lwip/sockets.h>
#include <lwip/sys.h>
#include <lwip/netdb.h>
#include <lwip/dns.h>

#include <ssid_config.h>

/* Add extras/sntp component to makefile for this include to work */
#include <sntp.h>
#include <time.h>

#define vTaskDelayMs(ms) vTaskDelay((ms)/portTICK_PERIOD_MS)

namespace wifiCtrl
{
struct sdk_station_config config = {WIFI_SSID, WIFI_PASS };

static const char* getIp(void)
{
   static char ip[16] = "0.0.0.0";
   ip[0] = 0;
   struct ip_info ipinfo;
   (void) sdk_wifi_get_ip_info(STATION_IF, &ipinfo);
   snprintf(ip, sizeof(ip), IPSTR, IP2STR(&ipinfo.ip));
   return (char*) ip;
}

void init(void)
{
   sdk_wifi_set_opmode(STATION_MODE);
   sdk_wifi_station_set_config(&config);

   /* Wait until we have joined AP and are assigned an IP */
   // while (sdk_wifi_station_get_connect_status() != STATION_GOT_IP)
   // {
   //   sdk_os_delay_us(5000);
   // }
}
}

#endif
