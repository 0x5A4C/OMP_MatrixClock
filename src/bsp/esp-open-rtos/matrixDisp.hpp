#ifndef MATRIXDISP_HPP
#define MATRIXDISP_HPP

#include <max7219/max7219.h>

extern "C" void sendFrameBuf(const max7219_display_t *disp, uint8_t *frameBuf);
extern "C" void writeFb(const max7219_display_t *disp);
extern "C" void drawPixel(const max7219_display_t *disp, int16_t xx, int16_t yy, uint16_t color);
extern "C" void setRotation(max7219_display_t *disp, uint8_t display, uint8_t rotation);
extern "C" void fillScreen(max7219_display_t *disp, uint16_t color);

#include "system/device/display/displayGraphic.hpp"
#include "system/framework/sGfx/color.hpp"

#define CS_PIN 16
#define HIGH   1
#define LOW    0

class Led8x32Display: public DisplayGraphic
{
public:
   Led8x32Display()
   {
      // ledMatrix.setRotation(0, 1);        // 1
      // ledMatrix.setRotation(1, 1);        // 2
      // ledMatrix.setRotation(2, 1);        // 3
      // ledMatrix.setRotation(3, 1);        // 4
      // ledMatrix.fillScreen(LOW);
      // ledMatrix.drawPixel(0, 0, HIGH);
      // ledMatrix.write();
      // ledMatrix.setIntensity(4); // Set brightness between 0 and 15

      disp.cs_pin       = CS_PIN;
      disp.digits       = 8;
      disp.cascade_size = 4;
      disp.mirrored     = true;

      max7219_matrix_init(&disp, 4, 1);
      setRotation(&disp, 0, 1);
      setRotation(&disp, 1, 1);
      setRotation(&disp, 2, 1);
      setRotation(&disp, 3, 1);

      fillScreen(&disp, 0);
   }

   virtual void setPixel(int16_t x, int16_t y, sGfx::Color color)
   {
      drawPixel(&disp, x,  y, color == sGfx::Color::white() ? HIGH : LOW);
   };

   virtual uint16_t getWidth()
   {
      return 4 * 8;
   }

   virtual uint16_t getHeight()
   {
      return 1 * 8;
   }

   void update()
   {
      // ledMatrix.write();
      writeFb(&disp);
   };
protected:

private:
   // const int pinCS = 16; // Attach CS to this pin, DIN to MOSI and CLK to SCK (cf http://arduino.cc/en/Reference/SPI )
   // const int numberOfHorizontalDisplays = 4;
   // const int numberOfVerticalDisplays = 1;
   //
   // Max72xxPanel ledMatrix = Max72xxPanel(pinCS, numberOfHorizontalDisplays, numberOfVerticalDisplays);

   max7219_display_t disp;
};

namespace matrixDisp
{
Led8x32Display led8x32Display;

//
// #define CS_PIN 16
//
// uint8_t fb[4] = {1,2,3,4};
//
// static max7219_display_t disp =
// {
//     .cs_pin       = CS_PIN,
//     .digits       = 8,
//     .cascade_size = 4,
//     .mirrored     = true
// };

void init(void)
{
   // max7219_matrix_init(&disp, 4, 1);
   // setRotation(&disp, 0, 1);
   // setRotation(&disp, 1, 1);
   // setRotation(&disp, 2, 1);
   // setRotation(&disp, 3, 1);
   // fillScreen(&disp, 0);
   // drawPixel(&disp, 0,  0, 1);
   // drawPixel(&disp, 8,  1, 1);
   // drawPixel(&disp, 16, 2, 1);
   // drawPixel(&disp, 24, 3, 1);

   led8x32Display.setPixel(0, 0, sGfx::Color::white());
}

void process(void)
{
   // writeFb(&disp);
   led8x32Display.update();
}
}



#endif
