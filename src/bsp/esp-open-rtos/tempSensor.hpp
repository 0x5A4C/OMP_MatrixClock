#ifndef TEMP_SENSOR_HPP
#define TEMP_SENSOR_HPP

// #include "espressif/esp_common.h"

#include <stdio.h>

#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "queue.h"
#include "semphr.h"
// BMP180 driver
#include "bmp180/bmp180.h"

#define MY_EVT_TIMER  0x01
#define MY_EVT_BMP180 0x02

namespace tempSenesor
{
//device descriptor
i2c_dev_t bmp180_dev;

typedef struct
{
   uint8_t event_type;
   bmp180_result_t bmp180_data;
} bmp180_event_t;

// Create Main Communication Queue
static QueueHandle_t mainqueue = xQueueCreate(10, sizeof(bmp180_event_t));
SemaphoreHandle_t xSemaphore = xSemaphoreCreateMutex();

bmp180_temp_t temp = 0;
bmp180_press_t press = 0;

bmp180_temp_t getTemp(void)
{
   bmp180_temp_t _temp;

   if( xSemaphoreTake( xSemaphore, ( TickType_t ) 10 ) == pdTRUE )
   {
      _temp = temp;

      xSemaphoreGive( xSemaphore );
   }
   else
   {
      _temp = 0;
   }

   return _temp;
}

bmp180_press_t getPress(void)
{
   bmp180_press_t _press;

   if( xSemaphoreTake( xSemaphore, ( TickType_t ) 10 ) == pdTRUE )
   {
      _press = press;

      xSemaphoreGive( xSemaphore );
   }
   else
   {
      _press = 0;
   }

   return _press;
}

bool bmp180_callBack(const QueueHandle_t* resultQueue, uint8_t cmd, bmp180_temp_t temperature, bmp180_press_t pressure)
{
   // bmp180_event_t ev;

   printf("%s: Received BMP180 Event temp:=%d.%dC press=%d.%02dhPa\n", __FUNCTION__, \
          (int32_t)temperature, abs((int32_t)(temperature*10)%10), \
          pressure/100, pressure%100 );

   if( xSemaphoreTake( xSemaphore, ( TickType_t ) 10 ) == pdTRUE )
   {
      temp = temperature;
      press = pressure;

      xSemaphoreGive( xSemaphore );
   }
   else
   {
   }

   // ev.event_type = MY_EVT_BMP180;
   // ev.bmp180_data.cmd = cmd;
   // ev.bmp180_data.temperature = temperature;
   // ev.bmp180_data.pressure = pressure;
   //
   // return (xQueueSend(*resultQueue, &ev, 0) == pdTRUE);

   return true;
}

void init(int i2cId)
{
   bmp180_dev.addr = BMP180_DEVICE_ADDRESS;
   bmp180_dev.bus = i2cId;
   bmp180_informUser = bmp180_callBack;
   bmp180_init(&bmp180_dev);
}

void process(void)
{
   bmp180_trigger_measurement(&bmp180_dev, &mainqueue);
}
}

#endif
