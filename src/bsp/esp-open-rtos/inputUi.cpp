#include "espressif/esp_common.h"
#include "esp/uart.h"
#include "esp8266.h"

#include "bsp/inputUi.hpp"

#include "apds9960/apds9960.hpp"

const int gpio = 0;
const int active = 0;

Apds9960 apds9960;

InputUiEvent InputUi::getState(void)
{
   InputUiEvent stateToRet = state;

   state = InputUiEvent::None;
   return stateToRet;
}

void InputUi::init(void)
{
   // gpio_set_pullup(gpio, true, true);
   gpio_enable(gpio, GPIO_INPUT);
   // gpio_enable(gpio, GPIO_OUT_OPEN_DRAIN);

   //  apds9960.init(0);
   //  apds9960.enableGestureSensor(false);

}

void InputUi::process()
{
   button.buttonDebounce(gpio_read(gpio) != active);
   switch(button.buttonGetState())
   {
   case eButtonStateNone:
      // printf("eButtonStateNone\n\r");
      break;
   case eButtonStateReleased:
      printf("eButtonStateReleased\n\r");
      state = InputUiEvent::Down;
      break;
   case eButtonStatePresed:
      printf("eButtonStatePresed\n\r");
      break;
   case eButtonStateLongPressed:
      printf("eButtonStateLongPressed\n\r");
      state = InputUiEvent::Out;
      break;
   case eButtonStateDoublePressed:
      printf("eButtonStateDoublePressed\n\r");
      state = InputUiEvent::Right;
      break;
   default:
      // printf("default");
      break;
   }

   //  uint8_t isGestureAvailable = apds9960.isGestureAvailable();
   //  if(isGestureAvailable == true)
   {
      //  printf("----------------------->\n\r");
#if 0
      switch(apds9960.readGesture())
      {
      case DIR_NONE:
         printf("NONE\n\r");
         break;
      case DIR_LEFT:
         printf("LEFT\n\r");
         break;
      case DIR_RIGHT:
         printf("RIGHT\n\r");
         break;
      case DIR_UP:
         printf("UP\n\r");
         break;
      case DIR_DOWN:
         printf("DOWN\n\r");
         break;
      case DIR_NEAR:
         printf("NEAR\n\r");
         break;
      case DIR_FAR:
         printf("FAR\n\r");
         break;
      case DIR_ALL:
         printf("ALL\n\r");
         break;
      default:
         // printf("default");
         break;
      }
#endif
   }
}
