#ifndef I2C_HPP
#define I2C_HPP

#define I2C_BUS 0
// #define SCL_PIN GPIO_ID_PIN((0))
// #define SDA_PIN GPIO_ID_PIN((2))
#define SCL_PIN GPIO_ID_PIN((5))
#define SDA_PIN GPIO_ID_PIN((4))

namespace i2c
{
void init(void)
{
   i2c_init(I2C_BUS, SCL_PIN, SDA_PIN, I2C_FREQ_100K);
}

int getI2cId(void)
{
   return I2C_BUS;
}
}

#endif
