#ifndef RTC_HPP
#define RTC_HPP

#include <stdint.h>
#include <stdlib.h>

#include "ds3231/ds3231.h"
#define ADDR DS3231_ADDR

#define TIMER_COUNT			RTC.COUNTER
// daylight settings
// Base calculated with value obtained from NTP server (64 bits)
#define sntp_base	(*((uint64_t*)RTC.SCRATCH))
// Timer value when base was obtained
#define tim_ref 	(RTC.SCRATCH[2])
// Calibration value -- ( microseconds / RTC tick ) * 2^12
#define cal 		(RTC.SCRATCH[3])

namespace rtc
{
struct tm time;
float tempFloat;
i2c_dev_t dev;

void updateSysRtc(time_t t)
{

   uint32_t now_rtc = TIMER_COUNT;

   // Apply daylight and timezone correction
   // t += (stz.tz_minuteswest + stz.tz_dsttime * 60) * 60;

   cal = sdk_system_rtc_clock_cali_proc();
   tim_ref = now_rtc;
   sntp_base = (((uint64_t)t * 1000000U) <<12) / cal;
}

void init(int i2cId)
{
   dev.addr = ADDR;
   dev.bus = i2cId;
}

void process(void)
{
   ds3231_getTime(&dev, &time);
   ds3231_getTempFloat(&dev, &tempFloat);
   updateSysRtc(mktime(&time));
   Time::timestamp = mktime(&time);//time(NULL);
   printf("RTC synchronization date/time is: %s", asctime(&time));
}

void setTime(struct tm *timeTs)
{
   ds3231_setTime(&dev, timeTs);
}

struct tm getTime(void)
{
   return time;
}

float getTemp(void)
{
   return tempFloat;
}
}

#endif
