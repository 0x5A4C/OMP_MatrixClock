#ifndef THINKSPEAK_SENSOR_HPP
#define THINKSPEAK_SENSOR_HPP

#include "espressif/esp_common.h"
#include "esp/uart.h"

#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "FreeRTOS.h"
#include "task.h"

#include "lwip/err.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"
#include "lwip/netdb.h"
#include "lwip/dns.h"

#include "tempSensor.hpp"

#define bzero(b,len) (memset((b), '\0', (len)), (void) 0)

namespace thinkSpeak
{
#define WEB_SERVER "api.thingspeak.com"
#define WEB_PORT 80
#define WEB_PATH "/"

const char* apiKey = "A4RLMXL6LYHWZAX4";

void writeField(char* fieldValue, const char *writeAPIKey)
{
   uint8_t failures = 0;
   uint8_t successes = 0;

#if 1
   struct addrinfo hints;

   hints.ai_family = AF_UNSPEC;
   hints.ai_socktype = SOCK_STREAM;

   struct addrinfo *res;

   printf("Running DNS lookup for %s...\r\n", WEB_SERVER);
   int err = getaddrinfo(WEB_SERVER, "80", &hints, &res);

   if (err != 0 || res == NULL)
   {
      printf("DNS lookup failed err=%d res=%p\r\n", err, res);
      if(res)
         freeaddrinfo(res);
      vTaskDelay(1000 / portTICK_PERIOD_MS);
      failures++;
      return;
   }

#if LWIP_IPV6
   {
      struct netif *netif = sdk_system_get_netif(0);
      int i;
      for (i = 0; i < LWIP_IPV6_NUM_ADDRESSES; i++)
      {
         printf("  ip6 %d state %x\n", i, netif_ip6_addr_state(netif, i));
         if (!ip6_addr_isinvalid(netif_ip6_addr_state(netif, i)))
            printf("  ip6 addr %d = %s\n", i, ip6addr_ntoa(netif_ip6_addr(netif, i)));
      }
   }
#endif

   struct sockaddr *sa = res->ai_addr;
   if (sa->sa_family == AF_INET)
   {
      printf("DNS lookup succeeded. IP=%s\r\n", inet_ntoa(((struct sockaddr_in *)sa)->sin_addr));
   }
#if LWIP_IPV6
   if (sa->sa_family == AF_INET6)
   {
      printf("DNS lookup succeeded. IP=%s\r\n", inet6_ntoa(((struct sockaddr_in6 *)sa)->sin6_addr));
   }
#endif
#endif
   int s = socket(res->ai_family, res->ai_socktype, 0);
   if(s < 0)
   {
      printf("... Failed to allocate socket.\r\n");
      freeaddrinfo(res);
      vTaskDelay(1000 / portTICK_PERIOD_MS);
      failures++;
      return;
   }

   printf("... allocated socket\r\n");

   if(connect(s, res->ai_addr, res->ai_addrlen) != 0)
   {
      close(s);
      freeaddrinfo(res);
      printf("... socket connect failed.\r\n");
      vTaskDelay(4000 / portTICK_PERIOD_MS);
      failures++;
      return;
   }

   printf("... connected\r\n");
   freeaddrinfo(res);

   // const char *req =
   //     "GET "WEB_PATH" HTTP/1.1\r\n"
   //     "Host: "WEB_SERVER"\r\n"
   //     "User-Agent: esp-open-rtos/0.1 esp8266\r\n"
   //     "Connection: close\r\n"
   //     "\r\n";
#if 0
   const char *req =
      "GET /update?key=A4RLMXL6LYHWZAX4&field1=123 HTTP/1.1\r\n"
      "Host: \"WEB_SERVER\"\r\n"
      "User-Agent: esp-open-rtos/0.1 esp8266\r\n"
      "Connection: close\r\n"
      "\r\n";

   if (write(s, req, strlen(req)) < 0)
   {
      printf("... socket send failed\r\n");
      close(s);
      vTaskDelay(4000 / portTICK_PERIOD_MS);
      failures++;
      return;
   }
#endif
   const char *req_00 = "GET /update";
   const char *req_01 = "?key=";
   const char *req_02 = writeAPIKey;
   const char *req_03 = "&field1=";
   const char *req_04 = fieldValue;
   const char *req_05 = " HTTP/1.1\r\n";
   const char *req_06 = "Host: \"WEB_SERVER\"\r\n";
   const char *req_07 = "User-Agent: esp-open-rtos/0.1 esp8266\r\n";
   const char *req_08 = "Connection: close\r\n";
   const char *req_09 = "\r\n";

   write(s, req_00, strlen(req_00));
   write(s, req_01, strlen(req_01));
   write(s, req_02, strlen(req_02));
   write(s, req_03, strlen(req_03));
   write(s, req_04, strlen(req_04));
   write(s, req_05, strlen(req_05));
   write(s, req_06, strlen(req_06));
   write(s, req_07, strlen(req_07));
   write(s, req_08, strlen(req_08));
   write(s, req_09, strlen(req_09));

   printf("... socket send success\r\n");

   static char recv_buf[128];
   int r;
   do
   {
      bzero(recv_buf, 128);
      r = read(s, recv_buf, 127);
      if(r > 0)
      {
         printf("%s", recv_buf);
      }
   }
   while(r > 0);

   printf("... done reading from socket. Last read return=%d errno=%d\r\n", r, errno);
   if(r != 0)
      failures++;
   else
      successes++;
   close(s);
   printf("successes = %d failures = %d\r\n", successes, failures);

}

void writeField(int8_t fieldValue, const char *writeAPIKey)
{
   char buffer [10];

   snprintf(buffer, sizeof(buffer), "%d", fieldValue);
   writeField(buffer, apiKey);
}

void init(void)
{
}

void process(void)
{
   char buffer [10];

   /* Wait until we have joined AP and are assigned an IP */
   while (sdk_wifi_station_get_connect_status() != STATION_GOT_IP)
   {
      vTaskDelayMs(1000);
   }

   bmp180_temp_t temp = tempSenesor::getTemp();
   snprintf(buffer, sizeof(buffer), "%d.%d", (int32_t)temp, abs((int32_t)(temp*10)%10));
   writeField(buffer, apiKey);

   printf("post data to thinkSpeak starting...\r\n");
}

}

#endif
