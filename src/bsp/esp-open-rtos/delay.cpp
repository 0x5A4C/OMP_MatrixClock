#include "delay.hpp"

#include "FreeRTOS.h"
#include "task.h"

#define vTaskDelayMs(ms)	vTaskDelay((ms)/portTICK_PERIOD_MS)

void sysDelay(uint32_t period)
{
   vTaskDelayMs(period);
}
