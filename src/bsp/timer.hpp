#ifndef TIMER_H
#define TIMER_H

#include "time.hpp"
#include "stdint.h"

extern time_t getSystemTicks(void);

class Timer
{
public:
   Timer();
   bool isStarted();
   bool isStopped();
   bool isPaused();
   bool isActive();
   void pause();
   void resume();
   void stop();
   void start();
   void reset();
   time_t getTicks();
private:
   time_t startedAt;
   time_t pausedAt;
   bool started;
   bool paused;
};

#endif
