
#ifndef SPRITESANDANIMS_HPP
#define SPRITESANDANIMS_HPP

#include <inttypes.h>
#include <stdlib.h>

extern const uint8_t aSprite00[2][8];
extern const uint8_t aSprite01[2][8];
extern const uint8_t aSprite02[2][8];
extern const uint8_t aSprite03[2][8];

extern const uint8_t aGifSprite00[30][8];
extern const uint8_t aGifSprite01[30][8];
extern const uint8_t aGifSprite02[30][8];
extern const uint8_t aGifSprite03[30][8];

#endif
