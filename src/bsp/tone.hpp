#ifndef TONE_HPP
#define TONE_HPP

#include "FreeRTOS.h"
#include "queue.h"
// #include "task.h"

class Tone
{
public:
   Tone(void)
   {
      signalQueue = xQueueCreate( 10, sizeof( SignalCmd ) );
      eventQueue = xQueueCreate( 10, sizeof( SignalEvent ) );
   }

   enum class SignalCmd
   {
      unknown,
      bip,
      bop,
      beep,
      bleep,
      buzz,
      siren,
      trill,
      phone,
      last
   };

   enum class EventGroup
   {
      unknown,
      beep,
      rttl,
      last
   };

   enum class BeepCmd
   {
      unknown,
      bip,
      bop,
      beep,
      bleep,
      buzz,
      siren,
      trill,
      phone,
      last
   };

   struct BeepEvent
   {
      BeepCmd beepCmd;
      uint8_t numOfReps;
   };

   enum class RttlCmd
   {
      unknown,
      The_Simpsons,
      Indiana,
      TakeOnMe,
      Entertainer,
      Muppets,
      Xfiles,
      Looney,
      Studio20thCenFox,
      Bond,
      MASH,
      StarWars,
      GoodBad,
      TopGun,
      A_Team,
      Flinstones,
      Jeopardy,
      Gadget,
      Smurfs,
      MahnaMahna,
      LeisureSuit,
      MissionImp,
      last
   };

   struct RttlEvent
   {
      RttlCmd rttlCmd;
      uint8_t numOfReps;
   };

   union EventParams
   {
      BeepEvent beepEvent;
      RttlEvent rttlEvent;
   };

   typedef struct SignalEvent
   {
      EventGroup eventGroup;
      EventParams eventParams;
   } SignalEvent;

   void stop(void);
   void start(void);
   void play(uint16_t frequency, uint32_t duration = 0);

   void playRttl(const char *p);

   void signal(SignalCmd signal);
   void signal(SignalEvent event);
   void signal(BeepEvent beepEvent);
   void signal(RttlEvent rttlEvent);
   void signal(BeepCmd BeepCmd);
   void signal(RttlCmd rttlCmd);

   void bip(int n = 1);
   void bop(int n = 1);
   void beep(int n = 1);
   void bleep(int n = 1);
   void buzz(int n = 1);
   void siren(int n = 1);
   void trill(int n = 1);
   void phone(int n = 1);

   void init(void);
   void process();

protected:
private:
   QueueHandle_t signalQueue;
   QueueHandle_t eventQueue;

   void process(SignalEvent signalEvent);
   void process(BeepEvent beepEvent);
   void process(RttlEvent rttlEvent);

};

#endif
