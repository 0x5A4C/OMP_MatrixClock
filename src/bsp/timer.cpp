#include "timer.hpp"

Timer::Timer()
{
   startedAt = 0;
   pausedAt = 0;
   paused = false;
   started = false;
}

bool Timer::isStarted()
{
   return started;
}

bool Timer::isStopped()
{
   return !started;
}

bool Timer::isPaused()
{
   return paused;
}

bool Timer::isActive()
{
   return !paused & started;
}

void Timer::pause()
{
   if( paused || !started )
      return;
   paused = true;
   pausedAt = getSystemTicks();
}

void Timer::resume()
{
   if( !paused )
      return;
   paused = false;
   startedAt += getSystemTicks() - pausedAt;
}

void Timer::stop()
{
   started = false;
}

void Timer::start()
{
   if( started )
      return;
   started = true;
   paused = false;
   startedAt = getSystemTicks();
}

void Timer::reset()
{
   paused = false;
   startedAt = getSystemTicks();
}

#if 0
uint32_t getSysTicks(void)
{
   return (uint32_t)Time::timestamp;
}
#endif

time_t Timer::getTicks()
{
   if( !started )
      return 0;
   if( paused )
      return pausedAt - startedAt;
   return getSystemTicks() - startedAt;
}
