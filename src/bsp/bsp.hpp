#ifndef __BSP_HPP
#define __BSP_HPP

#include <functional>

#include "time.hpp"
#include "inputUi.hpp"
#include "tone.hpp"
// >>->
// #include "system/device/display/displayGraphic.hpp"

namespace  Bsp
{
float getTemp();
void thinkSpeakTemp_tsk();
void synchTime_tsk();
// >>->
// DisplayGraphic* getDisplay();

extern InputUi inputUi;
extern std::function<void(void)> bspUpEventHdl;
extern std::function<void(void)> bspDownEventHdl;
extern std::function<void(void)> bspLeftEventHdl;
extern std::function<void(void)> bspRightEventHdl;
extern std::function<void(void)> bspInEventHdl;
extern std::function<void(void)> bspOutEventHdl;

extern Tone tone;

extern "C" void bspInit();
extern "C" void bspLoop();
}

#endif
