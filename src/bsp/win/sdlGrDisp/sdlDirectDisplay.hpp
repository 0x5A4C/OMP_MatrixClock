
/*! Short Description on the first line

    Detailed description...
    ...
 */

#ifndef SDLDIRECTDISPLAY_HPP
#define SDLDIRECTDISPLAY_HPP

#include <stdint.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include <SDL.h>

template<uint16_t X, uint16_t Y>
class SDLDirectDisplay
{
public:

   static void init();

   static void setPixel(int16_t x, int16_t y);

   static void setColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a = 0xff)
   {
      colorR = r;
      colorG = g;
      colorB = b;
      colorA = a;

      SDL_SetRenderDrawColor(renderer, colorR, colorG, colorB, colorA);
   }

   static void setColor(uint16_t rgb565)
   {
      colorR = ((((rgb565 >> 11) & 0x1F) * 527) + 23) >> 6;
      colorG = ((((rgb565 >> 5) & 0x3F) * 259) + 33) >> 6;
      colorB = (((rgb565 & 0x1F) * 527) + 23) >> 6;

      colorA = 0xff;

      SDL_SetRenderDrawColor(renderer, colorR, colorG, colorB, colorA);
   }

   static uint16_t getHeight()
   {
      return Y;
   }

   static uint16_t getWidth()
   {
      return X;
   }

   static void procecess()
   {
   }

   static void setWholeScreen()
   {
   }

   static bool saveScreenshotBMP(std::string filepath, SDL_Window* SDLWindow = window, SDL_Renderer* SDLRenderer = renderer)
   {
      SDL_Surface* saveSurface = NULL;
      SDL_Surface* infoSurface = NULL;
      infoSurface = SDL_GetWindowSurface(SDLWindow);
      if (infoSurface == NULL)
      {
         std::cerr << "Failed to create info surface from window in saveScreenshotBMP(string), SDL_GetError() - " << SDL_GetError() << "\n";
      }
      else
      {
         unsigned char * pixels = new (std::nothrow) unsigned char[infoSurface->w * infoSurface->h * infoSurface->format->BytesPerPixel];
         if (pixels == 0)
         {
            std::cerr << "Unable to allocate memory for screenshot pixel data buffer!\n";
            return false;
         }
         else
         {
            if (SDL_RenderReadPixels(SDLRenderer, &infoSurface->clip_rect, infoSurface->format->format, pixels, infoSurface->w * infoSurface->format->BytesPerPixel) != 0)
            {
               std::cerr << "Failed to read pixel data from SDL_Renderer object. SDL_GetError() - " << SDL_GetError() << "\n";
               pixels = NULL;
               return false;
            }
            else
            {
               saveSurface = SDL_CreateRGBSurfaceFrom(pixels, infoSurface->w, infoSurface->h, infoSurface->format->BitsPerPixel, infoSurface->w * infoSurface->format->BytesPerPixel, infoSurface->format->Rmask, infoSurface->format->Gmask, infoSurface->format->Bmask, infoSurface->format->Amask);
               if (saveSurface == NULL)
               {
                  std::cerr << "Couldn't create SDL_Surface from renderer pixel data. SDL_GetError() - " << SDL_GetError() << "\n";
                  return false;
               }
               SDL_SaveBMP(saveSurface, filepath.c_str());
               SDL_FreeSurface(saveSurface);
               saveSurface = NULL;
            }
            delete[] pixels;
         }
         SDL_FreeSurface(infoSurface);
         infoSurface = NULL;
      }
      return true;
   }

   static void update()
   {
      SDL_RenderPresent(renderer);
   }
protected:
private:
   static SDL_Window* window;
   static SDL_Renderer* renderer;
   static SDL_Rect rendererViewport;

   static uint8_t colorR;
   static uint8_t colorG;
   static uint8_t colorB;
   static uint8_t colorA;
};

template<uint16_t X, uint16_t Y>
SDL_Window* SDLDirectDisplay<X, Y>::window;
template<uint16_t X, uint16_t Y>
SDL_Renderer* SDLDirectDisplay<X, Y>::renderer;
template<uint16_t X, uint16_t Y>
SDL_Rect SDLDirectDisplay<X, Y>::rendererViewport;
template<uint16_t X, uint16_t Y>
uint8_t SDLDirectDisplay<X, Y>::colorR;
template<uint16_t X, uint16_t Y>
uint8_t SDLDirectDisplay<X, Y>::colorG;
template<uint16_t X, uint16_t Y>
uint8_t SDLDirectDisplay<X, Y>::colorB;
template<uint16_t X, uint16_t Y>
uint8_t SDLDirectDisplay<X, Y>::colorA;

template<uint16_t X, uint16_t Y>
void SDLDirectDisplay<X, Y>::init()
{
   int posX = 100;
   int posY = 200;

   // Initialize SDL
   // ==========================================================
   if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
   {
      // Something failed, print error and exit.
      //std::cout << " Failed to initialize SDL : " << SDL_GetError() << std::endl;
      //return -1;
   }

   // Create and init the window
   // ==========================================================
   window = SDL_CreateWindow("Display", posX, posY, X, Y, 0);

   if (window == nullptr)
   {
      //std::cout << "Failed to create window : " << SDL_GetError();
      //return -1;
   }

   // Create and init the renderer
   // ==========================================================
   renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

   if (renderer == nullptr)
   {
      //std::cout << "Failed to create renderer : " << SDL_GetError();
      //return -1;
   }

   // Render something
   // ==========================================================

   // Set size of renderer to the same as window
   SDL_RenderSetLogicalSize(renderer, X, Y);

   // Set color of renderer to black
   SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);

   // Clear the window and make it all red
   SDL_RenderClear(renderer);

   // Render the changes above ( which up until now had just happened behind the scenes )
   SDL_RenderPresent(renderer);

   // Pause program so that the window doesn't disappear at once.
   // This willpause for 4000 milliseconds which is the same as 4 seconds
   // SDL_Delay(4000);

   colorR = 0xff;
   colorG = 0xff;
   colorB = 0xff;
   colorA = 0xff;
}

template<uint16_t X, uint16_t Y>
void SDLDirectDisplay<X, Y>::setPixel(int16_t x, int16_t y)
{
   // Set color of renderer to white
   //SDL_SetRenderDrawColor(renderer, colorR, colorG, colorB, colorA);
   SDL_RenderDrawPoint(renderer, x, y); //Renders on middle of screen.
   //  SDL_RenderPresent(renderer);
}

#endif /* SDLDIRECTDISPLAY_HPP */
