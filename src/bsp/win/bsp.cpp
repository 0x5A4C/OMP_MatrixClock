#include "bsp/bsp.hpp"
#include <ctime>

#include <system/device/display/displayGraphic.hpp>

// >>--->platform
// #include "bsp/win/grSDLDisplay.hpp"
// typedef GrSdlDisplay display_t;
//>>--->platform
#include "bsp/win/grMxSDLDisplay.hpp"
typedef GrMxSdlDisplay display_t;

display_t bspDisplay;

DisplayGraphic* display = &bspDisplay;

namespace  Bsp
{
float getTemp()
{
   return 18.56;
}

void thinkSpeakTemp_tsk()
{
}

void synchTime_tsk()
{
}

void bspInit()
{
   time(&Time::timestamp);
}

void bspLoop()
{
}
}
