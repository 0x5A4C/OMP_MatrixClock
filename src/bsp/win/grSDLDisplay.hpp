#ifndef GRSDLDISPLAY_HPP
#define GRSDLDISPLAY_HPP

#include "system/device/display/displayGraphic.hpp"
#include "system/framework/sGfx/color.hpp"

#include "./sdlGrDisp/sdlDirectDisplay.hpp"

class GrSdlDisplay: public DisplayGraphic
{
public:
   GrSdlDisplay()//:display(SDLDisplay(8*4, 8, 10))
   {
      display.init();
      // display.setColor(0xff, 0xff, 0xff);
      // // display.setColor(0, 0, 0);
      //
      // for(int i = 0; i < 100; i++)
      // {
      //   display.setPixel(10+i, 10+i);
      //   display.setPixel(10+i, 10);
      //   display.setPixel(10, 10+i);
      // }
      // display.update();
      // display.test();
   }

   virtual void setPixel(int16_t x, int16_t y, sGfx::Color color)
   {
      display.setColor(color.getValue());
      display.setPixel(x, y);
   };

   virtual uint16_t getWidth()
   {
      return 0;
   }

   virtual uint16_t getHeight()
   {
      return 0;
   }

   void update()
   {
      display.update();
   };
protected:

private:

   // SDLDisplay display;
   SDLDirectDisplay<640, 480> display;
};

#endif
