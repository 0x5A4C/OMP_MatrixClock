
#ifndef VERSION_H
#define VERSION_H

#ifdef __cplusplus
extern "C" {
#endif

#ifndef SEMVER_VERSION
#define SEMVER_VERSION "0.2.0"
#endif

typedef struct version_s
{
  int major;
  int minor;
  int patch;
}version_t;

const version_t version = {0, 2, 0};

#ifdef __cplusplus
}
#endif

#endif
