#include "task.hpp"
#include "queue.hpp"
#include "espressif/esp_common.h"
#include "esp/uart.h"

#define vTaskDelayMs(ms)	vTaskDelay((ms)/portTICK_PERIOD_MS)
#define TASK_STACK_SIZE 256

extern "C" void appInit();
extern "C" void appLoop();

class AppTask_t: public esp_open_rtos::thread::task_t
{
public:
protected:
private:
    void task()
    {
      appInit();
      while(1)
      {
        appLoop();
        vTaskDelayMs(10);
      }

    }
};

AppTask_t app_task;

extern "C" void initTasks(void)
{
  app_task.task_create("app");
}
