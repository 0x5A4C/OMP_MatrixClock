#include <stdio.h>

#include "espressif/esp_common.h"
#include "esp/uart.h"

#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"

#include "tasks.hpp"

#define vTaskDelayMs(ms)	vTaskDelay((ms)/portTICK_PERIOD_MS)
#define TASK_STACK_SIZE 256

void bspInit();
void bspLoop();

void oneS_tsk();
void houndredMs_tsk();
void oneMs_tsk();
void appInit();
void appLoop();

#if 1
void appTask(void *pvParameters)
{
  while(1)
  {
    appLoop();
    vTaskDelayMs(10);
  }
}
#endif
#if 0
void bspTask(void *pvParameters)
{
  while(1)
  {
    // printf("bspTask\n");
    bspLoop();
    vTaskDelayMs(1000);
  }
}

void appTask_oneS(void *pvParameters)
{
  while(1)
  {
    oneS_tsk();
    vTaskDelayMs(1000);
  }
}

void appTask_houndredMs(void *pvParameters)
{
  while(1)
  {
    houndredMs_tsk();
    vTaskDelayMs(1);
  }
}

void appTask_oneMs(void *pvParameters)
{
  while(1)
  {
    oneMs_tsk();
    vTaskDelayMs(10);
  }
}

#endif
#if 0
TimerHandle_t appTask_oneS_timer, appTask_houndredMs_timer, appTask_oneMs_timer;

static void appTask_oneS_timerCallback( TimerHandle_t xTimer )
{
  // printf("appTask_oneS_timerCallback\n");
  oneS_tsk();
}

static void appTask_houndredMs_timerCallback( TimerHandle_t xTimer )
{
  // printf("appTask_houndredMs_timerCallback\n");
  houndredMs_tsk();
}

static void appTask_oneMs_timerCallback( TimerHandle_t xTimer )
{
  // printf("appTask_oneMs_timerCallback\n");
  oneMs_tsk();
}
#endif
void user_init(void)
{
    // Set UART Parameter
    uart_set_baud(0, 115200);
    // Give the UART some time to settle
    sdk_os_delay_us(500);

    bspInit();
    // xTaskCreate(bspTask, "bspTask", TASK_STACK_SIZE, NULL, 10, NULL);

    sdk_os_delay_us(10000);
#if 0
    appInit();
    xTaskCreate(appTask, "appTask", TASK_STACK_SIZE, NULL, 1, NULL);
#endif

    initTasks();

    // xTaskCreate(appTask_oneS, "appTask_oneS", TASK_STACK_SIZE, NULL, 10, NULL);
    // xTaskCreate(appTask_houndredMs, "appTask_houndredMs", TASK_STACK_SIZE, NULL, 1, NULL);
    // xTaskCreate(appTask_oneMs, "appTask_oneMs", TASK_STACK_SIZE, NULL, 10, NULL);
#if 0
    appTask_oneS_timer = xTimerCreate( "appTask_oneS_timer", pdMS_TO_TICKS( 1000 ), pdTRUE, 0, appTask_oneS_timerCallback );
    appTask_houndredMs_timer = xTimerCreate( "appTask_houndredMs_timer", pdMS_TO_TICKS( 10 ), pdTRUE, 0, appTask_houndredMs_timerCallback );
    appTask_oneMs_timer = xTimerCreate( "appTask_oneMs_timer", pdMS_TO_TICKS( 10 ), pdTRUE, 0, appTask_oneMs_timerCallback );
    xTimerStart( appTask_oneS_timer, 0 );
    xTimerStart( appTask_houndredMs_timer, 0 );
    xTimerStart( appTask_oneMs_timer, 0 );
#endif
    // Just some information
    printf("\n");
    printf("SDK version : %s\n", sdk_system_get_sdk_version());
    printf("GIT version : %s\n", GITSHORTREV);
}
