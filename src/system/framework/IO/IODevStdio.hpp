/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   IODevStdio.hpp
 * Author: lebiedzz
 *
 * Created on 9 grudzień 2015, 17:36
 */

#ifndef IODEVSTDIO_HPP
#define IODEVSTDIO_HPP

#include <stdio.h>

class IODevStdio : public IODevice
{
public:

   IODevStdio(): bytesWritten(0)
   {
   }

   virtual void write(char c)
   {
      putchar(c);
      this->bytesWritten++;
   }

   using IODevice::write;

   virtual void flush()
   {
      // TODO
      write('\b');
   }

   virtual bool read(char& /*c*/)
   {
      return false;
   }

   void clear()
   {
      this->bytesWritten = 0;
   }

   int bytesWritten;
};

#endif /* IODEVSTDIO_HPP */

