/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   IODevMemory.hpp
 * Author: lebiedzz
 *
 * Created on 8 grudzień 2015, 12:50
 */

#ifndef IODEVMEMORY_HPP
#define IODEVMEMORY_HPP

#include "IODevice.hpp"

#define BUFFER_SIZE 100

class IODevMemory : public IODevice
{
public:

   IODevMemory(): bytesWritten(0)
   {
   }

   virtual void write(char c)
   {
      this->buffer[this->bytesWritten] = c;
      this->bytesWritten++;
   }

   using IODevice::write;

   virtual void flush()
   {
      // TODO
      write('\b');
   }

   virtual bool read(char& /*c*/)
   {
      return false;
   }

   void clear()
   {
      this->bytesWritten = 0;
   }

   char buffer[BUFFER_SIZE];
   int bytesWritten;
};

#endif /* IODEVMEMORY_HPP */

