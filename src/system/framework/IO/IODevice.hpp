/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   IODevice.h
 * Author: lebiedzz
 *
 * Created on 8 grudzień 2015, 12:43
 */

#ifndef IODEVICE_H
#define IODEVICE_H

class IODevice
{
public:

   IODevice()
   {
   }

   virtual
   ~IODevice()
   {
   }

   ///	Write a single character
   virtual void write(char c) = 0;

   /// Write a C-string
   virtual void write(const char* str)
   {
      char c;
      while ((c = *str++))
      {
         this->write(c);
      }
   }

   virtual void flush() = 0;

   /// Read a single character
   virtual bool read(char& c) = 0;

private:
   IODevice(const IODevice&);
};

#endif /* IODEVICE_H */

