#include "IOStream.hpp"

const uint16_t base[] = {10, 100, 1000, 10000};

// ----------------------------------------------------------------------------

IOStream::IOStream(IODevice& outputDevice) :
   device(&outputDevice),
   mode(Mode::Ascii)
{
}

// ----------------------------------------------------------------------------

void
IOStream::writeInteger(int16_t value)
{
   if (value < 0)
   {
      this->device->write('-');
      this->writeInteger(static_cast<uint16_t> (-value));
   }
   else
   {
      this->writeInteger(static_cast<uint16_t> (value));
   }
}

void
IOStream::writeInteger(uint16_t value)
{
   const uint16_t *basePtr = base;

   bool zero = true;
   uint8_t i = 4;
   do
   {
      i--;
      char d;
      for (d = static_cast<uint16_t> ('0'); value >= basePtr[i]; value -= basePtr[i])
      {
         d++;
         zero = false;
      }
      if (!zero)
      {
         this->device->write(d);
      }
   }
   while (i);

   this->device->write(static_cast<char> (value) + '0');
}

void
IOStream::writeInteger(int32_t value)
{
#if defined(XPCC__CPU_AVR)
   char buffer[ArithmeticTraits<int32_t>::decimalDigits + 1]; // +1 for '\0'

   // Uses the optimized non standard function 'ltoa()' which is
   // not always available.

   this->device->write(ltoa(value, buffer, 10));
#else
   if (value < 0)
   {
      this->device->write('-');
      this->writeInteger(static_cast<uint32_t> (-value));
   }
   else
   {
      this->writeInteger(static_cast<uint32_t> (value));
   }
#endif
}

void
IOStream::writeInteger(uint32_t value)
{
#if defined(XPCC__CPU_AVR)
   char buffer[ArithmeticTraits<uint32_t>::decimalDigits + 1]; // +1 for '\0'

   // Uses the optimized non standard function 'ultoa()' which is
   // not always available.
   this->device->write(ultoa(value, buffer, 10));
#else
   char buffer[10 + 1]; // +1 for '\0'

   // ptr points to the end of the string, it will be filled backwards
   //FIX ME:
   //	  char *ptr = buffer + ArithmeticTraits<uint64_t>::decimalDigits;
   char *ptr = buffer + 10;

   *ptr = '\0';

   // calculate the string backwards
   do
   {
      uint32_t quot = value / 10;
      uint8_t rem = value - quot * 10;
      *(--ptr) = static_cast<char> (rem) + '0';
      value = quot;
   }
   while (value != 0);

   // write string
   this->device->write(ptr);
#endif
}

#ifndef XPCC__CPU_AVR

void
IOStream::writeInteger(int64_t value)
{
   if (value < 0)
   {
      this->device->write('-');
      this->writeInteger(static_cast<uint64_t> (-value));
   }
   else
   {
      this->writeInteger(static_cast<uint64_t> (value));
   }
}

void
IOStream::writeInteger(uint64_t value)
{
   //FIX ME:
   //	char buffer[ArithmeticTraits<uint64_t>::decimalDigits + 1]; // +1 for '\0'
   char buffer[10 + 1]; // +1 for '\0'

   // ptr points to the end of the string, it will be filled backwards
   //FIX ME:
   //	  char *ptr = buffer + ArithmeticTraits<uint64_t>::decimalDigits;
   char *ptr = buffer + 10;

   *ptr = '\0';

   // calculate the string backwards
   do
   {
      uint64_t quot = value / 10;
      uint8_t rem = value - quot * 10;
      *(--ptr) = static_cast<char> (rem) + '0';
      value = quot;
   }
   while (value != 0);

   // write string
   this->device->write(ptr);
}
#endif

// ----------------------------------------------------------------------------

void
IOStream::writeHex(const char* s)
{
   while (*s != '\0')
   {
      this->writeHex(*s);
      s++;
   }
}

void
IOStream::writeBin(const char* s)
{
   while (*s != '\0')
   {
      this->writeBin(*s);
      s++;
   }
}

// ----------------------------------------------------------------------------

void
IOStream::writeHexNibble(uint8_t nibble)
{
   char character;
   if (nibble > 9)
   {
      character = nibble + 'A' - 10;
   }
   else
   {
      character = nibble + '0';
   }
   this->device->write(character);
}

// ----------------------------------------------------------------------------

void
IOStream::writeHex(uint8_t value)
{
   writeHexNibble(value >> 4);
   writeHexNibble(value & 0xF);
}

void
IOStream::writeBin(uint8_t value)
{
   for (uint_fast8_t ii = 0; ii < 8; ii++)
   {
      if (value & 0x80)
      {
         this->device->write('1');
      }
      else
      {
         this->device->write('0');
      }
      value <<= 1;
   }

}

// ----------------------------------------------------------------------------

void
IOStream::writeFloat(const float& value)
{
   // hard coded for -2.22507e-308
   char str[13 + 1]; // +1 for '\0'

//#if defined(XPCC__CPU_AVR)
//	dtostre(value, str, 5, 0);
//	this->device->write(str);
//#elif defined(XPCC__CPU_CORTEX_M4) || defined(XPCC__CPU_CORTEX_M3) || defined(XPCC__CPU_CORTEX_M0)
   float v;
   char *ptr = &str[0];

   if (value < 0)
   {
      v = -value;
      *ptr++ = '-';
   }
   else
   {
      v = value;
   }

   int32_t ep = 0;
   if (v != 0)
   {
      while (v < 1.f)
      {
         v *= 10;
         ep -= 1;
      }

      while (v > 10)
      {
         v *= 0.1f;
         ep += 1;
      }
   }

   for (uint32_t i = 0; i < 6; ++i)
   {
      int8_t num = static_cast<int8_t>(v);
      *ptr++ = (num + '0');

      if (i == 0)
      {
         *ptr++ = '.';
      }

      // next digit
      v = (v - num) * 10;
   }

   *ptr++ = 'e';
   if (ep < 0)
   {
      ep = -ep;
      *ptr++ = '-';
   }
   else
   {
      *ptr++ = '+';
   }
   if (ep < 10)
   {
      *ptr++ = '0';
   }
   *ptr++ = '\0';	// End of string
   this->device->write(str);

   this->writeInteger(ep);
//#else
//	snprintf(str, sizeof(str), "%.5e", (double) value);
//	this->device->write(str);
//#endif
}

// ----------------------------------------------------------------------------
//#if !defined(XPCC__CPU_AVR)
void
IOStream::writeDouble(const double& value)
{
//#if defined(XPCC__CPU_CORTEX_M4) || defined(XPCC__CPU_CORTEX_M3) || defined(XPCC__CPU_CORTEX_M0)
//	// TODO do this better
   writeFloat(static_cast<float>(value));
//#else
//	// hard coded for 2.22507e-308
//	char str[13 + 1]; // +1 for '\0'
//	snprintf(str, sizeof(str), "%.5e", value);
//	this->device->write(str);
//#endif
}
//#endif

// ----------------------------------------------------------------------------

IOStream&
IOStream::operator<<(const void* p)
{
#if XPCC__SIZEOF_POINTER == 2

   this->device->write('0');
   this->device->write('x');

   uint16_t value = reinterpret_cast<uint16_t> (p);

   writeHex(value >> 8);
   writeHex(value);

#elif XPCC__SIZEOF_POINTER == 4

   this->device->write('0');
   this->device->write('x');

   uint32_t value = reinterpret_cast<uint32_t> (p);

   writeHex(value >> 24);
   writeHex(value >> 16);
   writeHex(value >> 8);
   writeHex(value);

#elif XPCC__SIZEOF_POINTER == 8

   this->device->write('0');
   this->device->write('x');

   uint64_t value = reinterpret_cast<uint64_t> (p);

   writeHex(value >> 56);
   writeHex(value >> 48);
   writeHex(value >> 40);
   writeHex(value >> 32);
   writeHex(value >> 24);
   writeHex(value >> 16);
   writeHex(value >> 8);
   writeHex(value);

#endif
   return *this;
}