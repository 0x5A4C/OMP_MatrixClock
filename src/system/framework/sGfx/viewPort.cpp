
/*! Short Description on the first line

    Detailed description...
    ...
 */

#include "viewPort.hpp"
#include "drawable.hpp"

sGfx::ViewPort::ViewPort(): x(0), y(0), xCur(0), yCur(0), parent(NULL), child(NULL)/*, display(NULL)*/
{
}

sGfx::ViewPort::ViewPort(int16_t x, int16_t y) : x(x), y(y), xCur(0), yCur(0), parent(NULL), child(NULL)/*, display(NULL)*/
{
}

sGfx::ViewPort::ViewPort(ViewPort* parent) : x(0), y(0), xCur(0), yCur(0), parent(parent), child(NULL)/*, display(NULL)*/
{
}

sGfx::ViewPort::ViewPort(int16_t x, int16_t y, ViewPort* parent) : x(x), y(y), xCur(0), yCur(0), parent(parent), child(NULL)/*, display(NULL)*/
{
}

void sGfx::ViewPort::gotoXY(int16_t x, int16_t y)
{
   this->xCur = x;
   this->yCur = y;
}

uint16_t sGfx::ViewPort::getCurX()
{
   return this->xCur;
}

uint16_t sGfx::ViewPort::getCurY()
{
   return this->yCur;
}

void sGfx::ViewPort::setPixel(int16_t x, int16_t y, Color color)
{
   if (parent != NULL)
   {
      parent->setPixel(this->x + this->xCur + x, this->y + this->yCur + y, color);
   }
   else
   {
      if(display != NULL)
      {
         display->setPixel(this->x + this->xCur + x, this->y + this->yCur + y, color);
      }
   }
}

uint16_t sGfx::ViewPort::getHeight()
{
   uint16_t retVal = 0;

   if (parent != NULL)
   {
      retVal = parent->getHeight();
   }
   else
   {
      if(display != NULL)
      {
         retVal = display->getHeight();
      }
   }
   return retVal;
}

uint16_t sGfx::ViewPort::getWidth()
{
   uint16_t retVal = 0;

   if (parent != NULL)
   {
      retVal = parent->getWidth();
   }
   else
   {
      if(display != NULL)
      {
         retVal = display->getWidth();
      }
   }
   return retVal;
}

void sGfx::ViewPort::update(void)
{
   if(child != NULL)
   {
      child->reDraw();
   }
   if(display != NULL)
   {
      display->update();
   }
}

void sGfx::ViewPort::add(Drawable* child)
{
   if(this->child == NULL)
   {
      this->child = child;
   }
   else
   {
      this->child->add(child);
   }
}

void sGfx::ViewPort::tick()
{
   if(child != NULL)
   {
      child->tick();
   }
}

void sGfx::ViewPort::setDisplay(DisplayGraphic* display)
{
   // this->display = display;
}
