#ifndef COLOR_HPP
#define COLOR_HPP

#define ALWAYS_INLINE inline

namespace sGfx
{
class Color
{
public:
   static ALWAYS_INLINE Color white()
   {
      return Color(0xffff);
   };
   static ALWAYS_INLINE Color yellow()
   {
      return Color(0xFFE0);
   };
   static ALWAYS_INLINE Color magenta()
   {
      return Color(0xF81F);
   };
   static ALWAYS_INLINE Color red()
   {
      return Color(0xF800);
   };
   static ALWAYS_INLINE Color orange()
   {
      return Color(0xFD20);
   };
   static ALWAYS_INLINE Color sliver()
   {
      return Color(0xC618);
   };
   static ALWAYS_INLINE Color gray()
   {
      return Color(0x8410);
   };
   static ALWAYS_INLINE Color maroon()
   {
      return Color(0x8000);
   };
   static ALWAYS_INLINE Color lime()
   {
      return Color(0x07E0);
   };
   static ALWAYS_INLINE Color green()
   {
      return Color(0x0400);
   };
   static ALWAYS_INLINE Color blue()
   {
      return Color(0x001F);
   };
   static ALWAYS_INLINE Color navy()
   {
      return Color(0x0010);
   };
   static ALWAYS_INLINE Color black()
   {
      return Color(0x0000);
   };

   /**
    * @param	red
    * 		Range [0..255]
    * @param	green
    * 		Range [0..255]
    * @param	blue
    * 		Range [0..255]
    */
   Color(uint8_t red, uint8_t green, uint8_t blue) :
      color(((static_cast<uint16_t>(red >> 3) << 11) |
             (static_cast<uint16_t>(green >> 2) << 5) |
             static_cast<uint16_t>(blue >> 3)))
   {
   }

   Color(uint16_t color) :
      color(color)
   {
   }

   Color() : color(0)
   {
   }

   inline uint16_t
   getValue() const
   {
      return color;
   }

   bool
   operator == (const Color& other) const
   {
      return (color == other.color);
   }

private:
   uint16_t color;
};
}

#endif
