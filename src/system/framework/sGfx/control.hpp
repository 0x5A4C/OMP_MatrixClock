#ifndef CONTROL_HPP
#define CONTROL_HPP

#include "drawable.hpp"
#include "control.hpp"

namespace sGfx
{
class Control: public Drawable
{
public:
   Control(): Drawable()
   {
   }

   Control(ViewPort* viewport): Drawable(viewport)
   {
   }

protected:
private:


};
}

#endif
