
/*! Short Description on the first line

    Detailed description...
    ...
 */

#ifndef TXTCMN_HPP
#define TXTCMN_HPP

#include <stdint.h>

#include "drawable.hpp"
#include "control.hpp"

namespace sGfx
{

  class TxtCmn : public Control
  {
  public:
    TxtCmn():Control()
    {};
    TxtCmn(ViewPort* viewport):Control(viewport)
    {};
    virtual void drawChar(unsigned char c) = 0;
    void setChar(unsigned char c)
    {
      charToDraw = c;
    }

    void draw()
    {
      drawChar(charToDraw);
    }

    void setViewport(ViewPort* viewport)
    {
      this->viewport = viewport;
    }

    virtual uint8_t getWidth() = 0;
    virtual uint8_t getHeight() = 0;

  protected:
  private:
    char charToDraw;
  };
}
#endif /* TXTCMN_HPP */
