
#ifndef TRANSFORXY_HPP
#define TRANSFORXY_HPP

#if 1
#include <functional>
#endif
#include "easing/easing.hpp"
#include "viewPort.hpp"

namespace sGfx
{
template<class T>
class TansformXY : public T
{
public:
   TansformXY():
      T(),
      isOn(false),
      counter(0)
   {
   }

   TansformXY(ViewPort* viewport):
      T(viewport),
      isOn(false),
      counter(0)
   {
   }

   virtual ~TansformXY()
   {
   }

   virtual bool isRunning() const
   {
      return isOn;
   }

   void start(int16_t endX, int16_t endY, uint16_t duration, Easing easingX = &Easings::linearTween, Easing easingY = &Easings::linearTween)
   {
      this->counter = 0;
      this->startX = T::x;
      this->startY = T::y;
      this->endX = endX;
      this->endY = endY;
      this->duration = duration;
      this->easingX = easingX;
      this->easingY = easingY;

      this->isOn = true;
   }

   void cancel()
   {
      isOn = false;
   }

   virtual void tick()
   {
      T::tick();

      if (isOn)
      {
         if (counter <= (uint32_t) (duration))
         {
            int16_t deltaX = easingX(counter, 0, endX - startX, duration);
            int16_t deltaY = easingY(counter, 0, endY - startY, duration);
            T::setX(startX + deltaX);
            T::setY(startY + deltaY);

            counter++;
         }
         else
         {
            isOn = false;
            counter = 0;
#if 1
            if(endCallback)
            {
               endCallback();
            }
#endif
         }
      }
   }

   //idea from:
   // - https://stackoverflow.com/questions/14189440/c-class-member-callback-simple-examples
   //other links:
   // - https://stackoverflow.com/questions/2298242/callback-functions-in-c
   // - http://tedfelix.com/software/c++-callbacks.html
   // - https://stackoverflow.com/questions/21806632/how-to-properly-check-if-stdfunction-is-empty-in-c11
#if 1
   void addEndHandler(std::function<void(void)> callback)
   {
      endCallback = callback;
   }
#endif
protected:
   bool           isOn;
   uint16_t       counter;
   uint16_t       duration;
   int16_t        startX;
   int16_t        startY;
   int16_t        endX;
   int16_t        endY;
   Easing         easingX;
   Easing         easingY;
#if 1
   std::function<void(void)> endCallback;
#endif
};

}

#endif /* TRANSFORXY_HPP */
