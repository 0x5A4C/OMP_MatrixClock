
/*! Short Description on the first line

    Detailed description...
    ...
 */

#ifndef BMPXBM_HPP
#define BMPXBM_HPP

#include <stdint.h>
#include "drawable.hpp"

namespace sGfx
{

class BmpXbm: public Drawable
{
public:
   BmpXbm();
   BmpXbm(ViewPort* viewport);
   virtual void draw();
   // void set(int16_t x, int16_t y, int16_t w, int16_t h);
   // virtual void setX(int16_t x);
   // virtual void setY(int16_t y);
   // virtual void setW(int16_t w);
   // virtual void setH(int16_t h);
   // virtual void setViewport(ViewPort* viewport);
   virtual void setBitmap(const uint8_t* bmpToDraw);

protected:
   const uint8_t* bitmap;

private:
};
}

#endif /* BMPXBM_HPP */
