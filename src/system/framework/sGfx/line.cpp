
/*! Short Description on the first line

    Detailed description...
    ...
 */

#include "line.hpp"
#include <math.h>
#include <stdlib.h>

#ifndef _swap_int16_t
#define _swap_int16_t(a, b) { int16_t t = a; a = b; b = t; }
#endif

sGfx::Line::Line(): Drawable(), x0(0), y0(0), x1(0), y1(0)
{
}

sGfx::Line::Line(ViewPort* viewport): Drawable(viewport)
{
}

sGfx::Line::Line(int16_t x0, int16_t y0, int16_t x1, int16_t y1): Drawable(), x0(x0), y0(y0), x1(x1), y1(y1)
{
}

sGfx::Line::Line(int16_t x0, int16_t y0, int16_t x1, int16_t y1, ViewPort* viewport): Drawable(viewport), x0(x0), y0(y0), x1(x1), y1(y1)
{
}

void sGfx::Line::draw()
{
   int16_t steep = abs(y1 - y0) > abs(x1 - x0);

   //  if (viewport != NULL)
   {
      if (steep)
      {
         _swap_int16_t(x0, y0);
         _swap_int16_t(x1, y1);
      }

      if (x0 > x1)
      {
         _swap_int16_t(x0, x1);
         _swap_int16_t(y0, y1);
      }

      int16_t dx, dy;
      dx = x1 - x0;
      dy = abs(y1 - y0);

      int16_t err = dx / 2;
      int16_t ystep;

      if (y0 < y1)
      {
         ystep = 1;
      }
      else
      {
         ystep = -1;
      }

      for (; x0 <= x1; x0++)
      {
         if (steep)
         {
            setPixel(y0, x0, this->color);
         }
         else
         {
            setPixel(x0, y0, this->color);
         }
         err -= dy;
         if (err < 0)
         {
            y0 += ystep;
            err += dx;
         }
      }
   }
}

void sGfx::Line::set(int16_t x0, int16_t y0, int16_t x1, int16_t y1)
{
   this->x0 = x0;
   this->y0 = y0;
   this->x1 = x1;
   this->y1 = y1;

   //  this->x = this->x0;
   //  this->y = this->y0;
   //  this->w = this->x1 - this->x0;
   //  this->h = this->y1 - this->y0;
}

void sGfx::Line::setPixel(int16_t x, int16_t y, Color color)
{
   if(x >= 0 && y >= 0)
   {
      {
         if(parent != NULL)
         {
            parent->setPixel(x + this->x, y + this->y, color);
         }
         else
         {
            viewport->setPixel(x + this->x, y + this->y, color);
         }

      }
   }
}

sGfx::LineV::LineV() : Line()
{
}

sGfx::LineV::LineV(ViewPort* viewport): Line(viewport)
{
}

sGfx::LineV::LineV(int16_t x0, int16_t y0, int16_t h) : Line(x0, y0, x0, y0 + h - 1)
{
}

sGfx::LineV::LineV(int16_t x0, int16_t y0, int16_t h, ViewPort* viewport) : Line(x0, y0, x0, y0 + h - 1, viewport)
{
}

void sGfx::LineV::set(int16_t x0, int16_t y0, int16_t h)
{
   this->x0 = x0;
   this->y0 = y0;
   this->x1 = x0;
   this->y1 = y0 + h - 1;

   //  this->x = this->x0;
   //  this->y = this->y0;
   //  this->w = 1;
   //  this->h = this->y1 - this->y0;
}



sGfx::LineH::LineH() : Line()
{
}

sGfx::LineH::LineH(ViewPort* viewport): Line(viewport)
{
}

sGfx::LineH::LineH(int16_t x0, int16_t y0, int16_t w) : Line(x0, y0, x0 + w - 1, y0)
{
}

sGfx::LineH::LineH(int16_t x0, int16_t y0, int16_t w, ViewPort* viewport) : Line(x0, y0, x0 + w - 1, y0, viewport)
{
}

void sGfx::LineH::set(int16_t x0, int16_t y0, int16_t w)
{
   this->x0 = x0;
   this->y0 = y0;
   this->x1 = x0 + w - 1;
   this->y1 = y0;

   //  this->x = this->x0;
   //  this->y = this->y0;
   //  this->w = this->x1 - this->x0;
   //  this->h = 1;
}
