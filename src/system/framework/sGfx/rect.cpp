
/*! Short Description on the first line

    Detailed description...
    ...
 */

#include "rect.hpp"

sGfx::Rect::Rect():
   Drawable(),
   lH0(LineH(x, y, w)),
   lH1(LineH(x, y + h - 1, w)),
   lV0(LineV(x, y, h)),
   lV1(LineV(x + w - 1, y, h))
{
}

sGfx::Rect::Rect(ViewPort* viewport):
   Drawable(viewport),
   lH0(LineH(x, y, w)),
   lH1(LineH(x, y + h - 1, w)),
   lV0(LineV(x, y, h)),
   lV1(LineV(x + w - 1, y, h))
{
   this->viewport = viewport;

   lH0.setParent(this);
   lH1.setParent(this);
   lV0.setParent(this);
   lV1.setParent(this);
}

void sGfx::Rect::draw()
{
   lH0.draw();
   lH1.draw();
   lV0.draw();
   lV1.draw();
}

void sGfx::Rect::setLines()
{
   lH0.set(0, 0, this->w);
   lH1.set(0, this->h, this->w);
   lV0.set(0, 0, this->h);
   lV1.set(this->w, 0, this->h);
}

void sGfx::Rect::set(int16_t x, int16_t y, int16_t w, int16_t h)
{
   this->x = x;
   this->y = y;
   this->w = w;
   this->h = h;

   setLines();
}

void sGfx::Rect::setX(int16_t x)
{
   this->x = x;

   setLines();
}

void sGfx::Rect::setY(int16_t y)
{
   this->y = y;

   setLines();
}

void sGfx::Rect::setW(int16_t w)
{
   this->w = w;

   setLines();
}

void sGfx::Rect::setH(int16_t h)
{
   this->h = h;

   setLines();
}

void sGfx::Rect::setViewport(ViewPort* viewport)
{
   this->viewport = viewport;

   lH0.setViewport(viewport);
   lH1.setViewport(viewport);
   lV0.setViewport(viewport);
   lV1.setViewport(viewport);
}



/*! Short Description on the first line

   Detailed description...
   ...
*/

sGfx::RectFill::RectFill()
{
}

sGfx::RectFill::RectFill(ViewPort* viewport): Rect(viewport)
{
   this->viewport = viewport;
}

void sGfx::RectFill::draw()
{
   lV0.setColor(this->color);
   for (int16_t i = 0; i < w; i++)
   {
      lV0.set(i, 0, h);
      lV0.draw();
   }
}
