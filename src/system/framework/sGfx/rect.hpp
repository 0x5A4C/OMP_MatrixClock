
/*! Short Description on the first line

    Detailed description...
    ...
 */

#ifndef RECT_HPP
#define RECT_HPP

#include <stdint.h>
#include "line.hpp"

namespace sGfx
{

class Rect: public Drawable
{
public:
   Rect();
   Rect(ViewPort* viewport);
   virtual void draw();
   void set(int16_t x, int16_t y, int16_t w, int16_t h);
   virtual void setX(int16_t x);
   virtual void setY(int16_t y);
   virtual void setW(int16_t w);
   virtual void setH(int16_t h);
   virtual void setViewport(ViewPort* viewport);

protected:
   LineH lH0;
   LineH lH1;
   LineV lV0;
   LineV lV1;

private:
   void setLines();
};

class RectFill: public Rect
{
public:
   RectFill();
   RectFill(ViewPort* viewport);
   virtual void draw();

protected:
private:
};
}

#endif /* RECT_HPP */
