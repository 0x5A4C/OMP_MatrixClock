
/*! Short Description on the first line

    Detailed description...
    ...
 */

#ifndef BMPXBMANIMATED_HPP
#define BMPXBMANIMATED_HPP

#include <stdint.h>
#include "bmpXbm.hpp"

namespace sGfx
{

class BmpXbmAnimated: public BmpXbm
{
public:
   BmpXbmAnimated();
   BmpXbmAnimated(ViewPort* viewport);
   virtual void draw();
   virtual void tick();
   void setDuration(uint16_t duration);
   void start();
   void stop();
   virtual void setBitmap(const uint8_t* bmpToDraw);
   void setNumOfSlides(uint16_t numOfSlides);

protected:

private:
   uint16_t duration;
   uint16_t pendingDuration;
   bool animated;
   uint16_t slideNum;
   const uint8_t* bitmapStartAddr;
   uint16_t numOfSlides;
};
}

#endif /* BMPXBMANIMATED_HPP */
