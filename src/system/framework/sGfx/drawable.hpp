#ifndef DRAWABLE_HPP
#define DRAWABLE_HPP

// #include <stddef.h>
#include <stdint.h>
#include <cstddef>

// #include "drawer.hpp"
#include "viewPort.hpp"

#include "color.hpp"

namespace sGfx
{
class Drawable/* : public Drawer*/
{

   //   int16_t x;
   //   int16_t y;
   //
   //   uint16_t w;
   //   uint16_t h;
   //
   //  ViewPort* viewport;
   //  Drawable* parent;
   //  Drawable* child;
   //  Color color;

public:
   Drawable(): x(0), y(0), viewport(NULL), parent(NULL), child(NULL), color(Color::white())
   {
   };

   Drawable(int16_t x, int16_t y): x(x), y(y), viewport(NULL), parent(NULL), child(NULL), color(Color::white())
   {
   };

   Drawable(ViewPort* viewport): x(0), y(0), viewport(viewport), parent(NULL), child(NULL), color(Color::white())
   {
   };

   Drawable(int16_t x, int16_t y, ViewPort* viewport): x(x), y(y), viewport(viewport), parent(NULL), child(NULL), color(Color::white())
   {
   };

   void setColor(Color color)
   {
      this->color = color;
   }

   void setViewport(ViewPort* viewport)
   {
      this->viewport = viewport;
   }

   void setParent(Drawable* parent)
   {
      this->parent = parent;
   }

   void add(Drawable* child)
   {
      if(this->child == NULL)
      {
         this->child = child;
      }
      else
      {
         this->child->add(child);
      }
   }

   virtual void draw() = 0;

   void reDraw()
   {
      if(child != NULL)
      {
         child->reDraw();
      }
      draw();
   }

   virtual void tick()
   {
      if(child != NULL)
      {
         child->tick();
      }
   }

   virtual void setX(int16_t x)
   {
      this->x = x;
   }

   virtual void setY(int16_t y)
   {
      this->y = y;
   }

   // int16_t getX()
   // {
   //   int16_t val = this->x;
   //   if(parent != NULL)
   //   {
   //     val += parent->getX();
   //   }
   //   return val;
   // }
   //
   // int16_t getY()
   // {
   //   int16_t val = this->y;
   //   if(parent != NULL)
   //   {
   //     val += parent->getY();
   //   }
   //   return val;
   // }

   void setW(uint16_t w)
   {
      this->w = w;
   }

   void setH(uint16_t h)
   {
      this->h = h;
   }

   // uint16_t getW()
   // {
   //   uint16_t val = w;
   //   if(parent != NULL)
   //   {
   //     if(x + val > parent->getW())
   //     {
   //       val = parent->getW() - x;
   //     }
   //   }
   //   return val;
   // }
   //
   // uint16_t getH()
   // {
   //   uint16_t val = h;
   //   if(parent != NULL)
   //   {
   //     if(y + val > parent->getH())
   //     {
   //       val = parent->getH() - y;
   //     }
   //   }
   //   return val;
   // }

   virtual void setPixel(int16_t x, int16_t y, Color color)
   {
      if(x >= 0 && y >= 0)
      {
         if(parent != NULL)
         {
            if( x + this->x < parent->w && y + this->y < parent->h)
            {
               parent->setPixel(x + this->x, y + this->y, color);
            }
         }
         else
         {
            viewport->setPixel(x + this->x, y + this->y, color);
         }
      }
   }

protected:
   int16_t x;
   int16_t y;

   uint16_t w;
   uint16_t h;

   ViewPort* viewport;
   Drawable* parent;
   Drawable* child;
   Color color;

private:

};
}

#endif
