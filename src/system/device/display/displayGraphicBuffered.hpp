
/*! Short Description on the first line

    Detailed description...
    ...
 */

#ifndef DISPLAYGRAPHICBUFFERED_HPP
#define DISPLAYGRAPHICBUFFERED_HPP

#include "displayGraphic.hpp"

template <uint8_t width, uint8_t height, typename typedata>
class DisplayGraphicBufferd: DisplayGraphic
{
public:
protected:
  virtual void setPixel(int16_t x, int16_t y) = 0;
  virtual void clearPixel(int16_t x, int16_t y) = 0;
  virtual void update(void) = 0;
private:
  typedata buffer[width, height];
};

#endif /* DISPLAYGRAPHICBUFFERED_HPP */

