
/*! Short Description on the first line

    Detailed description...
    ...
 */

#ifndef DISPLAYGRAPHIC_HPP
#define DISPLAYGRAPHIC_HPP

#include <stdint.h>
#include "display.hpp"
#include "system/framework/sGfx/color.hpp"

class DisplayGraphic
{
public:
  virtual void setPixel(int16_t x, int16_t y, sGfx::Color color) = 0;

  virtual uint16_t getWidth() = 0;
  virtual uint16_t getHeight() = 0;

  virtual void update() = 0;

protected:
private:
};

#endif /* DISPLAYGRAPHIC_HPP */
