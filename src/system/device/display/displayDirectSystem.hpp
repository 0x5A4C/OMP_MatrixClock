
/*! Short Description on the first line

    Detailed description...
    ...
 */

#ifndef DISPLAYGENERICBUFFERED_HPP
#define DISPLAYGENERICBUFFERED_HPP

#include "displayGraphic.hpp"

template <class PLATFORMDISP>
class DisplayDirectSystem : DisplayGraphic
{
public:

  virtual void setPixel(int16_t x, int16_t y)
  {
    PLATFORMDISP::setPixel(x, y);
  }

  virtual void clearPixel(int16_t x, int16_t y)
  {

  }

  virtual void setColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
  {
    PLATFORMDISP::setColor(r, g, b, a);
  }

  virtual void setColor(uint16_t rgb)
  {
    PLATFORMDISP::setColor(rgb);
  }

  uint16_t getHeight()
  {
    return PLATFORMDISP::getHeight();
  }

  uint16_t getWidth()
  {
    return PLATFORMDISP::getWidth();
  }

  virtual void update(void)
  {

  }
protected:
private:
};

#endif /* DISPLAYGRAPHICBUFFERED_HPP */

